"""
Django settings for main project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/

For generating cache manifest this would be useful
grep -e "http[s]*://lh[a-z 0-9 :  _ / . = } { -]*" index/templates/index.html -io > 
index/templates/cache.appcache 
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
APP_PATH = os.path.dirname(os.path.abspath(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/dev/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '!la)u3&((v=$-8p6hnpl5)o0-xsopj(ti9x*=n_t@v9q^on)_&'

# SECURITY WARNING: don't run with debug turned on in production!

DEBUG = True

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['*']

SESSON_COOKIE_SECURE=True
# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.staticfiles',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    #'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.sessions',
    'django_cron',
    'piston',
    'index',
    'algotrade',
    'woodstock',
    'data',
    'custom',
)
#EMAIL_VERIFICATION='mandatory'

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.request',
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.static',
)

DATABASE_ROUTERS = ['algotrade.router.AlgoTradeRouter','users.router.UserRouter']


LOGIN_URL='/accounts/login'


MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

CRON_CLASSES = [
                'woodstock.cron.UpdateOpeningPrice'
]

AUTHENTICATION_BACKENDS = (
                           'django.contrib.auth.backends.ModelBackend',
                           'users.backends.OAuthBackend',
)


APPEND_SLASH = True


ROOT_URLCONF = 'bazaar.urls'

WSGI_APPLICATION = 'bazaar.wsgi.application'


SITE_ID=1
# Database
# https://docs.djangoproject.com/en/dev/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'ktj14_bazaarfinalround',
        'HOST':'127.0.0.1',
        'PORT':'3306',
        'USER':'root',
        'PASSWORD':'',
    },
             
    'algotrade': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'algotrade',
        'USER':'root',
        'HOST': '127.0.0.1',                 
        'PORT': '3306', 
        'PASSWORD':'',
    },
             
    'beta': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'ktj15',
        'USER':'root',
        'HOST': '127.0.0.1',                 
        'PORT': '3306',
        'PASSWORD':''  
    }
}
# Internationalization
# https://docs.djangoproject.com/en/dev/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Calcutta'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/dev/howto/static-files/


STATIC_URL = '/static/'
STATIC_ROOT = '/home/ktj/kshitij-2014/bazaar/index/static'


# For Template Directories
TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'index/templates/'),
    os.path.join(BASE_DIR, 'woodstock/templates/'),
    os.path.join(BASE_DIR, 'users/templates/'),
)

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"

