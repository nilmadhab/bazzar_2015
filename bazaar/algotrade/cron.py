from algotrade.cron_functions import updateforexranks, forex_trigclose, \
    comm_trigclose, comm_pending, forex_pending
from django_cron import cronScheduler, Job
class UpdateForexRanks(Job):

        run_every = 1800
                
        def job(self):
                updateforexranks()
                
cronScheduler.register(UpdateForexRanks)

class ForexTrigClose(Job):

        run_every = 300
                
        def job(self):
                forex_trigclose()
                
cronScheduler.register(ForexTrigClose)

class CommTrigClose(Job):

        run_every = 300
                
        def job(self):
                comm_trigclose()
                
cronScheduler.register(CommTrigClose)

class CommPending(Job):

        run_every = 300
                
        def job(self):
                comm_pending()
                
cronScheduler.register(CommPending)

class ForexPending(Job):

        run_every = 300
                
        def job(self):
                forex_pending()
                
cronScheduler.register(ForexPending)
