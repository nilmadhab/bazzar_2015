from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from data.models import *
from django.core.files import File
from bazaar import settings
from custom.func import *
from custom.globals import globalvars,friday
from custom.classes import sterminal_wud_opened, sterminal_pend_closed, sterminal_pending
from datetime import datetime
from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.db import DatabaseError, IntegrityError
import MySQLdb
from index.models import Notifications
import copy
import urllib2, string, re
#from woodstock import settings

def index_beta(request):
    return render_to_response("index_beta.html",{},context_instance=RequestContext(request))



def index(request):
    user = request.user.username
    try:
        query = WudRankings.objects.get(userid=user)
    except:
        query=WudRankings.objects.create(userid=user,cashbalance=2500000,reserve=2500000,equity=2500000,rank=0,margin=0)
    query.reserve = round(query.reserve,2)
    query.equity = round(query.equity,2)
    query.margin = round(query.margin,2)
    notifications=Notifications.objects.all().order_by('-time')

    f2 = urllib2.urlopen('http://bazaar.ktj.in/static/files/stats.txt')
    cont2 = f2.read()
    stocknames = cont2.split('~')

    futures = []
    que = FuturesBase.objects.all().order_by('stockid')
        
    for q in que:
        name = q.javaname
        namechunk = name.split('.')
        namenew = namechunk[0]
        
        query = StocksRate.objects.filter(stock=namenew)
        future = {}
        future['name'] = name
        for que2 in query:
            future['stock'] = que2.stock
            future['price'] = que2.price
        futures.append(copy.deepcopy(future))

    options = []
    option = {}
    query = OptionsBase.objects.all().order_by('stockid')
    for arr in query:
        option['name'] = arr.javaname
        option['strike'] = arr.strikeprice
        namechunk = option['name'].split('.')
        namenew = namechunk[0]
        
        query3 = StocksRate.objects.filter(stock=namenew)
        for q in query3:
            option['stockname'] = q.stock
            option['price'] = round(q.price, 2)
        
        options.append(copy.deepcopy(option))

    return render_to_response("index.html",{'username':user, 'data':query,'notifications':notifications,'allStocks':stocknames,'futures':futures,'options':options},context_instance=RequestContext(request))


def help(request):
    return render_to_response('help.html',context_instance=RequestContext(request))

@login_required
def credits(request):
    return render_to_response('credits.html',context_instance=RequestContext(request))

@login_required
def learntoplay(request):
    return render_to_response('learntoplay.html',context_instance=RequestContext(request))

@login_required
def rules(request):
    return render_to_response('rules.html',context_instance=RequestContext(request))

@login_required
def interface(request):
    return render_to_response('interface.html',context_instance=RequestContext(request))

@login_required
def stats(request):
    user = request.user.username
    query = WudRankings.objects.get(userid=user)
    query.reserve = round(query.reserve,2)
    query.equity = round(query.equity,2)
    query.margin = round(query.margin,2)
    return render_to_response('stats.html',{'data':query},context_instance=RequestContext(request))

@login_required
def stocks_marquee(request):
    check_q = StocksRate.objects.all()
    cnt = check_q.count()
    
    if cnt == 0:
        return HttpResponse('No Stocks Exists')
    datas = []
    data = {}
    
    for arr in check_q:
        data['que'] = arr
        open = arr.open
        if open == 0:
            per = 0
        else:
            per = (arr.price-open)/open
            per = per*100
        
        data['per'] = round(per, 2)
        datas.append(copy.deepcopy(data))
    
    return render_to_response('stocks_marquee.html',{'datas':datas},context_instance=RequestContext(request))


@login_required
def info(request):
    query = StocksRate.objects.all()
    cnt = query.count()
    
    if cnt == 0:
        return HttpResponse('No stocks exist')
    datas = []
    data = {}
    for q in query:
        data['query'] = q
        open = q.open
        per = (q.price-open)/open
        per = per*100
        data['per'] = round(per,2)
        datas.append(copy.deepcopy(data))
    
    return render_to_response('info.html',{'datas':datas},context_instance=RequestContext(request))

@login_required
def toprankers(request):
    query = WudRankings.objects.all().order_by('-equity')[0:30]
    datas = []
    data = {}
    rank = 1
    for q in query:
        if rank < 31:
            data['userid'] = q.userid
            data['equity'] = round(q.equity,0)
            data['rank'] = rank
        rank = rank+1
        datas.append(copy.deepcopy(data))
    
    return render_to_response('toprankers.html',{'datas':datas},context_instance=RequestContext(request))

@login_required
def trade(request):

    f2 = urllib2.urlopen('http://bazaar.ktj.in/static/files/stats.txt')
    cont2 = f2.read()
    stocknames = cont2.split('~')

    futures = []
    que = FuturesBase.objects.all().order_by('stockid')
        
    for q in que:
        name = q.javaname
        namechunk = name.split('.')
        namenew = namechunk[0]
        
        query = StocksRate.objects.filter(stock=namenew)
        future = {}
        future['name'] = name
        for que2 in query:
            future['stock'] = que2.stock
            future['price'] = que2.price
        futures.append(copy.deepcopy(future))

    options = []
    option = {}
    query = OptionsBase.objects.all().order_by('stockid')
    for arr in query:
        option['name'] = arr.javaname
        option['strike'] = arr.strikeprice
        namechunk = option['name'].split('.')
        namenew = namechunk[0]
        
        query3 = StocksRate.objects.filter(stock=namenew)
        for q in query3:
            option['stockname'] = q.stock
            option['price'] = round(q.price, 2)
        
        options.append(copy.deepcopy(option))

    return render_to_response('trade.html',{'allStocks':stocknames,'futures':futures,'options':options},context_instance=RequestContext(request))
        
@login_required
def strade(request):
    if(globalvars.tradeallow != 1):
        return HttpResponse('Woodstock is now closed')
    cont = StocksRate.objects.all()
    f = urllib2.urlopen('http://bazaar.ktj.in/static/files/stats.txt')
    stats_file = f.read()
    stats = stats_file.split('~')
    lots=range(1,101)
    ctx = { 'stocks' : cont, 'stats': stats ,'lots':lots}
    return render_to_response("stocks/strade.html", ctx ,context_instance=RequestContext(request))

@login_required
def postexecute(request):
    user = request.user.username
    if request.POST :
        values = request.POST
        quantity = int(values['quantity'])
        if quantity < 0 :
            return HttpResponse('Positions cannot be placed')
        if not valid_quantity(values['quantity']):
            return HttpResponse('Please Enter a Valid quantity')
        
        if not((values['bors'] =='Buy') or (values['bors'] == 'Short Sell')):
            return HttpResponse('Position cannot be opened '+values['bors'])
            
        print values['stockname']    
        currentprice = current_price(values['stockname'])
        br = brockerage_fun(user,currentprice)
        if currentprice != '0':
            if quantity != '':
                pendingprice = values['pendingprice']
                triggerprice = values['triggerprice']
                
                if(globalvars.tradeallow == 1): #tradeallow
                    status =1
                    
                    stock_limit = values['stockname']+".NS"
                    options = WudOpenedOptions.objects.filter(userid=user, stockname=values['stockname'])
                    num = options.count()
                    print "options count = %s" % str(num)
                    #print quantity
                    quantity_limit = 0
                    if num != 0:
                        for option in options:
                            quantity_limit += option.quantity
                    quantity_limit = quantity_limit+ int(quantity)
                    if quantity_limit>10000 :
                        return HttpResponse("<div align='center'><font color='#FF3333'>You cannot hold more than 10,000 stocks of a company at a time</font></div>")
                    if pendingprice=='' and triggerprice=='':
                        triggerprice = 0
                        if values['bors'] == 'Buy':
                            row = WudRankings.objects.filter(userid=user)
                            for col in row:
                                brockerage = quantity*br
                                req = quantity*currentprice+brockerage
                                if col.reserve < req :
                                    quan = col.reserve/currentprice
                                    trademsg = 'You dont have enough reserve'
                                    trademsg += 'You can only buy <b>'+str(quan)+'</b> stocks'
                                else:
                                    if req == 0 or req == '' or req < 0 :
                                        return HttpResponse('The order cannot be placed')
                                    if currentprice==0 or currentprice=='' or currentprice<0 :
                                        return HttpResponse('The order cannot be placed')
                                    col.reserve = col.reserve - req
                                    col.equity = col.equity - brockerage
                                    try:
                                        col.save()
                                    except IntegrityError:
                                        return HttpResponse('Positions cannot be opened')
                                    amount_now = req*(-1)
                                    pr_now = brockerage*(-1)
                                    
                                    try:
                                        row1 = WudOpenedOptions.objects.get(bors='Buy', stockname=values['stockname'], tranprice=currentprice, triggerprice=triggerprice, userid=user)
                                        row1.quantity += quantity
                                        row1.save()
                                    except:
                                        new = WudOpenedOptions(userid = user, trandateandtime = datetime.now(), stockname = values['stockname'], bors = values['bors'], quantity = quantity, tranprice = currentprice, triggerprice = triggerprice)
                                        new.save()
                                    trademsg = "<div align='center'><font color='#2A3F00'>You have successfuly transacted <b>"+str(req)+"</b> on "+values['stockname']+" inclusive of Rs. <b>"+str(brockerage)+"</b> of brockerage</font></div>"
                        else:
                            que = WudRankings.objects.filter(userid=user)
                            for each in que:
                                marginfrac = 0.5
                                brockerage = br*quantity
                                req = quantity*currentprice+brockerage
                                reqamt =  quantity*currentprice
                                pen1 = reqamt*marginfrac
                                pen = reqamt*marginfrac + brockerage
                                
                                if each.reserve < req:
                                    quan = int(each.reserve/currentprice)
                                    trademsg = "You dont have enough reserve"
                                    trademsg += "You can only short sell <b>"+str(quan)+"</b> stocks"
                                else:
                                    if pen == 0 or pen == '' or pen < 0:
                                        return HttpResponse('The order cannot be placed')
                                    if currentprice == 0 or currentprice == '' or currentprice < 0:
                                        return HttpResponse('The order cannot be placed')
                                    each.reserve = each.reserve - pen
                                    each.equity = each.equity - brockerage
                                    try:
                                        each.save()
                                    except IntegrityError:
                                        return HttpResponse('Position cannot be opened')
                                    amount_11 = pen*(-1)
                                    br_no = brockerage*(-1)
                                    
                                    each.margin += pen1
                                    each.save()
                                    
                                    if pen1 == '' or pen1 == 0:
                                        return HttpResponse('Cannot place order with margin of ZERO')
                                    new1 = WudOpenedOptions(userid = user, trandateandtime = datetime.now(), stockname= values['stockname'], bors= values['bors'], quantity= quantity, tranprice= currentprice, triggerprice= triggerprice, margin= pen1)
                                    
                                    try:
                                        new1.save()
                                    except IntegrityError, DatabaseError:
                                        return HttpResponse('Position cannot be opened')
                                    trademsg = "<div align='center'><font color='#2A3F00'>You have successfuly short sold Rs <b>"+str(pen)+"</b> on stock holdings of "+values['stockname']+" inclusive of Rs. <b>"+str(brockerage)+"</b> as brockerage</font></div>"
                    elif pendingprice != '' and triggerprice == '':
                        triggerprice = 0;
                        if valid_pp(pendingprice):
                            check = 0
                            pendingprice = float(pendingprice)
                            if values['bors'] == 'Buy':
                                if currentprice >= pendingprice :
                                    check = 1
                                else:
                                    trademsg = "Pending price("+str(pendingprice)+") must be <b>lower</b> that the current price("+str(currentprice)+") to place a '<b>Buy</b>' order"
                            else:
                                if currentprice <= pendingprice :
                                    check = 1
                                else:
                                    trademsg = "Pending price("+str(pendingprice)+") must be <b>higher</b> that the current price("+str(currentprice)+") to place a '<b>Short sell</b>' order"
                            if check == 1 :
                                que = WudPendingOptions(userid = user, stockname= values['stockname'], quantity = quantity, bors = values['bors'], pendingprice = pendingprice, triggerprice = triggerprice, time=datetime.now())
                                que.save()
                                trademsg = "<div align='center'><font color='#2A3F00'>Your order was successfully placed and will be successfully executed when the market price reaches the applied price</font></div>"
                        else:
                            print "enter a valid pending price"
                    elif pendingprice != '' and triggerprice != '':
                        if valid_pp(pendingprice) and valid_pp(triggerprice):
                            check = 0
                            check2 = 0
                            pendingprice = float(pendingprice)
                            triggerprice = float(triggerprice)
                            if values['bors'] == 'Buy':
                                if currentprice >= pendingprice :
                                    check = 1
                                else:
                                    trademsg = "Pending price("+str(pendingprice)+") must be <b>lower</b> than the current price("+str(currentprice)+") to place a '<b>Buy</b>' order"
                                if currentprice > triggerprice :
                                    check2 = 1
                                else:
                                    trademsg = "Trigger price ("+str(triggerprice)+") must be <b>lower</b> than the current price("+str(currentprice)+") to place a '<b>Buy</b>' order"
                            else:
                                if currentprice <= pendingprice:
                                    check = 1
                                else:
                                    trademsg = "Pending price("+str(pendingprice)+") must be <b>higher</b> that the current price("+str(currentprice)+") to place a '<b>Short sell</b>' order"
                                if currentprice <= triggerprice : 
                                    check2 = 1
                                else:
                                    trademsg = "Trigger price ("+str(triggerprice)+") must be <b>higher</b> than the current price("+str(currentprice)+") to place a '<b>Shot sell</b>' order"
                            if check==1 and check2==1 :
                                que = WudPendingOptions(userid = user, stockname = values['stockname'], quantity = quantity, bors = values['bors'], pendingprice = pendingprice, triggerprice = triggerprice)
                                #if not que.save():
                                    #DatabaseError #Exception('Positions cacnnot be opened')
                                trademsg = "<div align='center'><font color='#2A3F00'>Your order was successfully placed and will be successfully executed when the market price reaches the applied price</font></div>"
                        else:
                            print 'Invalid number entered'
                    elif pendingprice == '' and triggerprice != '' :
                        if(valid_pp(triggerprice)):
                            check = 0
                            triggerprice = float(triggerprice)
                            if values['bors'] == 'Buy':
                                if currentprice > triggerprice :
                                    check = 1
                                else:
                                    trademsg = "Trigger price ("+str(triggerprice)+") must be <b>lower</b> than the current price("+str(currentprice)+") to place a '<b>Buy</b>' order"
                                if check == 1:
                                    que = WudRankings.objects.filter(userid = user)
                                    for query in que:
                                        print quantity
                                        brockerage = quantity*br
                                        req = quantity*currentprice + brockerage
                                        if query.reserve < req :
                                            quan = int(each.reserve/currentprice)
                                            trademsg = "You dont have enough reserve"
                                            trademsg += "You can only buy <b>"+quan+"</b> stocks"
                                        else:
                                            query.reserve = query.reserve - req
                                            query.equity = query.equity - brockerage
                                            query.save()
                                            p = req*(-1)
                                            q = brockerage*(-1)
                                            que2 = WudOpenedOptions(userid= user,trandateandtime= datetime.now(),stockname= values['stockname'],bors= values['bors'],quantity= quantity,tranprice= currentprice,triggerprice= triggerprice)
                                            que2.save()
                                            #return HttpResponse('Cannot process your request')
                                            trademsg = "<div align='center'><font color='#2A3F00'>You have successfuly transacted <b>"+str(req)+"</b> on "+values['stockname']+" inclusive of Rs. <b>"+str(brockerage)+"</b> of brockerage</font></div>"
                            else:
                                if currentprice <= triggerprice :
                                    check = 1
                                else:
                                    trademsg = "Trigger price ("+str(triggerprice)+") must be <b>higher</b> than the current price("+str(currentprice)+") to place a '<b>Short sell</b>' order"
                                if check == 1:
                                    que = WudRankings.objects.get(userid=user)
                                    if que:
                                        marginfrac = 0.5
                                        brockerage = br*quantity
                                        req = quantity*currentprice + brockerage
                                        reqamt = quantity*currentprice
                                        pen1 = reqamt*marginfrac
                                        pen = reqamt*marginfrac+brockerage
                                        if que.reserve < req:
                                            quan = int(que.reserve/currentprice)
                                            trademsg = "You dont have enough reserve"
                                            trademsg += "You can only short sell <b>"+str(quan)+"</b> stocks"
                                        else:
                                            que.reserve = que.reserve-pen
                                            que.equity = que.equity-brockerage
                                            que.margin += pen1
                                            try:
                                                que.save()
                                            except IntegrityError, DatabaseError:
                                                return HttpResponse('Position cannot be opened')
                                            amount_11 = pen*(-1)
                                            br_no = brockerage*(-1)
                                                
                                            que2 = WudOpenedOptions(userid= user,trandateandtime= datetime.now(),stockname= values['stockname'],bors= values['bors'],quantity= quantity,tranprice= currentprice,triggerprice= triggerprice,margin= pen1)
                                            try:
                                                que2.save()
                                            except IntegrityError, DatabaseError:
                                                return HttpResponse('Position cannot be opened')
                                            trademsg = "<div align='center'><font color='#2A3F00'>You have successfuly short sold Rs <b>"+str(pen)+"</b> on stock holdings of "+values['stockname']+" inclusive of Rs. <b>"+str(brockerage)+"</b> as brockerage</font></div>"
                        else:
                            trademsg='Please enter a valid trigger price'
                else:
                    trademsg='Please enter a valid digit'
        else:
            trademsg='Please enter a valid quantity'
    else:
        trademsg='You cant trade stock with 0 share price'
    ctx = { 'msg' : trademsg}
    return HttpResponse(trademsg)
    

@login_required
def sterminal(request):
    user = request.user.username
    tradeallow = 1
    nameval = {}
    nameval['Buy'] = 'Sell'
    nameval['Short Sell'] = 'Cover Short'
    ##############OPENED OPTIONS#############
    open = dict()
    que = WudOpenedOptions.objects.filter(userid=user).order_by('stockname')
    open['count'] = que.count()
    open['query'] = que
    i = 0
    for q in que:
        if i == 0:
            open['class'] = [sterminal_wud_opened(q)]
        else:
            open['class'].append(sterminal_wud_opened(q))
        i = i+1
    if que.count():
        open['zipped'] = zip(que, open['class'])
    op = datetime.now()
    open['op'] = op
        
    
    #############PENDING CLOSE OPTIONS############
    pend_close = dict()
    query = WudPendingCloseOptions.objects.filter(userid= user).order_by('stockname')
    pend_close['count'] = query.count()
    pend_close['query'] = query
    i = 0
    for q in query:
        if i == 0:
            pend_close['class'] = [sterminal_pend_closed(q)]
        else:
            pend_close['class'].append(sterminal_pend_closed(q))
        i = i+1
    if query.count():
        pend_close['zipped'] = zip(query, pend_close['class'])
    
    #############PENDING OPTIONS##################
    pending = {}
    query = WudPendingOptions.objects.filter(userid= user)
    count = query.count()
    pending['count'] = count
    pending['query'] = query
    i = 0
    for q in query:
        if i == 0:
            pending['class'] = [sterminal_pending(q)]
        else:
            pending['class'].append(sterminal_pending(q))
        i = i+1
    if query.count():
        pending['zipped'] = zip(query, pending['class'])
    
    
    ctx = { 'openctx' : open, 'pendclose' : pend_close , 'pending' : pending , 'nameval' : nameval ,'tradeallow' : tradeallow, 'zero' : 0 }
    return render_to_response('stocks/sterminal.html', ctx, context_instance = RequestContext(request))

@csrf_exempt
@login_required
def close(request):
    user = request.user.username
    tradeallow = globalvars.tradeallow
    que = WudRankings.objects.filter(userid=user)
    if que.count() == 0:
        return HttpResponse('Sorry :(')
    
    values = request.POST
    orderno = MySQLdb.escape_string(values['orderno'])
    bors = MySQLdb.escape_string(values['bors'])
    check_q = WudOpenedOptions.objects.filter(orderno=orderno)
    if check_q.count() == 0:
        return HttpResponse('Order is no longer valid')
    for check_a in check_q:
        stockname = check_a.stockname
        quantity = int(MySQLdb.escape_string(values['quantity']))
        if quantity > 10000:
            return HttpResponse('You cannot have more than 10,000 shares of same company')
        oldquantity = int(check_a.quantity)
        margin = float(check_a.margin)
        try:
            pendingprice = float(values['pendingprice'])
        except:
            pendingprice = 0
        newquantity = oldquantity - quantity
        sellprice = float(current_price(stockname))
        br = float(brockerage_fun(user, sellprice))
        brockerage = quantity*br
        amount = quantity*sellprice
        cp = float(check_a.tranprice)
        if sellprice=='':
            return HttpResponse('Order cannot be executed')
        mp = float(check_a.margin/check_a.quantity)*quantity
        newMargin = margin-mp
        pr_now = (sellprice - cp)*quantity
        
        if check_a.userid != user :
            return HttpResponse('Order not valid')
        if check_a.stockname != stockname:
            return HttpResponse('Order not valid')
        if check_a.quantity < quantity:
            return HttpResponse('Ordeer not valid')
        if check_a.quantity > 10000:
            return HttpResponse('You cannot have more than 10000 shares of same company')
    
    if quantity <= 0:
        return HttpResponse('Please enter a valid quantity')
    if not valid_quantity(str(quantity)):
        return HttpResponse('Please enter a valid quantity')
    if pendingprice != 0:
        if not valid_pp(str(pendingprice)):
            return HttpResponse('Please enter a valid pending price')
    if tradeallow == 1:
        if pendingprice == 0:
            if bors == 'Buy':
                if newquantity != 0:
                    q1 = WudOpenedOptions.objects.get(bors='Buy', stockname=stockname, orderno=orderno, userid=user)
                    q1.quantity = newquantity
                    q1.save()
                else:
                    q1 = WudOpenedOptions.objects.get(bors='Buy', stockname=stockname, orderno=orderno, userid=user)
                    q1.delete()
                q2 = WudRankings.objects.get(userid=user)
                q2.reserve = q2.reserve+amount
                q2.equity = q2.equity+pr_now
                q2.save()
                
                q3 = WudClosedOptions(userid=user, trandateandtime= datetime.now(), stockname=stockname, bors='Sell', quantity=quantity, tranprice=cp, brockerage=brockerage, profit=pr_now, closecp=sellprice )
                try:
                    q3.save()
                except DatabaseError:
                    return HttpResponse('Sorry..Error occured')
            else:
                if newquantity != 0:
                    frac = float(newquantity/oldquantity)
                    if margin == '':
                        margin = check_a.margin
                    margin1 = margin*frac
                    add_margin = (1-frac)*margin
                    q1 = WudOpenedOptions.objects.get(bors='Short Sell', orderno=orderno, stockname=stockname, userid=user)
                    q1.quantity = newquantity
                    q1.margin = margin1
                    que = WudRankings.objects.get(userid=user)
                    que.margin = que.margin-add_margin
                    que.save()
                else:
                    add_margin = margin
                    if add_margin == '':
                        add_margin = check_a.margin
                    q1 = WudOpenedOptions.objects.get(bors='Short Sell', stockname=stockname, orderno=orderno, userid=user)
                    q1.delete()
                    que = WudRankings.objects.get(userid=user)
                    que.margin = que.margin - add_margin
                    que.save()
                
                if add_margin == '' or add_margin == 0:
                    return HttpResponse('Order cannot be executed')
                porl = 0
                porl = add_margin*2 - amount
                porl = round(porl,2)
                q2 = WudRankings.objects.get(userid=user)
                q2.reserve = q2.reserve+add_margin+porl
                q2.equity = q2.equity+porl
                try:
                    q2.save()
                except DatabaseError:
                    return HttpResponse('Could not process your request')
                am_now = add_margin+porl
                
                q3 = WudClosedOptions(userid=user, trandateandtime=datetime.now(), stockname=stockname, bors='Short Cover', quantity=quantity, tranprice=cp, brockerage=brockerage, closecp=sellprice, profit=porl )
                try:
                    q3.save()
                except DatabaseError:
                    return HttpResponse('Sorry..Error occured')
        else:
            if bors == 'Buy':
                if newquantity != 0:
                    q1 = WudOpenedOptions.objects.get(bors='Buy', stockname=stockname, orderno=orderno, userid=user)
                    q1.quantity = newquantity
                    q1.save()
                else:
                    q1 = WudOpenedOptions.objects.get(bors='Buy', stockname=stockname, orderno=orderno, userid=user)
                    q1.delete()
                query = WudPendingCloseOptions(userid=user, trandateandtime=datetime.now(), stockname=stockname, bors='Buy', quantity=quantity, pendingprice=pendingprice, tranprice=cp, time=datetime.now())
                query.save()
            else:
                if newquantity != 0:
                    q1 = WudOpenedOptions.objects.get(bors='Short Sell', orderno=orderno, stockname=stockname, userid=user)
                    q1.quantity = newquantity
                    q1.margin = newMargin
                    q1.save()
                else:
                    q1 = WudOpenedOptions.objects.get(bors='Short Sell', orderno=orderno, stockname=stockname, userid=user)
                    q1.delete()
                query = WudPendingCloseOptions(userid=user, trandateandtime=datetime.now(), stockname=stockname, bors='Short Sell', quantity=quantity, pendingprice=pendingprice, tranprice=cp, margin=mp, time=datetime.now())
                query.save()
                
        return HttpResponse('Successfully processed your request')
    else:
        return HttpResponse('Something Wrong')

@csrf_exempt
@login_required
def cancelbuyorder(request):
    user = request.user.username
    values = request.POST
    pid = values['pendingid']
    qqq = values['stockname']
    que = WudPendingOptions.objects.get(userid=user, pendingid=pid, stockname=qqq)
    que.delete()
    return HttpResponse('The order has been cancelled succesfully')

@csrf_exempt
@login_required
def cancelpendingorder(request):
    user = request.user.username
    
    if not request.POST:
        return HttpResponse('Prohibited Page')
    else:
        values = request.POST
        orderno = MySQLdb.escape_string(values['orderno'])
        stockname = MySQLdb.escape_string(values['stockname'])
        bors = MySQLdb.escape_string(values['bors'])
        currentprice = current_price(stockname)
        que = WudPendingCloseOptions.objects.filter(orderno=orderno, stockname=stockname, bors=bors, userid=user)
        
        for q in que:
            quantity = q.quantity
            currentprice = q.tranprice
            
            que1 = WudOpenedOptions(userid=user, trandateandtime=datetime.now(), stockname=stockname, bors=q.bors, quantity=quantity, tranprice=currentprice, triggerprice=0)
            que1.save()
            
            que2 = WudPendingCloseOptions.objects.get(orderno=orderno, stockname=stockname, bors=bors, userid=user)
            que2.delete()
        
        return HttpResponse('Request Successful')

@login_required
def shistory(request):
    user = request.user.username
    ctx = {}
    if 't' in request.GET:
        t = request.GET['t']
    else:
        t = 0
    no = 10
    if t == '':
        t = 0
    next = int(t)+int(no)
    que = WudClosedOptions.objects.filter(userid=user).order_by('orderno')[t:next] 
    print('shistory count = '+str(que.count()))
    ctx['next'] = int(t)+int(no)
    ctx['prev'] = int(t)-int(no)
    ctx['no'] = 10
    ctx['count'] = que.count()
    ctx['query'] = que
    
    
    return render_to_response('stocks/shistory.html', { 'ctx' : ctx }, context_instance=RequestContext(request))


@login_required
def ftrade(request):
    if(globalvars.tradeallow != 1):
        return HttpResponse('Woodstock is now closed')
    ctx = {}
    #f = urllib2.urlopen('http://bazaar.ktj.in/static/files/futures/fstock.txt')
    #cont = f.read()
    #stock = cont.split('~')
    stock = FuturesBase.objects.all().order_by('stockid')
    p=2
    q=3
    opt = {}
    #print(stock[p])
    #while stock[p] != "":
    for s in stock:
        name = s.javaname.split('.')[0]
        try:
            qu = StocksRate.objects.get(stock=name)
            opt[qu.compstatname] = qu.price
        except:
            continue
        #p = p+2
        #q = q+2
    
    ctx['opt'] = opt
    ctx['friday'] = friday.day
    
    stocks = []
    que = FuturesBase.objects.all().order_by('stockid')
        
    for q in que:
        name = q.javaname
        namechunk = name.split('.')
        namenew = namechunk[0]
        
        query = StocksRate.objects.filter(stock=namenew)
        stock = {}
        stock['name'] = name
        for que2 in query:
            stock['stock'] = que2.stock
            stock['price'] = que2.price
        stocks.append(stock)
    
    f2 = urllib2.urlopen('http://bazaar.ktj.in/static/files/stats.txt')
    cont2 = f2.read()
    stock = cont2.split('~')
    
    ctx['opt2'] = stock
    lots=range(1,101)

    return render_to_response('futures/ftrade.html', {'ctx': ctx,'stocks': stocks,'lots':lots}, context_instance=RequestContext(request))

@csrf_exempt
@login_required
def futuresexe(request):
    user = request.user.username
    tradeallow = globalvars.tradeallow
    print('tradeallow is '+str(tradeallow))
    if not request.POST:
        return HttpResponse('Prohibited')
    
    values = request.POST
    quantity = MySQLdb.escape_string(values['quantity'])
    if quantity < 0 :
        return HttpResponse('Order cannot be placed')
    if not valid_quantity(quantity):
        return HttpResponse('Please enter a valid quantity')
    
    margin = MySQLdb.escape_string(values['margin'])
    if not (margin == '0.25' or margin == '0.50' or margin == '0.40' or margin == '0.30'):
        return HttpResponse('Please enter a valid margin')
    
    expiry = MySQLdb.escape_string(values['expiry'])
    if not expiry == friday.day :
        return HttpResponse('Please Enter a valid Expiry Date')
    
    stockname = MySQLdb.escape_string(values['stockname'])
    bors = MySQLdb.escape_string(values['bors'])
    cont = 0
    
    if (bors == 'Long' or bors == 'Short'):
        cont = 1
    if cont != 1:
        return HttpResponse('Positions cannot be opened')
    
    id = user
    triggerprice = MySQLdb.escape_string(values['triggerprice'])
    if triggerprice != '':
        if not valid_pp(triggerprice):
            return HttpResponse('Please Enter a valid triggerprice')
    
    if tradeallow != 1:
        return HttpResponse('The Stock Exchange Market is closed <br> You cannot trade stock now but can only view your opened stock positions.')
    
    status = 1
    stock_limit = stockname+".NS"
    limit_query = FuturesOpenedOrClosed.objects.filter(userid=user,stock=stock_limit,status=status)
    num = limit_query.count()
    quantity_limit = 0
    
    if num != 0:
        for que in limit_query:
            quantity_limit = quantity_limit+int(que.quantity)
    quantity_limit = quantity_limit+int(quantity)
    if quantity_limit > 10000:
        return HttpResponse('You cannot hold more than 10,000 stocks of a company at a time')
    
    query = WudRankings.objects.filter(userid=user)
    if (query.count() == 0):
        return HttpResponse('Sorry...something wrong'+user)
    for q in query:
        if q.reserve>q.equity:
            return HttpResponse('You cannot place order now. Your account needs to be approved')
        
        week = 2
        initialbalance = q.cashbalance
        reserve = q.reserve
        reserve_init = reserve
        equity = q.equity
        marginA = q.margin
        stockname = stockname+'.NS'
            
        query2 = FuturesBase.objects.filter(javaname=stockname)
        ch = query2.count()
        
        if ch==0:
            return HttpResponse('No such stock exists')
        for que2 in query2:
            cp = que2.contractprice2
            if cp < 0 or cp == '':
                return HttpResponse('Order cannot be placed now')
            if margin == 0 or margin == '':
                return HttpResponse('Order cannot be placed')
            
            transaction = float(margin)*float(cp)*float(quantity)
            if transaction > reserve:
                return HttpResponse('You do not have sufficient balance to make this transaction!!')
            
            reserve = reserve - transaction
            if not reserve < reserve_init:
                return HttpResponse('Order cannot be placed')
            
            marginA = marginA+transaction
            if triggerprice=='':
                triggerprice = 0
                query3 = FuturesOpenedOrClosed(userid=user,stock=stockname,quantity=quantity,margin=margin,expiry=expiry,expiryno=week,lors=bors,opencp=cp,profitcp=cp,time=datetime.now(),triggerprice=triggerprice)
                query3.save()
            else:
                if valid_pp(triggerprice):
                    check = 1
                    if bors == 'Long':
                        if(float(cp) < float(triggerprice)):
                            print(cp)
                            print(triggerprice)
                            check = 0
                            return HttpResponse('Trigger price('+str(triggerprice)+') must be <b>lower</b> than the current price('+str(cp)+') to place a "<b>Long</b>" order')
                        else:
                            query4 = FuturesOpenedOrClosed(userid=user,stock=stockname,quantity=quantity,margin=margin,expiry=expiry,expiryno=week,lors=bors,opencp=cp,profitcp=cp,time=datetime.now(),triggerprice=triggerprice)
                            query4.save()
                    else:
                        if(float(cp) > float(triggerprice)):
                            return HttpResponse('Trigger price('+str(triggerprice)+') must be <b>higher</b> than the current price('+str(cp)+') to place a "<b>Short</b>" order')
                        else:
                            query5 = FuturesOpenedOrClosed(userid=user,stock=stockname,quantity=quantity,margin=margin,expiry=expiry,expiryno=week,lors=bors,opencp=cp,profitcp=cp,time=datetime.now(),triggerprice=triggerprice)
                            query5.save()
                else:
                    return HttpResponse('Please enter a valid trigger price')
            print('reached msgggggg')
            msg = 'Transaction of Rs.'+str(transaction)+' Successful!'
                
            query6 = WudRankings.objects.filter(userid=user)
            for que6 in query6:
                que6.reserve = reserve
                que6.margin = marginA
                que6.save()
            
            zero = 0
            p = (-1)*transaction
            
    return HttpResponse(msg)


@csrf_exempt
@login_required
def getstats(request):
    print('entered getstats')
    companyname = request.GET['companyname']
    if not companyname[-3:] == '.NS':
        companyname = companyname+'.NS'
    
    ctx = {}
    ctx['companyname'] = companyname
    ctx['namelower'] = companyname.lower()
    return render_to_response('getstats.html',{'ctx': ctx},context_instance=RequestContext(request))

@csrf_exempt
@login_required                             
def futurestats(request):
    print('entered futurestats')
    try:
        values = request.GET
        com = values['companyname']
    except NameError:
        return HttpResponse('Invalid Entry')
    ctx = {}
    if globalvars.tradeallow == 1:
        company=MySQLdb.escape_string(values['companyname'])
        companychunk = company.split('.')
        companynew = companychunk[0]
        
        ctx['company'] = company
        ctx['companynew'] = companynew
        ctx['friday'] = friday.day
        query = FuturesBase.objects.filter(javaname=company)
        co = query.count()
        if co == 0:
            return HttpResponse('No such stock exists')
        for q in query:
            startprice2 = q.startprice2
            contractprice2 = q.contractprice2
        ctx['contractprice2'] = contractprice2
    else:
        return HttpResponse('Woodstock is now closed')
    
    return render_to_response('futures/futurestats.html', {'ctx': ctx},context_instance=RequestContext(request))


@login_required
def fterminal(request):
    user = request.user.username
    status = 1
    ctx = {}
    t = ''
    if request.GET:
        t = int(request.GET['t'])
    no = 10
    if t=='':
        t=0
    next = int(t)+int(no)
    query = FuturesOpenedOrClosed.objects.filter(userid=user,status=status).order_by('orderid')[t:next]
    ctx['no'] = no
    ctx['prev'] = t-no
    ctx['next'] = t+no
    ctx['count'] = query.count()
    print(ctx['count'])
    ctx['counter'] = 0
    ctx['now'] = timenow()
    print(ctx['now'])
    datas = []
    data = {}
    
    for que in query:
        data['query'] = que
        timethen = que.time
        timedelta = datetime.now()-datetime.strptime(timethen, '%Y-%m-%d %H:%M:%S.%f')
        delta = timedelta.days*86400+timedelta.seconds
        data['timedelta'] = delta
        print(delta)
        data['idvar'] = "fut"+str(ctx['count'])
        data['margin'] = que.margin*100
        data['namechunk'] = que.stock.split('.')
        data['name'] = data['namechunk'][0]
        data['triggerprice'] = que.triggerprice
        if data['triggerprice'] == 0:
            data['triggerprice'] = "None"
        data['stockname'] = que.stock
        data['expno'] = que.expiryno
        que2 = FuturesBase.objects.filter(javaname=data['stockname'])
        for q2 in que2:
            data['currentcp'] = q2.contractprice2
        datas.append(copy.deepcopy(data))
        ctx['counter'] = ctx['counter']+1
    
    return render_to_response('futures/fterminal.html',{'ctx': ctx,'datas': datas,'tradeallow': globalvars.tradeallow},context_instance=RequestContext(request))


@login_required
def fclose(request):
    if not request.POST:
        return HttpResponse('You are trying to access a prohibited page')
    user = request.user.username
    
    values = request.POST
    orderid = MySQLdb.escape_string(values['orderid'])
    
    if not valid_quantity(orderid):
        return HttpResponse('Invalid Order')
    
    query = WudRankings.objects.filter(userid=user)
    for arr in query:
        balance = arr.cashbalance
        reserve = arr.reserve
        margina = arr.margin
        
    query2 = FuturesOpenedOrClosed.objects.filter(orderid=orderid)
    count2 = query2.count()
    
    if count2 == 0:
        return HttpResponse('Position has already been taken')
    
    for arr2 in query2:
        if arr2.userid != user:
            return HttpResponse('Invalid Order')
        stock = arr2.stock
        quantity = arr2.quantity
        if quantity > 10000:
            return HttpResponse('You cannot have more than 10,000 shares of a company')
        margin = arr2.margin
        opencp = arr2.opencp
        profitcp = arr2.profitcp
        lors = arr2.lors
        expiry = arr2.expiry
        expiryno = arr2.expiryno
        time = arr2.time
    
    query3 = FuturesBase.objects.filter(javaname=stock)
    for arr3 in query3:
        closecp = arr3.contractprice2
    if lors == 'Long':
        flag = 1
    if lors == 'Short':
        flag = -1
    profit = (closecp - opencp)*flag*quantity
    ret = margin*quantity*opencp
    if quantity > 10000:
        return HttpResponse('You cannot have more than 10,000 shares of a company')
    if profit >= 0:
        msg = "<div align='center'><font color='#2A3F00'>You earned a profit of Rs.&nbsp;"+str(profit)+"</font></div>"
    if profit < 0:
        msg="<div align='center'><font color='#FF3333'>You earned a loss of Rs.&nbsp;"+str(profit)+"</font></div>";
    
    query4 = FuturesOpenedOrClosed.objects.filter(orderid=orderid)
    for q in query4:
        q.profitcp = closecp
        q.profit = profit
        q.status = 0
        q.save()
    reserve = reserve + profit + ret
    p = profit + ret
    margina = margina - ret
    
    query5 = WudRankings.objects.filter(userid=user)
    for q in query5:
        q.reserve = reserve
        q.equity = q.equity+profit
        q.margin = margina
        q.save()
    
    return HttpResponse(msg)

@login_required
def fhistory(request):
    user = request.user.username
    status = 0
    t = ""
    ctx={}
    no = 10
    ctx['no'] = no
    
    if request.GET:
        t = int(request.GET['t'])
    if t == "":
        t = 0
    next = int(t)+int(no)
    query = FuturesOpenedOrClosed.objects.filter(userid=user,status=status).order_by('orderid')[t:next]
    count = 0
    data = {}
    datas = []
    
    for arr in query:
        data['query'] = arr
        data['margin'] = arr.margin*100
        namechunk = arr.stock.split('.')
        data['name'] = namechunk[0]
        count = count+1
        datas.append(copy.deepcopy(data))
    
    ctx['count'] = count
    ctx['prev'] = t-no
    ctx['next'] = t+no
    
    return render_to_response('futures/fhistory.html',{'ctx':ctx, 'datas':datas},context_instance=RequestContext(request))

@login_required
def otrade(request):
    if globalvars.tradeallow == 2:
        return HttpResponse('Woodstock is closed now. <br> You cannot trade stock now but can only view your opened stock positions.You can play <a href="/forex/">forex</a> till 12 midnight.')
    if globalvars.tradeallow == 0:
        return HttpResponse('Both Woodstock and Forex are closed')
    
    que = OptionsBase.objects.all().order_by('stock')[0]
    spriceA = que.strikeprice
    spricechunk = spriceA.split('~')
    ctx = {}
    strikes = []
    for each in spricechunk:
        strikes.append(each)
    ctx['strikes'] = strikes
    ctx['friday'] = friday.day
    
    datas = []
    data = {}
    query = OptionsBase.objects.all().order_by('stockid')
    for arr in query:
        data['name'] = arr.javaname
        data['strike'] = arr.strikeprice
        namechunk = data['name'].split('.')
        namenew = namechunk[0]
        try:
            query3 = StocksRate.objects.filter(stock=namenew)
            
            for q in query3:
                data['stockname'] = q.stock
                data['price'] = round(q.price, 2)
        
            datas.append(copy.deepcopy(data))   
        except:
            return HttpResponse('Error')
    try:
        f = urllib2.urlopen('http://bazaar.ktj.in/static/files/stats.txt')
    except:
        return HttpResponse('file error')
    home = f.read()
    stock = home.split('~')
    
    ctx['stocks'] = stock
    lots=range(1,101)
      
    return render_to_response('options/otrade.html',{'ctx':ctx, 'datas':datas,'lots':lots},context_instance=RequestContext(request))   

@csrf_exempt
@login_required
def optionsexe(request):
    user = request.user.username
    if not request.POST:
        return HttpResponse('Prohibited Page')
    if not globalvars.tradeallow == 1:
        return HttpResponse('Trade is not allowed now')
    
    values = request.POST
    name = MySQLdb.escape_string(values['company'])
    quantity = MySQLdb.escape_string(values['quantity'])
    
    if quantity<0:
        return HttpResponse('Order cannot be placed')
    if not valid_quantity(quantity):
        return HttpResponse('Please enter a valid quantity')
    
    porc = values['porc']
    if not (porc == 'Put' or porc == 'Call'):
        return HttpResponse('Please enter a valid call/put option')
    
    expiry = MySQLdb.escape_string(values['expiry'])
    if not expiry == friday.day:
        return HttpResponse('Please enter a valid expiry date')
    
    lors = MySQLdb.escape_string(values['lors'])
    if not (lors == 'Long' or lors == "Short"):
        return HttpResponse('Please enter a valid long/short option')
    
    strikeprice = MySQLdb.escape_string(values['strike'])
    if not valid_pp(strikeprice):
        return HttpResponse('Please enter a valid strike price')
    
    name1 = name.split('.')
    namenow = name1[0]
    namenew = namenow+".NS"
    
    query = OptionsBase.objects.filter(javaname=namenew)
    count = query.count()
    if count == 0:
        return HttpResponse('No such stock exists')
    
    for arr in query:
        strikestring = arr.strikeprice
        strikechunk = strikestring.split('~')
        rank = 0
        while strikechunk[rank] != strikeprice:
            rank = rank+1
        uprice = arr.uprice
        status = 1
        volatilitystring = arr.volatility
        
        if porc == 'Put':
            putpremium = arr.currentput2
            putpremiumchunk = putpremium.split('~')
            premium = putpremiumchunk[rank]
            volatility = volatilitystring
        if porc == 'Call':
            callpremium = arr.currentcall2
            callpremiumchunk = callpremium.split('~')
            premium = callpremiumchunk[rank]
            volatility = volatilitystring
        
        print(premium)
        if premium<0.05:
            premium = 0.05
        
        print('quantityyyyyyy'+quantity)
        paidpremium = float(premium)*int(quantity)
    
    limitquery = OptionsOpenedOrClosed.objects.filter(userid=user,name=namenew,status=status)
    num = limitquery.count()
    quantity_limit = 0
    
    if num != 0:
        for que in limitquery:
            quantity_limit = quantity_limit + int(que.quantity)
        
    quantity_limit = quantity_limit + int(quantity)
    if quantity_limit > 10000:
        return HttpResponse("<div align='center'><font color='#FF3333'>You cannot hold more than 10,000 stocks of a company at a time</font></div>")
    
    query1 = WudRankings.objects.filter(userid=user)
    for q in query1:
        reserve = q.reserve
        equity = q.equity
        margin = q.margin
        q.save()
    if lors == 'Long':
        mar = 0
        reserve1 = float(reserve)-paidpremium
        equity1 = float(equity)-paidpremium
        
        if reserve1 < 0:
            return HttpResponse("<div align='center'><font color='#FF3333'>You cannot do this transaction as you do not have sufficient Cash in Hand.</font></div>")
    else:
        mar = 0.5*int(quantity)*float(strikeprice)
        if quantity < 0:
            return HttpResponse('Quantity cannot be negative')
            
        reserve1 = reserve+paidpremium-mar
        equity1 = equity+paidpremium
        margin =  margin+mar
        short = 'Short'
        if mar > reserve:
            return HttpResponse("You don't have sufficient balance to pay as margin for the order.")
        if reserve1 < 0:
            return HttpResponse("You don't have sufficient cash to pay as margin.")
        
    query3 = WudRankings.objects.filter(userid=user)
    for q in query3:
        q.reserve = reserve1
        q.equity = equity1
        q.margin = margin
        q.save()
    msg = ""
    
    if lors == "Long":
        msg = "<div align='center'><font color='#2A3F00'>Rs "+str(paidpremium)+" has been debited from your account.</font></div>"
    else:
        msg = "<div align='center'><font color='#2A3F00'>Rs "+str(paidpremium)+" has been credited to your account.</font></div><div align='center'><font color='#2A3F00'>Rs "+str(mar)+" has been debited from your account as margin.</font></div>"
    
    
    query1 = OptionsOpenedOrClosed(userid=user, name=namenew, quantity=quantity, expiry=expiry, callorput=porc, longorshort=lors, margin=mar, strikeprice=strikeprice, underlyingprice=uprice, premium=paidpremium, volatility=volatility, status=status, date=datetime.now())
    query1.save()
    
    
    return HttpResponse(msg)

@csrf_exempt
@login_required
def optionstats(request):

    try:
        values = request.GET
        com = values['companyname']
    except NameError:
        return HttpResponse('Invalid Entry')

    if globalvars.tradeallow == 1:
        company = MySQLdb.escape_string(values['companyname'])
        companychunk = company.split('.')
        companynew = companychunk[0]
        
        query = OptionsBase.objects.filter(javaname=company)
        ch = query.count()
        
        if ch == 0:
            return HttpResponse('No such stock exists')
        for q in query:
            strikeprice = q.strikeprice
            strikechunks = strikeprice.split('~')
             
            callbweek = q.currentcall2
            callbweekchunk = callbweek.split('~')
             
            putbweek = q.currentput2
            putbweekchunk = putbweek.split('~')
             
            company = q.javaname
        
        zipped = [{'strike': t[0], 'call': t[1], 'put': t[2]} for t in zip(strikechunks, callbweekchunk, putbweekchunk)]
        
        return render_to_response('options/optionstats.html', {'zipped': zipped},context_instance=RequestContext(request))
    
    return HttpResponse('')

@login_required
def ohistory(request):
    user = request.user.username
    print('ohistory enterrrrrrrrrrrrrrrrrrrr')
    t = ""
    
    if request.GET:
        t = int(request.GET['t'])
    no = 10
    if t == "":
        t = 0
    ctx = {}
    ctx['prev'] = t-no
    ctx['next'] = t+no
    status = 0
    count = 0
    next = int(t)+int(no)
    query = OptionsOpenedOrClosed.objects.filter(userid=user,status=status).order_by('transactionid')[t:next]
    print('ohistory count = '+str(query.count()))
    datas = []
    data = {}
    
    for que in query:
        namechunk = que.name.split('.')
        data['name'] = namechunk[0]
        data['query'] = que
        data['if'] = 0
        count = count+1
        if que.premium > que.profit:
            data['if'] = 1
            data['diff'] = que.premium-que.profit
        else:
            data['diff'] = que.profit-que.premium 
        
        datas.append(copy.deepcopy(data))
    
    ctx['count'] = count
    
    return render_to_response('options/ohistory.html', {'ctx':ctx, 'datas':datas},context_instance=RequestContext(request))


@login_required
def oterminal(request):
    
    user = request.user.username
    t = ""
    
    if request.GET:
        t = int(request.GET['t'])
    no = 10
    if t == "":
        t = 0
    ctx = {}
    ctx['prev'] = t-no
    ctx['next'] = t+no
    next = int(t)+int(no)
    query = OptionsOpenedOrClosed.objects.filter(userid=user,status=1).order_by('transactionid')[t:next]
    count = query.count()
    ctx['count'] = count
    
    datas = []
    data = {}
    for q in query:
        data['que'] = q
        stockfull = q.name
        st = stockfull.split('.')
        stockname_java = st[0]
        try:
            q_java = StocksRate.objects.get(stock=stockname_java)
            
            price_current = q_java.price
            data['price_current'] = price_current
        except:
            continue
        status = 0
        loss =0
        profit=0
        
        if q.longorshort == 'Long':
            if q.callorput == 'Call':
                if q.strikeprice<price_current:
                    porl = q.quantity*(price_current - q.strikeprice)-q.premium
                    if porl<0:
                        loss = (-1)*porl
                else:
                    loss = q.premium
            elif q.callorput == 'Put':
                porl = 0
                loss = 0
                status1 = 0
                if q.strikeprice > price_current:
                    porl = (-1)*q.quantity*price_current
                    status1 = 1
                    loss = 0
                    if porl < 0:
                        loss = (-1)*porl
                    else:
                        profit = porl
                else:
                    loss = q.premium
        elif q.longorshort == 'Short':
            if q.callorput == 'Call':
                if q.strikeprice < price_current:
                    porl = (-1)*q.quantity*price_current
                    status1 = 0
                    loss =0 
                else:
                    status1 = 1
                    porl = q.premium
                if porl<0:
                    loss = (-1)*porl
                else:
                    loss = 0
                    profit = porl
            elif q.callorput == 'Put':
                if q.strikeprice > price_current:
                    porl = (-1)*((-1)*q.quantity*price_current)
                    status1 = 0
                    loss = 0
                else:
                    status1 = 1
                    porl = q.premium
                if porl < 0:
                    loss = (-1)*porl
                else:
                    loss = 0
                    profit = porl
        
        if loss == 0:
            color = 'green'
            price_current1 = profit
            profit = 0
        else:
            color = 'red'
            price_current1 = loss
            loss = 0
        data['color'] = color
        data['price_current1'] = round(price_current1,2)
        
        
        op = datetime.now()
        time = q.date
        
        datas.append(copy.deepcopy(data))
    
    return render_to_response('options/oterminal.html', {'ctx': ctx, 'datas': datas, 'tradeallow': globalvars.tradeallow},context_instance=RequestContext(request))

@csrf_exempt
@login_required
def optionclose(request):
    if not request.POST:
        return HttpResponse('Prohibited Page')
    
    user = request.user.username
    msg = ""
    
    query = WudRankings.objects.filter(userid=user)
    values = request.POST
    
    if globalvars.tradeallow == 1:
        orderno = MySQLdb.escape_string(values['transactionid'])
        
        if not valid_quantity(orderno):
            return HttpResponse('Please enter a valid order no')
        
        query1 = OptionsOpenedOrClosed.objects.filter(transactionid = orderno)
        
        for q1 in query1:
            if q1.longorshort == 'Short':
                return HttpResponse('A short option cannot be closseed before expiry')
            
            stockfull = q1.name
            if q1.quantity > 10000:
                return HttpResponse('You cannot have more than 10,000 shares of a company')
            
            st = stockfull.split('.')
            stockname_java = st[0]
            q_java = StocksRate.objects.filter(stock=stockname_java)
            for a_java in q_java:
                price_current = a_java.price
            
            if q1.status == 0:
                return HttpResponse('Position has already been taken')
            if q1.userid != user:
                return HttpResponse('Order not valid')
            
            query = WudRankings.objects.filter(userid=user)
            
            for q in query:
                reserve = q.reserve
            payoff = (price_current-q1.strikeprice)*q1.quantity
            premium = q1.premium
            
            if q1.longorshort == 'Long':
                if q1.callorput == 'Call':
                    if payoff > 0:
                        reserve1 = reserve+payoff    
                        profit = payoff-premium
                        
                        query3 = OptionsOpenedOrClosed.objects.filter(transactionid=orderno)
                        for q3 in query3:
                            q3.profit = payoff
                            q3.save()
                        
                        query4 = WudRankings.objects.filter(userid=user)
                        for q4 in query4:
                            q4.reserve = reserve1
                            q4.equity = q4.equity+payoff
                        
                        msg = "<div align='center'><font color='#2A3F00'>Rs "+str(payoff)+" has been credited to your account.</font></div><br>"
                        if profit >= 0:
                            msg += "<div align='center'><font color='#2A3F00'>You earned a profit of Rs "+str(profit)+" from this option transaction.</font></div>"
                        else:
                            msg += "<div align='center'><font color='#FF3333'>You earned a loss of Rs "+str(profit)+" from this option transaction.</font></div>"
                    else:
                        msg = "<div align='center'><font color='#FF3333'>You earned a loss of Rs "+str(premium)+" from this option transaction.</font></div>"
                if q1.callorput == 'Put':
                    if payoff < 0:
                        payoff2 = -payoff
                        reserve2 = reserve + payoff2
                        profit = payoff2 - premium
                        
                        query3 = OptionsOpenedOrClosed.objects.filter(transactionid = orderno)
                        for q3 in query3:
                            q3.profit = payoff2
                            q3.save()
                        
                        query4 = WudRankings.objects.filter(userid=user)
                        for q4 in query4:
                            q4.reserve = reserve2
                            q4.equity = q4.equity + payoff2
                            q4.save()
                        
                        msg = "<div align='center'><font color='#2A3F00'>Rs  "+str(payoff2)+" has been credited to your account.</font></div><br>"
                        
                        if profit >= 0:
                            msg += "<div align='center'><font color='#2A3F00'>You earned a profit of Rs "+str(profit)+" from this option transaction.</font></div>"
                        else:
                            msg += "<div align='center'><font color='#FF3333'>You earned a loss of Rs "+str(-profit)+" from this option transaction.</font></div>"
                    else:
                        msg = "<div align='center'><font color='#FF3333'>You earned a loss of Rs "+str(premium)+" from this option transaction.</font></div>"
            
            if q1.longorshort == 'Short':
                if q1.callorput == 'Put':
                    if payoff<0:
                        reserve1 = reserve+payoff
                        profit = premium+payoff
                        
                        query3 = OptionsOpenedOrClosed.objects.filter(transactionid=orderno)
                        for q3 in query3:
                            q3.profit = payoff
                            q3.save()
                        
                        msg = "<div align='center'><font color='#2A3F00'>Rs "+str(-payoff)+" has been debited from your account.</font></div><br>"
                        
                        if profit >= 0:
                            msg += "<div align='center'><font color='#2A3F00'>You earned a profit of Rs "+str(profit)+" from this option transaction.</font></div>"
                            pr = profit+premium
                            query4 = WudRankings.objects.filter(userid=user)
                            for q4 in query4:
                                q4.reserve = reserve1
                                q4.equity = q4.equity + payoff
                        else:
                            msg += "<div align='center'><font color='red'>You earned a loss of Rs "+str(-profit)+" from this option transaction.</font></div>"
                    else:
                        msg = "<div align='center'><font color='#2A3F00'>You earned a profit of Rs "+str(premium)+" from this option transaction.</font></div>"
                        query4 = WudRankings.objects.filter(userid=user)
                        for q4 in query4:
                            q4.reserve = q4.reserve+premium
                            q4.equity = q4.equity+premium+payoff
                            q4.save()
                        
                        p = premium + payoff
                
                if q1.callorput == 'Call':
                    if payoff > 0:
                        payoff2 = -payoff
                        reserve2 = reserve + payoff2 + premium
                        profit = premium+payoff2
                        
                        query3 = OptionsOpenedOrClosed.objects.filter(transactionid=orderno)
                        for q3 in query3:
                            q3.profit = payoff
                            q3.save()
                        query4 = WudRankings.objects.filter(userid=user)
                        for q4 in query4:
                            q4.reserve = reserve2
                            q4.equity = q4.equity+profit
                            q4.save()
                        
                        msg = "<div align='center'><font color='#2A3F00'>Rs "+str(payoff)+" has been debited from your account.</font></div><br>"
                        
                        if profit >= 0:
                            msg += "<div align='center'><font color='#2A3F00'>You earned a profit of Rs "+str(profit)+" from this option transaction.</font></div>"
                        else:
                            msg += "<div align='center'><font color='red'>You earned a loss of Rs "+str(-profit)+" from this option transaction.</font></div>"
            
            query2 = OptionsOpenedOrClosed.objects.filter(transactionid = orderno)
            for q2 in query2:
                q2.status = 0
                q2.save()
    
    return HttpResponse(msg)
    
        
                
        
    
            
        
            
        
        