from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'woodstock.views.index', name='index'),
    #url(r'^test/$','woodstock.views.index',name='index'),
    url(r'^help/$', 'woodstock.views.help', name='help'),
    url(r'^credits/$', 'woodstock.views.credits', name='credits'),
    url(r'^learntoplay/$', 'woodstock.views.learntoplay', name='learntoplay'),
    url(r'^rules/$', 'woodstock.views.rules', name='rules'),
    url(r'^interface/$', 'woodstock.views.interface', name='interface'),
    url(r'^trade/$', 'woodstock.views.trade', name='trade'),
    url(r'^stats/$', 'woodstock.views.stats', name='stats'),
    url(r'^stocks_marquee/$', 'woodstock.views.stocks_marquee', name='stocks_marquee'),
    url(r'^toprankers/$', 'woodstock.views.toprankers', name='toprankers'),
    url(r'^strade/$', 'woodstock.views.strade', name='strade'),
    url(r'^sterminal/$', 'woodstock.views.sterminal', name='sterminal'),
    url(r'^shistory/$', 'woodstock.views.shistory', name='shistory'),
    url(r'^postexe/$', 'woodstock.views.postexecute', name='postexe'),
    url(r'^close/$', 'woodstock.views.close', name='close'),
    url(r'^cancelbuy/$', 'woodstock.views.cancelbuyorder', name='cancelbuy'),
    url(r'^cancelpending/$', 'woodstock.views.cancelpendingorder', name='cancelpending'),
    url(r'^ftrade/$', 'woodstock.views.ftrade', name='ftrade'),
    url(r'^fterminal/$', 'woodstock.views.fterminal', name='fterminal'),
    url(r'^futuresexe/$', 'woodstock.views.futuresexe', name='futuresexe'),
    url(r'^getstats/$', 'woodstock.views.getstats', name='getstats'),
    url(r'^futurestats/$', 'woodstock.views.futurestats', name='futurestats'),
    url(r'^fclose/$', 'woodstock.views.fclose', name='fclose'),
    url(r'^fhistory/$', 'woodstock.views.fhistory', name='fhistory'),
    url(r'^otrade/$', 'woodstock.views.otrade', name='otrade'),
    url(r'^ohistory/$', 'woodstock.views.ohistory', name='ohistory'),
    url(r'^oterminal/$', 'woodstock.views.oterminal', name='oterminal'),
    url(r'^optionsexe/$', 'woodstock.views.optionsexe', name='optionsexe'),
    url(r'^optionclose/$', 'woodstock.views.optionclose', name='optonclose'),
    url(r'^optionstats/$', 'woodstock.views.optionstats', name='optionstats'),
    url(r'^info/$','woodstock.views.info', name='info'),
    #url(r'^woodstock/', include('woodstock.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls'),
    # Uncomment the next line to enable the admin:

)
