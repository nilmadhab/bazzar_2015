from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView
from users.forms import ProfileForm,LoginForm,SocialLoginForm,SocialForm
from users.models import Profile,Institute
from django.contrib.auth import logout
from django.http.response import HttpResponse, HttpResponseRedirect, Http404
from django.contrib.auth.models import User
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
import json
from django.shortcuts import render
from index.models import ForexRankings
from data.models import WudRankings

# Create your views here.
def nil(request):
    return HttpResponse("nil th graet")
def check_logged(request):
    data={}
    data['logged']=False
    if request.user.is_authenticated():
        data['logged']=True
    return HttpResponse(json.dumps(data),mimetype="application/json")

def profile_exists(request):
    data={}
    data['logged']=False
    data['exists']=False
    try:
        username=request.POST['social']+'_'+request.POST['username']
        user=User.objects.get(username=username)
        if User.objects.filter(email=request.POST['email']).count():
            data['exists']=True;
        else:
            raise User.DoesNotExist
        profile=Profile.objects.get(user=user)
        if user and profile:  
            data['logged']=True
    except User.DoesNotExist:
        data['exists']=False
    return HttpResponse(json.dumps(data),mimetype="application/json")

class SignupView(FormView):
    template_name = "signup.html"
    form_class = ProfileForm
    
    def get_success_url(self):
        return '/accounts/success'

    def get_context_data(self, **kwargs):
        context = super(SignupView, self).get_context_data(**kwargs)
        return context
    
    def form_valid(self, form):
        """
        If the form is valid, redirect to the supplied URL.
        """
        self.object = form.save()
        return HttpResponseRedirect(self.get_success_url())
    
class FacebookSignupView(SignupView):
    template_name="facebook_signup.html"
    form_class = SocialForm
    
    def get_context_data(self, **kwargs):
        context = super(FacebookSignupView, self).get_context_data(**kwargs)
        return context
    
class GoogleSignupView(SignupView):
    template_name="google_signup.html"
    form_class = SocialForm
    
    def get_context_data(self, **kwargs):
        context = super(GoogleSignupView, self).get_context_data(**kwargs)
        return context
    
    
class LoginView(FormView):
    template_name = "login.html"
    form_class = LoginForm
    
    def get(self,request):
        if(request.user.is_authenticated()):
            return HttpResponseRedirect('/forex');
        else:
            print("not authenticated")
            return super(LoginView, self).get(request)
    
    def get_success_url(self):
        return '/forex'

    def get_context_data(self, **kwargs):
        context = super(LoginView, self).get_context_data(**kwargs)
        return context
    
    def form_valid(self, form):
        success_url = self.get_success_url()
        return form.login(self.request, redirect_url=success_url)
    

class SocialLoginView(FormView):
    template_name = "login.html"
    form_class = SocialLoginForm
    
    def get_success_url(self):
        return '/accounts/login'
    
    def get_fail_url(self):
        return '/accounts/login'

    def get_context_data(self, **kwargs):
        context = super(SocialLoginView, self).get_context_data(**kwargs)
        return context
    
    def form_valid(self, form):
        success_url = self.get_success_url()
        print('Heooo')
        return form.login(self.request, redirect_url=success_url)
    


def logout_ktj(request):
    l=logout(request)
    if(l):
        return HttpResponseRedirect('/accounts/login')
    else:
        return HttpResponseRedirect('/accounts/login')

def profile(request):
    user=request.user
    #try:
    print(user)
    profile=Profile.objects.get(user__pk=user.pk)
    context={'profile':profile,'user':user}
    return render(request,'profile.html',context)
    #except Profile.DoesNotExist:
        #raise Http404
        
def institutes(request,state_val=None):
    if state_val is not 'all':
        institute=Institute.objects.filter(state__state=state_val)
    else:
        institute=Institute.objects.all()
    institutes={}
    for insti in institute:
        institutes[insti.institute]=insti.institute
    if len(institutes)==0:
        institutes['others']="Others"
    return HttpResponse(json.dumps(institutes),mimetype="application/json")

login = LoginView.as_view()
social_login = SocialLoginView.as_view()
signup = SignupView.as_view()
facebook_signup=FacebookSignupView.as_view()
google_signup=GoogleSignupView.as_view()

def success(request):
    return HttpResponse("You have successfully Registered with Kshitij IIT Kharagpur !!!")

def wsfxFinalists(request):
    query = list(set().union(WudRankings.objects.values_list('userid',flat=True), ForexRankings.objects.values_list('userid',flat=True)))
    users = [str(q) for q in query]
    return render(request,'finalists.html',{'users':users})

def closed(request):
    return HttpResponse("hello there")
    