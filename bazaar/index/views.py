from django.views.decorators.vary import vary_on_headers
from index.decorators import bazaar_access
from index.forms import CancelOrderForm, OpenTransaction, PendingTransaction, \
    OpenCommTransaction, CommPendingTransaction, CommCancelOrderForm
from index.models import CommodityBase, ForexBase, ForexPendingOptions, \
    ForexRankings, ForexClosedOptions, CommOpenedOptions, ForexOpenedOptions, \
    CommClosedOptions, CommPendingOptions, Notifications, Users
from index.utils import checkparameters, valid_pp, current_price, calc_profit, \
    find_price, calc_comm_profit, brockerage
from datetime import timedelta
from django.contrib.auth.decorators import login_required
from django.http.request import HttpRequest
from django.http.response import HttpResponse, Http404
from django.shortcuts import render_to_response, render
from django.utils import timezone, formats
from django.utils.dateformat import DateFormat
from django.views.decorators.csrf import csrf_exempt
from httplib import HTTPResponse
from itertools import izip
import algotrade
import json
import os

@login_required
#@bazaar_access()
def commodity_history(request):
    user=request.session['user']
    allow=checkparameters(request)
    if allow==1 or allow==2:
        try:
            start=int(request.GET['t'])
        except:
            start=0
        limit=20
        count=CommClosedOptions.objects.filter(userid=user).count()
        closed=CommClosedOptions.objects.filter(userid=user)[start:start+limit]
        context={'user':user,'closed':closed,'count':count,'prev':(start-limit),'next':(start+limit)}
        return render(request,'forex/commodity-history.html',context)
    else:
        raise Http404

@login_required
#@bazaar_access()
def history(request):
    user=request.user.username
    allow=checkparameters(request)
    if allow==1 or allow==2:
        try:
            start=int(request.GET['t'])
        except:
            start=0
        limit=20
        count=ForexClosedOptions.objects.filter(userid=user).count()
        closed=ForexClosedOptions.objects.filter(userid=user)[start:start+limit]
        print(closed)
        context={'user':user,'closed':closed,'count':count,'prev':(start-limit),'next':(start+limit)}
        return render(request,'forex/history.html',context)
    else:
        raise Http404

@login_required
#@bazaar_access()
@csrf_exempt
def commclosetradeall(request):
    user=request.user.username
    allow=checkparameters(request)
    if allow==1 or allow==2:
        commodity=request.GET['commodity']
        bors=request.GET['bors']
        opened=0;
        try:
            if commodity!='0':
                opened_arr=CommOpenedOptions.objects.filter(userid=user,tradename=commodity,bors=bors)
            else:
                opened_arr=CommOpenedOptions.objects.filter(userid=user)
        except:
            return HttpResponse('Positions have already been closed.')
        
        [opened for opened in opened_arr]
        totalpr=0
        for opened in opened_arr:
            op = timezone.now()
            time = opened.opendateandtime
            if (op-time)>timedelta(seconds=60):
                orderid=opened.orderno;
                commodity=opened.tradename
                lots=opened.quantity
                levearage=opened.levearage
                openprice=opened.openprice
                bors=opened.bors
                opendateandtime=opened.opendateandtime
                if bors=="Short":
                    bs="Long"
                    flag=1
                else:
                    bs="Short"
                    flag=-1
                closepr=find_price(commodity)
                pl=calc_comm_profit(levearage,lots,openprice,commodity,bors)
                br=brockerage(user,closepr)
                totalpr+=pl
                amount=lots*openprice
                #pl = (closepr - openprice)*lots*levearage*flag 
                amountnow = amount + pl
                ranking=ForexRankings.objects.get(userid=user)
                ranking.balance=ranking.balance+amountnow
                ranking.equity=ranking.equity+pl
                ranking.save()
                closed=CommClosedOptions(userid=user,trandateandtime=timezone.now(),tradename=commodity,quantity=lots,bors=bors,tranprice=openprice,brockerage=br,levearage=levearage,profit=pl,closecp=closepr)
                closed.save()
                opened.delete()
    
        if totalpr>0:
            return HttpResponse("<div align='center'><span class='green'>Transactions Closed Successfully with net profit of $ "+str(totalpr)+"</span></div>")
        else:
            return HttpResponse("<div align='center'><span class='red'>Transactions Closed Successfully with net loss of $ "+str(-totalpr)+"</span></div>")


@login_required
#@bazaar_access()
@csrf_exempt
def commclosetrade(request):
    user=request.user.username
    allow=checkparameters(request)
    if allow==1 or allow==2:
        orderid=request.GET['orderid'];
        opened=0;
        try:
            opened=CommOpenedOptions.objects.get(orderno=orderid)
        except:
            return HttpResponse('Position has already been closed.')
        op = timezone.now()
        time = opened.opendateandtime
        if (op-time)>timedelta(seconds=60) and opened.userid==user:
            commodity=opened.tradename
            lots=opened.quantity
            levearage=opened.levearage
            openprice=opened.openprice
            bors=opened.bors
            opendateandtime=opened.opendateandtime
            if bors=="Short":
                bs="Long"
                flag=1
            else:
                bs="Short"
                flag=-1
            closepr=find_price(commodity)
            pl=calc_comm_profit(levearage,lots,openprice,commodity,bors)
            br=brockerage(user,closepr)
            amount=lots*openprice
            #pl = (closepr - openprice)*lots*levearage*flag 
            amountnow = amount + pl
            ranking=ForexRankings.objects.get(userid=user)
            ranking.balance=ranking.balance+amountnow
            ranking.equity=ranking.equity+pl
            ranking.save()
            closed=CommClosedOptions(userid=user,trandateandtime=timezone.now(),tradename=commodity,quantity=lots,bors=bors,tranprice=openprice,brockerage=br,levearage=levearage,profit=pl,closecp=closepr)
            closed.save()
            opened.delete()
        if pl>0:
            return HttpResponse("<div align='center'><font color='#2A3F00'>Transaction Closed Successfully with profit of $ "+str(pl)+"</font></div>")
        else:
            return HttpResponse("<div align='center'><font color='red'>Transaction Closed Successfully with loss of $ "+str(-pl)+"</font></div>")

@login_required
#@bazaar_access()
@csrf_exempt
def comm_terminal_list(request):
    user=request.user.username
    allow=checkparameters(request)
    if allow==1 or allow==2:
        close_allow=[]
        bs={"Long":"Short","Short":"Long"}
        commodity=request.POST['commodity']
        bors=request.POST['bors']
        tr_list=CommOpenedOptions.objects.filter(userid=user,tradename=commodity,bors=bors)
        current_pr=[]
        profit=[]
        totalpl=0
        [tr for tr in tr_list]
        for row in tr_list:
            current_pr.append(find_price(row.tradename))
            profit.append(calc_comm_profit(row.levearage, row.quantity, row.openprice, row.tradename, row.bors))
            totalpl=totalpl+calc_comm_profit(row.levearage, row.quantity, row.openprice, row.tradename, row.bors)
            if timezone.now()-row.opendateandtime < timedelta(seconds=60):
                close_allow.append(0)
            else:
                close_allow.append(1)
    transactions=izip(current_pr,profit,tr_list,close_allow)
    context={'commodity':commodity,'bors':bors,'user':user,'transactions':transactions,"close_allow":close_allow,"totalpl":totalpl}
    return render(request,'forex/comm-terminal-list.html',context)

@login_required
#@bazaar_access()
@csrf_exempt
def closetradeall(request):
    user=request.user.username
    allow=checkparameters(request)
    if allow==1 or allow==2:
        currency=request.GET['currency']
        bors=request.GET['bors']
        opened=0;
        try:
            if currency!='0':
                opened_arr=ForexOpenedOptions.objects.filter(userid=user,currency=currency,bors=bors)
            else:
                opened_arr=ForexOpenedOptions.objects.filter(userid=user)
        except:
            return HttpResponse('Positions have already been closed.')
        
        [opened for opened in opened_arr]
        totalpr=0
        for opened in opened_arr:
            op = timezone.now()
            time = opened.opendateandtime
            if (op-time)>timedelta(seconds=60):
                orderid=opened.orderno;
                currency=opened.currency
                lots=opened.lots
                levearage=opened.levearage
                openprice=opened.openprice
                bors=opened.bors
                opendateandtime=opened.opendateandtime
                if bors=="Short":
                    bs="Long"
                else:
                    bs="Short"
                closepr=current_price(currency,bs)
                pl=calc_profit(levearage,lots,openprice,currency,bors)
                totalpr+=pl
                amountnow = pl + (openprice*lots)
                ranking=ForexRankings.objects.get(userid=user)
                ranking.balance=ranking.balance+amountnow
                ranking.equity=ranking.equity+pl
                ranking.margin=ranking.margin-(openprice*lots*levearage*10)
                ranking.save()
                closed=ForexClosedOptions(userid=user,closedateandtime=timezone.now(),currency=currency,lots=lots,bors=bors,openprice=openprice,levearage=levearage,fltprofit=pl,closeprice=closepr,opendateandtime=opendateandtime)
                closed.save()
                opened.delete()
    
        if totalpr>0:
            return HttpResponse("<div align='center'><span class='green'>Transactions Closed Successfully with net profit of $ "+str(totalpr)+"</span></div>")
        else:
            return HttpResponse("<div align='center'><span class='red'>Transactions Closed Successfully with net loss of $ "+str(-totalpr)+"</span></div>")

@vary_on_headers('User-Agent')
def manifest(request):
    user_string = request.META['HTTP_USER_AGENT'].lower()
    return render(request, 'cache.appcache',{}, content_type = 'text/cache-manifest')


@login_required
#@bazaar_access()
@csrf_exempt
def closetrade(request):
    user=request.user.username
    allow=checkparameters(request)
    if allow==1 or allow==2:
        orderid=request.GET['orderid'];
        opened=0;
        try:
            opened=ForexOpenedOptions.objects.get(orderno=orderid)
        except:
            return HttpResponse('Position has already been closed.')
        op = timezone.now()
        time = opened.opendateandtime
        if (op-time)>timedelta(seconds=60) and opened.userid==user:
            orderid=orderid;
            currency=opened.currency
            lots=opened.lots
            levearage=opened.levearage
            openprice=opened.openprice
            bors=opened.bors
            opendateandtime=opened.opendateandtime
        if bors=="Short":
            bs="Long"
        else:
            bs="Short"
        closepr=current_price(currency,bs)
        pl=calc_profit(levearage,lots,openprice,currency,bors)
        amountnow = pl + (openprice*lots)
        ranking=ForexRankings.objects.get(userid=user)
        ranking.balance=ranking.balance+amountnow
        ranking.equity=ranking.equity+pl
        ranking.margin=ranking.margin-(openprice*lots*levearage*10)
        ranking.save()
        closed=ForexClosedOptions(userid=user,closedateandtime=timezone.now(),currency=currency,lots=lots,bors=bors,openprice=openprice,levearage=levearage,fltprofit=pl,closeprice=closepr,opendateandtime=opendateandtime)
        closed.save()
        opened.delete()
    
        if pl>0:
            return HttpResponse("<div align='center'><font color='#2A3F00'>Transaction Closed Successfully with profit of $ "+str(pl)+"</font></div>")
        else:
            return HttpResponse("<div align='center'><font color='red'>Transaction Closed Successfully with loss of $ "+str(-pl)+"</font></div>")

@login_required
#@bazaar_access()
@csrf_exempt
def terminal_list(request):
    user=request.user.username
    allow=checkparameters(request)
    if allow==1 or allow==2:
        close_allow=[]
        bs={"Long":"Short","Short":"Long"}
        currency=request.POST['currency']
        bors=request.POST['bors']
        tr_list=ForexOpenedOptions.objects.filter(userid=user,currency=currency,bors=bors)
        current_pr=[]
        profit=[]
        openpricestr={"Long":" (Ask)","Short":" (Bid)"}
        currentpricestr={"Long":" (Bid)","Short":" (Ask)"}
        totalpl=0
        [tr for tr in tr_list]
        for row in tr_list:
            current_pr.append(current_price(row.currency, bs[row.bors]))
            profit.append(calc_profit(row.levearage, row.lots, row.openprice, row.currency, row.bors))
            totalpl=totalpl+calc_profit(row.levearage, row.lots, row.openprice, row.currency, row.bors)
            if timezone.now()-row.opendateandtime < timedelta(seconds=60):
                close_allow.append(0)
            else:
                close_allow.append(1)
    transactions=izip(current_pr,profit,tr_list,close_allow)
    context={'currency':currency,'bors':bors,'user':user,'transactions':transactions,"close_allow":close_allow,"currentpricestr":currentpricestr,"openpricestr":openpricestr,"totalpl":totalpl}
    return render(request,'forex/terminal-list.html',context)

@login_required
#@bazaar_access()
def terminal(request):
    user=request.user.username
    forex_base=ForexBase.objects.all()
    options=[]
    i=0
    for option in forex_base:
        printFormat=round(option.ask,4)-round(option.bid,4)
        printFormat=round(printFormat,4)
        printFormat=printFormat*10000
        printFormat=round(printFormat,0) 
        val=printFormat/100
        if round(val,0)>0:
            printFormat=val
        options.append(printFormat)
        i=i+1
        close_allow=[]
        bs={"Long":"Short","Short":"Long"}
    tr_list=ForexOpenedOptions.objects.filter(userid=user)
    current_pr=[]
    profit=[]
    openpricestr={"Long":" (Ask)","Short":" (Bid)"}
    currentpricestr={"Long":" (Bid)","Short":" (Ask)"}
    totalpl=0
    [tr for tr in tr_list]
    for row in tr_list:
        current_pr.append(current_price(row.currency, bs[row.bors]))
        profit.append(calc_profit(row.levearage, row.lots, row.openprice, row.currency, row.bors))
        totalpl=totalpl+calc_profit(row.levearage, row.lots, row.openprice, row.currency, row.bors)
        if timezone.now()-row.opendateandtime < timedelta(seconds=60):
            close_allow.append(0)
        else:
            close_allow.append(1)
    transactions=izip(current_pr,profit,tr_list,close_allow)
    zipped_values=izip(forex_base,options)
    context={'user':user,'forex_base':zipped_values,'transactions':transactions,"close_allow":close_allow,"currentpricestr":currentpricestr,"openpricestr":openpricestr,"totalpl":totalpl}
    return render(request,'forex/terminal.html',context)



@login_required
#@bazaar_access()
def commodity_terminal(request):
    user=request.user.username
    comm_base=CommodityBase.objects.all()
    i=0
    close_allow=[]
    bs={"Long":"Short","Short":"Long"}
    tr_list=CommOpenedOptions.objects.filter(userid=user)
    current_pr=[]
    profit=[]
    openpricestr={"Long":" (Ask)","Short":" (Bid)"}
    currentpricestr={"Long":" (Bid)","Short":" (Ask)"}
    totalpl=0
    [tr for tr in tr_list]
    for row in tr_list:
        current_pr.append(find_price(row.tradename))
        profit.append(calc_comm_profit(row.levearage, row.quantity, row.openprice, row.tradename, row.bors))
        totalpl=totalpl+calc_comm_profit(row.levearage, row.quantity, row.openprice, row.tradename, row.bors)
        if timezone.now()-row.opendateandtime < timedelta(seconds=60):
            close_allow.append(0)
        else:
            close_allow.append(1)
    transactions=izip(current_pr,profit,tr_list,close_allow)
    zipped_values=comm_base
    context={'user':user,'comm_base':zipped_values,'transactions':transactions,"close_allow":close_allow,"totalpl":totalpl}
    return render(request,'forex/commodity_terminal.html',context)

@login_required
#@bazaar_access()
def indices(request):
    from index.models import Indices
    indices=Indices.objects.all()
    return render(request,'widgets/indices.html',{'indices':indices})

@login_required
#@bazaar_access()
def time(request):
    time=timezone.now()
    time=time
    tf=DateFormat(time)
    time=tf.format("Y-m-d H:i:s")
    array={'dateString': str(time)}
    return HttpResponse(json.dumps(array), mimetype="application/json")

@login_required
#@bazaar_access()
def index(request):
    user=request.user.username
    try:
        bazaar_user=Users.objects.get(username=user)
        ranking=ForexRankings.objects.get(userid=user)
    except:
        password=os.urandom(20).encode('hex')
        Users.objects.create(username=user,password=password)
        ForexRankings.objects.create(userid=user,equity=2500000,balance=2500000,margin=0,percentage=0,rank=1,ex_bal=0,ex_wor=0)
    try:
        algotrade_user=algotrade.models.Users.objects.get(username=user)
    except:
        password=os.urandom(20).encode('hex')
        algotrade.models.Users.objects.create(username=user,password=password)
    try:
        ranking=algotrade.models.ForexRankings.objects.get(userid=user)
    except:
        algotrade.models.ForexRankings.objects.create(userid=user,equity=2500000,balance=2500000,margin=0,percentage=0,rank=1,ex_bal=0,ex_wor=0)
        
    currency=ForexBase.objects.all()
    commodity=CommodityBase.objects.all()
    notifications=Notifications.objects.all().order_by('-time')
    context={'user':user,'currency':currency,'commodity':commodity,'notifications':notifications}
    return render(request,'forex/index.html',context)

@login_required
#@bazaar_access()
def trade(request):
    user=request.user.username
    currency=ForexBase.objects.all()
    commodity=CommodityBase.objects.all()
    context={'user':user,'currency':currency,'commodity':commodity}
    return render(request,'forex/trade.html',context)

@login_required
#@bazaar_access()
def charts(request,type,identifier):
    return render(request,'widgets/charts.html',{'identifier':identifier,'type':type})

@login_required
#@bazaar_access()
def toprankers(request):
    rankings=ForexRankings.objects.all().order_by("-equity")[:50]
    return render(request,'forex/toprankers.html',{'rankings':rankings})


@login_required
#@bazaar_access()
def round_end(request):
    check_r=ForexClosedOptions.objects.all()
    check_cnt=check_r.count()
    if check_cnt==0:
        return HttpResponse('Position has already been closed.')
    output=""    
    for check_a in check_r:    
        currency=check_a.currency
        lots=check_a.lots
        orderno = check_a.orderno
        levearage=check_a.levearage
        openprice=check_a.openprice
        opentime = check_a.opendateandtime
        bors=check_a.bors
        if bors=="Short":
            bs="Long"
        else:
            bs="Short"
        closepr=current_price(currency,bs)
        pl=calc_profit(levearage,lots,openprice,currency,bors)
        amountnow = pl + (openprice*lots);
        try:
            ranking=ForexRankings.objects.get(userid=check_a.userid)
            ranking.balance=ranking.balance + amountnow
            ranking.equity = ranking.equity + pl
            ranking.margin= ranking.margin - (openprice*lots*int(levearage)*10)
            ranking.save()
        except:
            return HttpResponse("Cannot process your request.")                
        
        try:
            closed=ForexClosedOptions()
            closed.userid=check_a.userid
            closed.closedateandtime=timezone.now()
            closed.currency=currency
            closed.lots=lots
            closed.bors=bors
            closed.openprice=openprice
            closed.levearage=levearage
            closed.fltprofit=pl
            closed.closeprice=closepr
            closed.opendataandtime=opentime
            closed.save()
        except:
            return HttpResponse("Cannot execute")
    
        try:
            check_a.delete()
        except:
            return HttpResponse('Cannot process your request.')
        output+= "Deleted "
        output+= check_a.userid
        output+= " '"
        output+= "order."
        output+= "<br/>"
        

@login_required
#@bazaar_access()
def statistics(request):
    user=request.user.username
    time=timezone.now()
    ranking=ForexRankings.objects.get(userid=user)
    context={'ranking':ranking,'user':user,'time':time}
    return render(request,'forex/stats.html',context)


@login_required
#@bazaar_access()
def pendingorders(request):
    user=request.user.username
    pendingoptions=ForexPendingOptions.objects.filter(userid=user)
    price=[]
    [opt for opt in pendingoptions]
    for option in pendingoptions:
        price.append(current_price(option.currency, option.bors))
    pendingoptions=izip(pendingoptions,price) 
    context={'user':user,'pendingoptions':pendingoptions}
    return render(request,'forex/pendingorders.html',context)

@login_required
#@bazaar_access()
def commpendingorders(request):
    user=request.user.username
    pendingoptions=CommPendingOptions.objects.filter(userid=user)
    price=[]
    [opt for opt in pendingoptions]
    for option in pendingoptions:
        price.append(find_price(option.tradename))
    pendingoptions=izip(pendingoptions,price) 
    context={'user':user,'pendingoptions':pendingoptions}
    return render(request,'forex/commpendingorders.html',context)


@login_required
#@bazaar_access()
def learn(request):
    return render(request,'forex/learntoplay.html')

@login_required
#@bazaar_access()
def interface(request):
    return render(request,'forex/interface.html')

@login_required
#@bazaar_access()
def price_info(request):
    commodities=CommodityBase.objects.all()
    forex_base=ForexBase.objects.all()
    percent=[]
    for commodity in commodities:
        if(commodity.open==0):
            commodity.open=1
        per=(float)((commodity.price-commodity.open)/(commodity.open))
        per = per*100;
        per = round(per,2)
        percent.append(per)
    commodities=izip(commodities,percent)  
    context={'commodities':commodities,'forex_base':forex_base}
    return render(request,'forex/price_info.html',context)
        
    

@login_required
#@bazaar_access()
def home(request):
    return render(request,'forex/home.html')

@login_required
#@bazaar_access()
def help(request):
    return render(request,'forex/help.html')

@login_required
#@bazaar_access()
def transact_commodity(request):
    allow=checkparameters(request)
    if allow==1 or allow==2:
        quantity=int(request.POST['lots'])
        pendingprice=request.POST['pendingprice']
        triggerprice = request.POST['triggerprice']
        if triggerprice!="" and not valid_pp(triggerprice):
            return HttpResponse("Please enter a valid trigger price.")
        if pendingprice!="" and not valid_pp(pendingprice):
            return HttpResponse("Please enter a valid trigger price.")
        if quantity<1 or quantity >100:
            return HttpResponse("Please enter value of lots between 1 and 100.")
        commodity=request.POST['commodity']
        bors=request.POST['bors']
        levearage = request.POST['levearage']
        lots=int(request.POST['lots'])
        flag=0
        if levearage=="50" or levearage=="100":
            flag = 1
        if not flag==1:
            return HttpResponse("Please enter a valid levearage value.")
        flag = 0
        if bors=="Long" or bors=="Short":
            flag = 1
        if not flag==1:
            return HttpResponse("Order cannot be executed.")
        currentprice = find_price(commodity)
        br = currentprice*(.0005)    
        if currentprice==0 or currentprice=="" or currentprice<0:
            return HttpResponse("The order cannot be placed.")
        output=""
        if not triggerprice=="":
            if bors=="Long":
                if (float(triggerprice) > currentprice):
                    output+="<div align='center'><font color='red'>"
                    output+="Trigger price" 
                    output+=str(triggerprice)
                    output+=" must be less than the current price "
                    output+=str(currentprice)
                    output+=" of the commodity to place a BUY order"
                    output+="</font></div>"
                    return HttpResponse(output)
            if bors == "Short":
                if (float(triggerprice) < currentprice):
                    output+="<div align='center'><font color='red'>"
                    output+="Trigger price" 
                    output+=str(triggerprice)
                    output+=" must be more than the current price "
                    output+=str(currentprice)
                    output+=" of the commodity to place a SHORT SELL order"
                    output+="</font></div>"
                    return HttpResponse(output)
            if not pendingprice=="":
                if bors=="Long":
                    if float(pendingprice) > currentprice:
                        return HttpResponse("<div align='center'><font color='red'>Pending price must be less than the current price of the commodity to place a BUY order")
                if bors == "Short":
                    if (float(pendingprice) < currentprice):
                        return HttpResponse("<div align='center'><font color='red'>Pending price must be more than the current price of the commodity to place a SHORT SELL order </font></div>")
        
        user=request.user.username;
        if pendingprice=="":
            if triggerprice=="":
                triggerprice = 0
            else:
                triggerprice = float(triggerprice)
            try:
                ranking=ForexRankings.objects.get(userid=user)
            except:
                return HttpResponse('Could not process Request1')
            cost = quantity*(currentprice+br)
            if (currentprice==0 or currentprice=="" or currentprice<0):
                return HttpResponse("The order cannot be placed.")
            if (cost==0 or cost=="" or cost<0):
                return HttpResponse("The order cannot be placed.")
            brnet = quantity* br
            balance = ranking.balance
            if balance<cost:
                return HttpResponse("<div align='center'><font color='red'>Your balance ("+str(balance)+") is less than the amount required  ("+str(cost)+") to open the position.</font></div>")
            balance1 = balance - cost
            c = (-1)*cost
            e = (-1)*brnet
            form_array={'commodity':commodity,'levearage':levearage,'lots':lots,'bors':bors,'currentprice':currentprice,'triggerprice':triggerprice}
            form_array['userid']=request.user.username
            open_form=OpenCommTransaction(form_array)
            if open_form.is_valid():
                open_form.save()
            else:
                return HttpResponse('Error in Input')
            ranking.margin=ranking.margin+(currentprice*int(levearage)*lots*10) 
            ranking.balance=balance1
            ranking.equity=ranking.equity-brnet
            ranking.save()
            return HttpResponse("<div align='center'><font color='green'>Your transaction for <b>"+str(lots)+"</b> lots of <b>"+str(commodity)+"</b> at a leverage of <b>1:"+str(levearage)+"</b> has been successfully completed.</font></div>")
                
        elif not pendingprice=="":
            if triggerprice=="":
                triggerprice=0
            else:
                triggerprice=float(triggerprice)
            if valid_pp(pendingprice):
                pendingprice=float(pendingprice)
            try:
                ranking=ForexRankings.objects.get(userid=user)
            except:
                return HttpResponse('Could not process Request3')
            cost = quantity * triggerprice;
            balance = ranking.balance
            if balance<cost:
                return HttpResponse("<div align='center'><font color='red'>Your balance ("+str(balance)+") is less than the amount required  ("+str(cost)+") to open the position.</font></div>")
            form_array={'commodity':commodity,'levearage':levearage,'lots':lots,'bors':bors,'pendingprice':pendingprice,'triggerprice':triggerprice}
            form_array['userid']=user
            pending_form=CommPendingTransaction(form_array)
            if pending_form.is_valid():
                pending_form.save()
            else:
                return HttpResponse('Error in Input')
            return HttpResponse("<div align='center'><font color='green'>Your order for <b>"+str(lots)+"</b> lots of <b>"+str(commodity)+"</b> at a leverage of <b>1:"+str(levearage)+"</b> was successfully placed and will be executed when the market price reaches <b>"+str(pendingprice)+"</b></font></div>")
    else:
        return HttpResponse("Market is closed now.")

@login_required
#@bazaar_access()
def commodity_transaction(request):
    commodity_base=CommodityBase.objects.all()
    lots=range(1,101)
    context={'commodity_base':commodity_base, 'lots':lots}
    return render(request,'forex/commodity_transaction.html',context)

@login_required
#@bazaar_access()
def transact(request):
    allow=checkparameters(request)
    if allow==1 or allow==2:
        lots=int(request.POST['lots'])
        user=request.user.username
        currency=request.POST['currency']
        levearage=int(request.POST['levearage'])
        bors=request.POST['bors']
        pendingprice=request.POST['pendingprice']
        currentprice=current_price(currency,bors)
        triggerprice =request.POST['triggerprice']
        if triggerprice=='' or triggerprice==' ' or triggerprice<0:
            triggerprice = '0'
        elif valid_pp(triggerprice):
            triggerprice=float(triggerprice)
        
        if lots<0 or lots>50 or lots=="":
            return HttpResponse("Please enter lots in between 0 to 50")
        if pendingprice=="":
            result=ForexRankings.objects.get(userid=request.user.username)
            if result:
                margin=result.margin + (currentprice*levearage*lots*10)  
                if currentprice==0 or currentprice=="" or currentprice<0:
                    return HttpResponse("The order cannot be placed.");
                c = calc_profit(levearage,lots,currentprice,currency,bors)-(lots*currentprice)
                #afterequity=result.balance+calc_profit(levearage,lots,currentprice,currency,bors)-(lots*currentprice)
                afterequity=result.balance-(lots*currentprice)
                e = calc_profit(levearage,lots,currentprice,currency,bors)
                equity = result.equity+calc_profit(levearage,lots,currentprice,currency,bors)
                per=afterequity/margin
                per=per*100;
                balequityflag=0;
                if result.balance<0:
                    return HttpResponse("<div align='center'><font color='red'>Your balance ("+str(result.balance)+") is not sufficient to open the position.</font></div>")
                if balequityflag==0:
                    form_array={'currency':currency,'levearage':levearage,'lots':lots,'bors':bors,'currentprice':currentprice,'triggerprice':triggerprice}
                    form_array['userid']=request.user.username
                    open_form=OpenTransaction(form_array)
                    if open_form.is_valid():
                        open_form.save()
                    else:
                        return HttpResponse('Error in Input')
                    result.margin=margin
                    result.balance=afterequity
                    result.equity=equity
                    result.save()
                    return HttpResponse("<div align='center'><font color='green'>Your transaction for <b>"+str(lots)+"</b> lots of <b>"+currency+"</b> at a leverage of <b>1:"+str(levearage)+"</b> has been successfully completed.</font></div>")
            else:
                return HttpResponse("Error usr356 : User not found")
        else:
            if valid_pp(pendingprice):
                pendingprice=float(pendingprice)
                form_array={'currency':currency,'levearage':levearage,'lots':lots,'bors':bors,'pendingprice':pendingprice,'triggerprice':triggerprice}
                form_array['userid']=user
                pending_form=PendingTransaction(form_array)
                if pending_form.is_valid():
                    pending_form.save()
                else:
                    return HttpResponse('Error in Input')
                return HttpResponse("<div align='center'><font color='green'>Your order for <b>"+str(lots)+"</b> lots of <b>"+currency+"</b> at a leverage of <b>1:"+str(levearage)+"</b> was successfully placed and will be executed when the market price reaches <b>"+str(pendingprice)+"</b></font></div>")
    else:
        return HttpResponse("Market is closed now.")

@login_required
#@bazaar_access()
def transaction(request):
    forex_base=ForexBase.objects.all()
    options=[]
    i=0
    for option in forex_base:
        printFormat=round(option.ask,4)-round(option.bid,4)
        printFormat=round(printFormat,4)
        printFormat=printFormat*10000
        printFormat=round(printFormat,0) 
        val=printFormat/100
        if round(val,0)>0:
            printFormat=val
        options.append(printFormat)
        i=i+1
    lots=range(1,51)
    zipped_values=izip(forex_base,options)
    context={'options':options,'forex_base':zipped_values, 'lots':lots}
    return render(request,'forex/transaction.html',context)
    
    
@login_required
#@bazaar_access()
@csrf_exempt
def cancelbuyordercomm(request):
    allow=checkparameters(request)
    form=CommCancelOrderForm(request.POST)
    if not form.is_valid():
        return HttpResponse("Form Invalid") 
    if form.is_valid() and form.cleaned_data['tradename'] in CommodityBase.objects.values_list('commodity',flat=True):
        if allow==1 or allow==2 :
            pending=CommPendingOptions.objects.get(pendingid=form.cleaned_data['pendingid'])
            pending.delete()
            output='Pending Order cancelled'
        else:
            raise Http404
    else:
        output='Please enter a valid commodity'
    return HttpResponse(output)

#Cancel pending order function
@login_required
#@bazaar_access()
@csrf_exempt
def cancelbuyorder(request):
    allow=checkparameters(request)
    form=CancelOrderForm(request.POST) 
    if form.is_valid() and form.cleaned_data['currency'] in ForexBase.objects.values_list('currency',flat=True):
        if allow==1 or allow==2 :
            pending=ForexPendingOptions.objects.get(pendingid=form.cleaned_data['pendingid'])
            pending.delete()
            output='Pending Order cancelled'
        else:
            raise Http404
    else:
        output='Please enter a valid currency'
    return HttpResponse(output)
    

#basic rules and regulations view(forex/basics)
def forex_basics(request):
    return render_to_response('forex/basics.html')


# Stocks Marquee View (/forex/stocks)
def stocks_marquee(request):
    c_base=CommodityBase.objects.all()
    output="Commodity Prices : "
    check_cnt=c_base.count()
    if check_cnt==0:
        output+='No commodity exists'
    for check_a in c_base:
        open1 = check_a.open
        if open1==0:
            per = 0
        else: 
            per = (check_a.price-open1)/(open1)
            per = per*100
        output+='<font color="#AA9F00">'
        output+=str(check_a.tradename)
        output+=' : '
        output+=str(check_a.price)
        output+=' '
        output+='</font>'
        if per>=0:
            output+='<font color="#18f60d">'
        else:
            output+='<font color="#f43212">'
        output+=str(round(per,2))
        output+= ' %'
        output+= ' &nbsp'
        output+= ' &nbsp'
        output+= '</font>'        

    output+='Currency Rates : '
    check_r=ForexBase.objects.all()
    check_cnt=check_r.count()
    if check_cnt==0:
        output+='No currency exists'
    else:
        for check_a in check_r:
            output+='<font color="#AA9F00">'
            output+=str(check_a.currency)
            output+=' : ' 
            output+='</font>'
            output+='<font color="#eb4929">'
            output+=str(check_a.bid)
            output+=' / '
            output+=str(check_a.ask)
            output+=' ';
            output+=' &nbsp'
            output+=' &nbsp'
            output+='</font>'
            open1 = check_a.open
            if open1==0:
                per = 0 
            else:
                per = (check_a.bid-open1)/(open1);
                per = per*100
            if per>=0:
                output+='<font color="#18f60d">'
            else:
                output+='<font color="#f43212">'
            output+=str(round(per,2))
            output+=' %'
            output+=' &nbsp'
            output+=' &nbsp'
            output+='</font>'
    
    return HttpResponse(output)

def not_allowed(request):
    return HttpResponse('Baazar has been closed. Results will be announced shortly. Good Luck.')

def closed(request):
    return render(request, 'login.html', {})