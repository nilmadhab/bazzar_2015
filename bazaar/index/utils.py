'''
Created on Jun 11, 2013

@author: Srinivasan
'''
from index.models import ForexRankings, ForexPendingOptions, ForexOpenedOptions, \
    MarketOpen, CommOpenedOptions, CommodityBase
from django.utils import timezone
from locale import currency

def brockerage(userid,price):
    result=CommOpenedOptions.objects.filter(userid=userid)
    rows=result.count()
    if rows<100:
        br=0.1*price/100;
        if br<0.02:
            br=0.02
    elif rows>100 and rows<1000:
        br=0.2*price/100;
        if br<0.05:
            br=0.05
    else:
        br=0.5*price/100
        if br<0.1:
            br=0.1
    return br
    
def find_price(name):
    price=0
    try:
        qprice = CommodityBase.objects.filter(commodity=name)[0]
        price = qprice.price
    except:
        pass
    return price


def checkparameters(request):
    arr = MarketOpen.objects.all()[0]
    tradeallow=arr.oc
    if request.user.username=='srinivasan':
        tradeallow=1
    return tradeallow

def get_forex_base():
    from index.models import ForexBase
    base=ForexBase.objects.all()
    data={}
    for price in base:
        data[price.bidvar]=price.bid
        data[price.askvar]=price.ask
    return data

def get_comm_base(commodity):
    from index.models import CommodityBase
    try:
        base=CommodityBase.objects.filter(commodity=commodity)[0]
        data=base.price
    except:
        data=0
    return data
    

def calc_profit(leverage,lots,openprice,currency,bos):
    currency=currency.lower()
    data=get_forex_base();
    bidstr=currency+'bid'
    askstr=currency+'ask'
    bid=data[bidstr]
    ask=data[askstr]
    if currency[0:3]=='usd':
        ttype=1
    else:
        ttype=2
    
    profit='error'
    if bos=='Long':
        profit=leverage*lots*(bid-openprice)*1000/bid
        if ttype==1:
            #profit=profit/bid
            pass
    
    if bos=='Short':
        profit=leverage*lots*(openprice-ask)*1000/ask;
        if ttype!=1:
            #profit=profit/bid;
            pass
    
    return round(profit,2)

def calc_comm_profit(leverage,lots,openprice,commodity,bos):
    if bos=='Long':
        flag=1
    if bos=='Short':
        flag=-1
    current_price=get_comm_base(commodity)
    profit=flag*(current_price-openprice)*lots*leverage
    return round(profit,2)
            
            
def rahagahah():
    from index.models import CommClosedOptions
    closed=CommClosedOptions.objects.all()
    count=0
    for close in closed:
        if((close.bors=='Long' and close.tranprice<close.closecp and close.profit<0) or (close.bors=='Long' and close.tranprice>close.closecp and close.profit>0) or (close.bors=='Short' and close.tranprice<close.closecp and close.profit>0) or (close.bors=='Short' and close.tranprice>close.closecp and close.profit<0)):
            close.profit=close.profit*-1
            close.save()
            print(close.userid)
            print(close.profit)
            print(close.tranprice)
            print(close.closecp)
            try:
                ur=ForexRankings.objects.get(userid=close.userid)
                ur.balance=ur.balance+2*close.profit
                ur.equity=ur.equity+2*close.profit
                ur.save()
                count=count+1
            except:
                pass
    print(count)


    
def valid_pp(pending_price):
    import re
    if re.match(r'^[0-9\.]+$',pending_price,re.M|re.I):
        return True
    else:
        return False

def calc_profit_trigger(leverage,lots,openprice,bos,triggerprice):
    if bos=="Long":
        profit=leverage*lots*(triggerprice-openprice)*1000/triggerprice
    if bos=="Short":
        profit=leverage*lots*(openprice-triggerprice)*1000/triggerprice
    return round(profit,2)

def current_price(currency,bors):
    data=get_forex_base()
    currency=currency.lower()
    bidstr=currency+'bid'
    askstr=currency+'ask'
    bid=data[bidstr]
    ask=data[askstr]
    if bors=="Long":
        return ask
    if bors=="Short":
        return bid
    
def calculate_net():
    a = ForexRankings.objects.all()
    for a1 in a:
        reserve = a1.equity;
        marginA = 2500000;
        mar=0;
        a1.balance=reserve
        a1.save()
        # Make reserve and equity 2500000 here for restarting game
        print('Updated account of '+a1.userid+'\n')


def reset_game():
    a = ForexRankings.objects.all()
    for a1 in a:
        reserve = a1.equity;
        marginA = 2500000;
        mar=0;
        a1.balance=reserve
        a1.equity=marginA
        a1.margin=mar
        a1.save()
        # Make reserve and equity 2500000 here for restarting game
        print('Account Reset :  '+a1.userid+'\n')


def update():
    print('Forex update called')
    result1=ForexRankings.objects.all()
    j=0
    data={}
    for arr1 in result1:
        data[arr1.userid]=arr1.percentage
        j=j+1
        print(j+' '+arr1.userid+'='+data[arr1.userid]+'\n')
    result=ForexPendingOptions.objects.all()
    for arr in result:
        currentprice=current_price(arr.currency,arr.bors)
        flag=0
        print(currentprice)
        if arr.bors=='Long':
            flag=arr.pendingprice-currentprice
        else: 
            flag=currentprice-arr.pendingprice 
        if flag>=0:
            print(currentprice+' '+arr.pendingprice+' '+arr.userid+'    '+arr.bors+'    '+flag+'\n')
            currency=arr.currency
            levearage=arr.levearage
            bors=arr.bors
            lots=arr.lots
            margin=(levearage*lots*10)
            perc=data[arr.userid]
            print(perc)
            openprice = arr.pendingprice
            closepr=current_price(currency,bors)
            pl=calc_profit(levearage,lots,openprice,currency,bors)
            amountnow = (openprice*lots)
            ranking=ForexRankings.objects.get(userid=arr.userid)
            ranking.balance=ranking.balance-amountnow
            ranking.equity=ranking.equity-pl
            ranking.margin=ranking.margin-(openprice*lots*levearage*10)
            ranking.save()
            
            opened=ForexOpenedOptions()
            opened.userid=arr.userid
            opened.opendateandtime=timezone.now()
            opened.currency=currency
            opened.levearage=levearage
            opened.bors=bors
            opened.lots=lots
            opened.openprice=currentprice
            opened.save()
            
            ranking.margin=ranking.margin+margin
            ranking.save()
            q=ForexPendingOptions.objects.get(arr.pendingid)
            q.delete()