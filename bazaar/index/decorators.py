'''
Created on Jun 12, 2013

@author: Srinivasan
'''
from django.contrib.auth.models import User
from index.utils import checkparameters
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from functools import wraps
from index.models import ForexRankings
from data.models import WudRankings 
 
def bazaar_access():
    def decorator(func):
        def inner_decorator(request, *args, **kwargs):
        	return HttpResponseRedirect(reverse(index.views.not_allowed))

#           if (request.user.username in User.objects.values_list('username',flat=True) and (request.user.username in WudRankings.objects.values_list('userid',flat=True) or request.user.username in ForexRankings.objects.values_list('userid',flat=True))) and checkparameters(request):
#				return func(request, *args, **kwargs)
#           else:
 
        return wraps(func)(inner_decorator)
    return decorator
