/*
SQLyog Enterprise - MySQL GUI v8.02 RC
MySQL - 5.5.36 : Database - ktj14_bazaarfinalround
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`ktj14_bazaarfinalround` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `ktj14_bazaarfinalround`;

/*Table structure for table `auth_group` */

DROP TABLE IF EXISTS `auth_group`;

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `auth_group` */

LOCK TABLES `auth_group` WRITE;

UNLOCK TABLES;

/*Table structure for table `auth_group_permissions` */

DROP TABLE IF EXISTS `auth_group_permissions`;

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `permission_id_refs_id_6ba0f519` (`permission_id`),
  CONSTRAINT `group_id_refs_id_f4b32aac` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `permission_id_refs_id_6ba0f519` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `auth_group_permissions` */

LOCK TABLES `auth_group_permissions` WRITE;

UNLOCK TABLES;

/*Table structure for table `auth_permission` */

DROP TABLE IF EXISTS `auth_permission`;

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  CONSTRAINT `content_type_id_refs_id_d043b34a` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=304 DEFAULT CHARSET=latin1;

/*Data for the table `auth_permission` */

LOCK TABLES `auth_permission` WRITE;

insert  into `auth_permission`(`id`,`name`,`content_type_id`,`codename`) values (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add permission',2,'add_permission'),(5,'Can change permission',2,'change_permission'),(6,'Can delete permission',2,'delete_permission'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add user',4,'add_user'),(11,'Can change user',4,'change_user'),(12,'Can delete user',4,'delete_user'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add site',7,'add_site'),(20,'Can change site',7,'change_site'),(21,'Can delete site',7,'delete_site'),(22,'Can add job',8,'add_job'),(23,'Can change job',8,'change_job'),(24,'Can delete job',8,'delete_job'),(25,'Can add cron',9,'add_cron'),(26,'Can change cron',9,'change_cron'),(27,'Can delete cron',9,'delete_cron'),(28,'Can add users',10,'add_users'),(29,'Can change users',10,'change_users'),(30,'Can delete users',10,'delete_users'),(31,'Can add comm closed options',11,'add_commclosedoptions'),(32,'Can change comm closed options',11,'change_commclosedoptions'),(33,'Can delete comm closed options',11,'delete_commclosedoptions'),(34,'Can add comm opened options',12,'add_commopenedoptions'),(35,'Can change comm opened options',12,'change_commopenedoptions'),(36,'Can delete comm opened options',12,'delete_commopenedoptions'),(37,'Can add comm pending close options',13,'add_commpendingcloseoptions'),(38,'Can change comm pending close options',13,'change_commpendingcloseoptions'),(39,'Can delete comm pending close options',13,'delete_commpendingcloseoptions'),(40,'Can add comm pending options',14,'add_commpendingoptions'),(41,'Can change comm pending options',14,'change_commpendingoptions'),(42,'Can delete comm pending options',14,'delete_commpendingoptions'),(43,'Can add commodity base',15,'add_commoditybase'),(44,'Can change commodity base',15,'change_commoditybase'),(45,'Can delete commodity base',15,'delete_commoditybase'),(46,'Can add commodity trading',16,'add_commoditytrading'),(47,'Can change commodity trading',16,'change_commoditytrading'),(48,'Can delete commodity trading',16,'delete_commoditytrading'),(49,'Can add forex base',17,'add_forexbase'),(50,'Can change forex base',17,'change_forexbase'),(51,'Can delete forex base',17,'delete_forexbase'),(52,'Can add forex closed options',18,'add_forexclosedoptions'),(53,'Can change forex closed options',18,'change_forexclosedoptions'),(54,'Can delete forex closed options',18,'delete_forexclosedoptions'),(55,'Can add indices',19,'add_indices'),(56,'Can change indices',19,'change_indices'),(57,'Can delete indices',19,'delete_indices'),(58,'Can add forex opened options',20,'add_forexopenedoptions'),(59,'Can change forex opened options',20,'change_forexopenedoptions'),(60,'Can delete forex opened options',20,'delete_forexopenedoptions'),(61,'Can add forex pending options',21,'add_forexpendingoptions'),(62,'Can change forex pending options',21,'change_forexpendingoptions'),(63,'Can delete forex pending options',21,'delete_forexpendingoptions'),(64,'Can add forex rankings',22,'add_forexrankings'),(65,'Can change forex rankings',22,'change_forexrankings'),(66,'Can delete forex rankings',22,'delete_forexrankings'),(67,'Can add forex user deductions',23,'add_forexuserdeductions'),(68,'Can change forex user deductions',23,'change_forexuserdeductions'),(69,'Can delete forex user deductions',23,'delete_forexuserdeductions'),(70,'Can add forexbasedata',24,'add_forexbasedata'),(71,'Can change forexbasedata',24,'change_forexbasedata'),(72,'Can delete forexbasedata',24,'delete_forexbasedata'),(73,'Can add futures base',25,'add_futuresbase'),(74,'Can change futures base',25,'change_futuresbase'),(75,'Can delete futures base',25,'delete_futuresbase'),(76,'Can add futures opened or closed',26,'add_futuresopenedorclosed'),(77,'Can change futures opened or closed',26,'change_futuresopenedorclosed'),(78,'Can delete futures opened or closed',26,'delete_futuresopenedorclosed'),(79,'Can add market open',27,'add_marketopen'),(80,'Can change market open',27,'change_marketopen'),(81,'Can delete market open',27,'delete_marketopen'),(82,'Can add options base',28,'add_optionsbase'),(83,'Can change options base',28,'change_optionsbase'),(84,'Can delete options base',28,'delete_optionsbase'),(85,'Can add options opened or closed',29,'add_optionsopenedorclosed'),(86,'Can change options opened or closed',29,'change_optionsopenedorclosed'),(87,'Can delete options opened or closed',29,'delete_optionsopenedorclosed'),(88,'Can add rediff links',30,'add_redifflinks'),(89,'Can change rediff links',30,'change_redifflinks'),(90,'Can delete rediff links',30,'delete_redifflinks'),(91,'Can add status',31,'add_status'),(92,'Can change status',31,'change_status'),(93,'Can delete status',31,'delete_status'),(94,'Can add stockprice',32,'add_stockprice'),(95,'Can change stockprice',32,'change_stockprice'),(96,'Can delete stockprice',32,'delete_stockprice'),(97,'Can add stocks rate',33,'add_stocksrate'),(98,'Can change stocks rate',33,'change_stocksrate'),(99,'Can delete stocks rate',33,'delete_stocksrate'),(100,'Can add wud closed options',34,'add_wudclosedoptions'),(101,'Can change wud closed options',34,'change_wudclosedoptions'),(102,'Can delete wud closed options',34,'delete_wudclosedoptions'),(103,'Can add wud opened options',35,'add_wudopenedoptions'),(104,'Can change wud opened options',35,'change_wudopenedoptions'),(105,'Can delete wud opened options',35,'delete_wudopenedoptions'),(106,'Can add wud pending close options',36,'add_wudpendingcloseoptions'),(107,'Can change wud pending close options',36,'change_wudpendingcloseoptions'),(108,'Can delete wud pending close options',36,'delete_wudpendingcloseoptions'),(109,'Can add wud pending options',37,'add_wudpendingoptions'),(110,'Can change wud pending options',37,'change_wudpendingoptions'),(111,'Can delete wud pending options',37,'delete_wudpendingoptions'),(112,'Can add wud rankings',38,'add_wudrankings'),(113,'Can change wud rankings',38,'change_wudrankings'),(114,'Can delete wud rankings',38,'delete_wudrankings'),(115,'Can add wud user deductions',39,'add_wuduserdeductions'),(116,'Can change wud user deductions',39,'change_wuduserdeductions'),(117,'Can delete wud user deductions',39,'delete_wuduserdeductions'),(118,'Can add notifications',40,'add_notifications'),(119,'Can change notifications',40,'change_notifications'),(120,'Can delete notifications',40,'delete_notifications'),(121,'Can add users',41,'add_users'),(122,'Can change users',41,'change_users'),(123,'Can delete users',41,'delete_users'),(124,'Can add comm closed options',42,'add_commclosedoptions'),(125,'Can change comm closed options',42,'change_commclosedoptions'),(126,'Can delete comm closed options',42,'delete_commclosedoptions'),(127,'Can add comm opened options',43,'add_commopenedoptions'),(128,'Can change comm opened options',43,'change_commopenedoptions'),(129,'Can delete comm opened options',43,'delete_commopenedoptions'),(130,'Can add comm pending close options',44,'add_commpendingcloseoptions'),(131,'Can change comm pending close options',44,'change_commpendingcloseoptions'),(132,'Can delete comm pending close options',44,'delete_commpendingcloseoptions'),(133,'Can add comm pending options',45,'add_commpendingoptions'),(134,'Can change comm pending options',45,'change_commpendingoptions'),(135,'Can delete comm pending options',45,'delete_commpendingoptions'),(136,'Can add commodity base',46,'add_commoditybase'),(137,'Can change commodity base',46,'change_commoditybase'),(138,'Can delete commodity base',46,'delete_commoditybase'),(139,'Can add commodity trading',47,'add_commoditytrading'),(140,'Can change commodity trading',47,'change_commoditytrading'),(141,'Can delete commodity trading',47,'delete_commoditytrading'),(142,'Can add forex base',48,'add_forexbase'),(143,'Can change forex base',48,'change_forexbase'),(144,'Can delete forex base',48,'delete_forexbase'),(145,'Can add forex closed options',49,'add_forexclosedoptions'),(146,'Can change forex closed options',49,'change_forexclosedoptions'),(147,'Can delete forex closed options',49,'delete_forexclosedoptions'),(148,'Can add indices',50,'add_indices'),(149,'Can change indices',50,'change_indices'),(150,'Can delete indices',50,'delete_indices'),(151,'Can add forex opened options',51,'add_forexopenedoptions'),(152,'Can change forex opened options',51,'change_forexopenedoptions'),(153,'Can delete forex opened options',51,'delete_forexopenedoptions'),(154,'Can add forex pending options',52,'add_forexpendingoptions'),(155,'Can change forex pending options',52,'change_forexpendingoptions'),(156,'Can delete forex pending options',52,'delete_forexpendingoptions'),(157,'Can add forex rankings',53,'add_forexrankings'),(158,'Can change forex rankings',53,'change_forexrankings'),(159,'Can delete forex rankings',53,'delete_forexrankings'),(160,'Can add forex user deductions',54,'add_forexuserdeductions'),(161,'Can change forex user deductions',54,'change_forexuserdeductions'),(162,'Can delete forex user deductions',54,'delete_forexuserdeductions'),(163,'Can add forexbasedata',55,'add_forexbasedata'),(164,'Can change forexbasedata',55,'change_forexbasedata'),(165,'Can delete forexbasedata',55,'delete_forexbasedata'),(166,'Can add futures base',56,'add_futuresbase'),(167,'Can change futures base',56,'change_futuresbase'),(168,'Can delete futures base',56,'delete_futuresbase'),(169,'Can add futures opened or closed',57,'add_futuresopenedorclosed'),(170,'Can change futures opened or closed',57,'change_futuresopenedorclosed'),(171,'Can delete futures opened or closed',57,'delete_futuresopenedorclosed'),(172,'Can add market open',58,'add_marketopen'),(173,'Can change market open',58,'change_marketopen'),(174,'Can delete market open',58,'delete_marketopen'),(175,'Can add options base',59,'add_optionsbase'),(176,'Can change options base',59,'change_optionsbase'),(177,'Can delete options base',59,'delete_optionsbase'),(178,'Can add options opened or closed',60,'add_optionsopenedorclosed'),(179,'Can change options opened or closed',60,'change_optionsopenedorclosed'),(180,'Can delete options opened or closed',60,'delete_optionsopenedorclosed'),(181,'Can add rediff links',61,'add_redifflinks'),(182,'Can change rediff links',61,'change_redifflinks'),(183,'Can delete rediff links',61,'delete_redifflinks'),(184,'Can add status',62,'add_status'),(185,'Can change status',62,'change_status'),(186,'Can delete status',62,'delete_status'),(187,'Can add stockprice',63,'add_stockprice'),(188,'Can change stockprice',63,'change_stockprice'),(189,'Can delete stockprice',63,'delete_stockprice'),(190,'Can add stocks rate',64,'add_stocksrate'),(191,'Can change stocks rate',64,'change_stocksrate'),(192,'Can delete stocks rate',64,'delete_stocksrate'),(193,'Can add wud closed options',65,'add_wudclosedoptions'),(194,'Can change wud closed options',65,'change_wudclosedoptions'),(195,'Can delete wud closed options',65,'delete_wudclosedoptions'),(196,'Can add wud opened options',66,'add_wudopenedoptions'),(197,'Can change wud opened options',66,'change_wudopenedoptions'),(198,'Can delete wud opened options',66,'delete_wudopenedoptions'),(199,'Can add wud pending close options',67,'add_wudpendingcloseoptions'),(200,'Can change wud pending close options',67,'change_wudpendingcloseoptions'),(201,'Can delete wud pending close options',67,'delete_wudpendingcloseoptions'),(202,'Can add wud pending options',68,'add_wudpendingoptions'),(203,'Can change wud pending options',68,'change_wudpendingoptions'),(204,'Can delete wud pending options',68,'delete_wudpendingoptions'),(205,'Can add wud rankings',69,'add_wudrankings'),(206,'Can change wud rankings',69,'change_wudrankings'),(207,'Can delete wud rankings',69,'delete_wudrankings'),(208,'Can add wud user deductions',70,'add_wuduserdeductions'),(209,'Can change wud user deductions',70,'change_wuduserdeductions'),(210,'Can delete wud user deductions',70,'delete_wuduserdeductions'),(211,'Can add notifications',71,'add_notifications'),(212,'Can change notifications',71,'change_notifications'),(213,'Can delete notifications',71,'delete_notifications'),(214,'Can add auth group',72,'add_authgroup'),(215,'Can change auth group',72,'change_authgroup'),(216,'Can delete auth group',72,'delete_authgroup'),(217,'Can add auth group permissions',73,'add_authgrouppermissions'),(218,'Can change auth group permissions',73,'change_authgrouppermissions'),(219,'Can delete auth group permissions',73,'delete_authgrouppermissions'),(220,'Can add auth permission',74,'add_authpermission'),(221,'Can change auth permission',74,'change_authpermission'),(222,'Can delete auth permission',74,'delete_authpermission'),(223,'Can add auth user',75,'add_authuser'),(224,'Can change auth user',75,'change_authuser'),(225,'Can delete auth user',75,'delete_authuser'),(226,'Can add auth user groups',76,'add_authusergroups'),(227,'Can change auth user groups',76,'change_authusergroups'),(228,'Can delete auth user groups',76,'delete_authusergroups'),(229,'Can add auth user user permissions',77,'add_authuseruserpermissions'),(230,'Can change auth user user permissions',77,'change_authuseruserpermissions'),(231,'Can delete auth user user permissions',77,'delete_authuseruserpermissions'),(232,'Can add django admin log',78,'add_djangoadminlog'),(233,'Can change django admin log',78,'change_djangoadminlog'),(234,'Can delete django admin log',78,'delete_djangoadminlog'),(235,'Can add django content type',79,'add_djangocontenttype'),(236,'Can change django content type',79,'change_djangocontenttype'),(237,'Can delete django content type',79,'delete_djangocontenttype'),(238,'Can add django session',80,'add_djangosession'),(239,'Can change django session',80,'change_djangosession'),(240,'Can delete django session',80,'delete_djangosession'),(241,'Can add django site',81,'add_djangosite'),(242,'Can change django site',81,'change_djangosite'),(243,'Can delete django site',81,'delete_djangosite'),(244,'Can add futures base',82,'add_futuresbase'),(245,'Can change futures base',82,'change_futuresbase'),(246,'Can delete futures base',82,'delete_futuresbase'),(247,'Can add futures opened or closed',83,'add_futuresopenedorclosed'),(248,'Can change futures opened or closed',83,'change_futuresopenedorclosed'),(249,'Can delete futures opened or closed',83,'delete_futuresopenedorclosed'),(250,'Can add market open',84,'add_marketopen'),(251,'Can change market open',84,'change_marketopen'),(252,'Can delete market open',84,'delete_marketopen'),(253,'Can add options base',85,'add_optionsbase'),(254,'Can change options base',85,'change_optionsbase'),(255,'Can delete options base',85,'delete_optionsbase'),(256,'Can add options opened or closed',86,'add_optionsopenedorclosed'),(257,'Can change options opened or closed',86,'change_optionsopenedorclosed'),(258,'Can delete options opened or closed',86,'delete_optionsopenedorclosed'),(259,'Can add rediff link',87,'add_redifflink'),(260,'Can change rediff link',87,'change_redifflink'),(261,'Can delete rediff link',87,'delete_redifflink'),(262,'Can add rediff links',88,'add_redifflinks'),(263,'Can change rediff links',88,'change_redifflinks'),(264,'Can delete rediff links',88,'delete_redifflinks'),(265,'Can add status',89,'add_status'),(266,'Can change status',89,'change_status'),(267,'Can delete status',89,'delete_status'),(268,'Can add stockprice',90,'add_stockprice'),(269,'Can change stockprice',90,'change_stockprice'),(270,'Can delete stockprice',90,'delete_stockprice'),(271,'Can add stocks rate',91,'add_stocksrate'),(272,'Can change stocks rate',91,'change_stocksrate'),(273,'Can delete stocks rate',91,'delete_stocksrate'),(274,'Can add wud closed options',92,'add_wudclosedoptions'),(275,'Can change wud closed options',92,'change_wudclosedoptions'),(276,'Can delete wud closed options',92,'delete_wudclosedoptions'),(277,'Can add wud opened options',93,'add_wudopenedoptions'),(278,'Can change wud opened options',93,'change_wudopenedoptions'),(279,'Can delete wud opened options',93,'delete_wudopenedoptions'),(280,'Can add wud pending close options',94,'add_wudpendingcloseoptions'),(281,'Can change wud pending close options',94,'change_wudpendingcloseoptions'),(282,'Can delete wud pending close options',94,'delete_wudpendingcloseoptions'),(283,'Can add wud pending options',95,'add_wudpendingoptions'),(284,'Can change wud pending options',95,'change_wudpendingoptions'),(285,'Can delete wud pending options',95,'delete_wudpendingoptions'),(286,'Can add wud rankings',96,'add_wudrankings'),(287,'Can change wud rankings',96,'change_wudrankings'),(288,'Can delete wud rankings',96,'delete_wudrankings'),(289,'Can add wud user deductions',97,'add_wuduserdeductions'),(290,'Can change wud user deductions',97,'change_wuduserdeductions'),(291,'Can delete wud user deductions',97,'delete_wuduserdeductions'),(292,'Can add nonce',98,'add_nonce'),(293,'Can change nonce',98,'change_nonce'),(294,'Can delete nonce',98,'delete_nonce'),(295,'Can add resource',99,'add_resource'),(296,'Can change resource',99,'change_resource'),(297,'Can delete resource',99,'delete_resource'),(298,'Can add consumer',100,'add_consumer'),(299,'Can change consumer',100,'change_consumer'),(300,'Can delete consumer',100,'delete_consumer'),(301,'Can add token',101,'add_token'),(302,'Can change token',101,'change_token'),(303,'Can delete token',101,'delete_token');

UNLOCK TABLES;

/*Table structure for table `auth_user` */

DROP TABLE IF EXISTS `auth_user`;

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `auth_user` */

LOCK TABLES `auth_user` WRITE;

insert  into `auth_user`(`id`,`password`,`last_login`,`is_superuser`,`username`,`first_name`,`last_name`,`email`,`is_staff`,`is_active`,`date_joined`) values (1,'pbkdf2_sha256$10000$q09I030c7NhD$majSEQhLZ2GFOsw8AvA6m4LLO58K+KtN1+SXDCW9fNM=','2014-10-17 03:15:55',1,'nilmadhab','','','nil@bnn.com',1,1,'2014-10-17 03:15:55'),(2,'pbkdf2_sha256$10000$glV3IY6BMtR3$NNji2GXt6vxTqwFH+79PyUC+KBLrK1B41cFq9+IQNB4=','2014-10-18 11:13:53',1,'admin','','','nilmadhab1994@gmail.com',1,1,'2014-10-18 11:13:53'),(3,'pbkdf2_sha256$10000$rOeEvZH7Fagp$dh9hh+i0KuWVYiM5IbCTacSuhlYYh/fReoiC9xkaXrw=','2014-10-29 17:53:39',1,'nilmadhab1994','','','nil@gkg.com',1,1,'2014-10-29 17:53:39'),(4,'pbkdf2_sha256$10000$Qdr5RNzqApbH$zNgn7/jTdxiZLNE2i2kaggleKvlxHALUaUr9jRCaQik=','2014-10-29 18:11:41',1,'admin1994','','','nil@fgfg.com',1,1,'2014-10-29 18:11:41'),(5,'pbkdf2_sha256$10000$Z7IeMufHG6uW$on8+K79p+/0YUu7HkBgrlWXwica7rsxghrtI/CM7r3U=','2014-10-29 18:32:04',1,'admin1995','','','nil@fffg.com',1,1,'2014-10-29 18:32:04'),(6,'pbkdf2_sha256$10000$inXBo87vBVuM$PVuTHkHROjGogmDmoTxy4tp0UPWYeoMYpOB1qrVtSac=','2014-10-30 16:16:08',1,'root','','','root@root.com',1,1,'2014-10-30 16:16:08');

UNLOCK TABLES;

/*Table structure for table `auth_user_groups` */

DROP TABLE IF EXISTS `auth_user_groups`;

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `group_id_refs_id_274b862c` (`group_id`),
  CONSTRAINT `group_id_refs_id_274b862c` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `user_id_refs_id_40c41112` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `auth_user_groups` */

LOCK TABLES `auth_user_groups` WRITE;

UNLOCK TABLES;

/*Table structure for table `auth_user_user_permissions` */

DROP TABLE IF EXISTS `auth_user_user_permissions`;

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `permission_id_refs_id_35d9ac25` (`permission_id`),
  CONSTRAINT `permission_id_refs_id_35d9ac25` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `user_id_refs_id_4dc23c39` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `auth_user_user_permissions` */

LOCK TABLES `auth_user_user_permissions` WRITE;

UNLOCK TABLES;

/*Table structure for table `comm_closed_options` */

DROP TABLE IF EXISTS `comm_closed_options`;

CREATE TABLE `comm_closed_options` (
  `serial` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(75) NOT NULL,
  `trandateandtime` datetime NOT NULL,
  `tradename` varchar(100) NOT NULL,
  `bors` varchar(15) NOT NULL,
  `quantity` int(11) NOT NULL,
  `tranprice` double NOT NULL,
  `brockerage` double NOT NULL,
  `levearage` int(11) NOT NULL,
  `closeCP` double NOT NULL,
  `profit` double NOT NULL,
  PRIMARY KEY (`serial`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `comm_closed_options` */

LOCK TABLES `comm_closed_options` WRITE;

insert  into `comm_closed_options`(`serial`,`userid`,`trandateandtime`,`tradename`,`bors`,`quantity`,`tranprice`,`brockerage`,`levearage`,`closeCP`,`profit`) values (1,'nilmadhab','2014-10-12 11:38:47','BRENT CRUDE (USD/bbl)','Short Cover',1,1,0.02,50,1,0),(2,'nilmadhab','2014-10-12 11:38:58','BRENT CRUDE (USD/bbl)','Short Cover',18,1,0.36,50,1,0),(3,'nilmadhab','2014-10-12 15:47:02','BRENT CRUDE (USD/bbl)','Sell',1,1,0.02,50,1,0),(4,'nilmadhab','2014-10-13 00:08:40','BRENT CRUDE (USD/bbl)','Sell',1,1,0.02,50,1,0);

UNLOCK TABLES;

/*Table structure for table `comm_opened_options` */

DROP TABLE IF EXISTS `comm_opened_options`;

CREATE TABLE `comm_opened_options` (
  `userId` varchar(75) NOT NULL,
  `orderNo` int(11) NOT NULL AUTO_INCREMENT,
  `opendateandtime` datetime NOT NULL,
  `tradename` varchar(100) NOT NULL,
  `quantity` int(11) NOT NULL,
  `bors` varchar(25) NOT NULL,
  `openPrice` double NOT NULL,
  `triggerprice` double NOT NULL,
  `levearage` int(11) NOT NULL,
  PRIMARY KEY (`orderNo`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `comm_opened_options` */

LOCK TABLES `comm_opened_options` WRITE;

insert  into `comm_opened_options`(`userId`,`orderNo`,`opendateandtime`,`tradename`,`quantity`,`bors`,`openPrice`,`triggerprice`,`levearage`) values ('nilmadhab',1,'2014-10-11 03:21:38','BRENT CRUDE (USD/bbl)',1,'Buy',1,0,50),('nilmadhab',6,'2014-10-13 00:08:20','BRENT CRUDE (USD/bbl)',1,'Buy',1,0,50),('nilmadhab',7,'2014-10-18 05:01:56','1',1,'Long',1,0,100),('nilmadhab',8,'2014-10-18 05:01:59','1',1,'Long',1,0,100),('nilmadhab',9,'2014-10-18 05:02:00','1',1,'Long',1,0,100),('nilmadhab',10,'2014-10-18 05:02:00','1',1,'Long',1,0,100),('nilmadhab',11,'2014-10-18 05:02:00','1',1,'Long',1,0,100),('nilmadhab',12,'2014-10-18 05:02:00','1',1,'Long',1,0,100),('nilmadhab',13,'2014-10-18 05:02:01','1',1,'Long',1,0,100),('nilmadhab',14,'2014-10-18 05:02:02','1',1,'Long',1,0,100),('nilmadhab',15,'2014-10-18 05:02:02','1',1,'Long',1,0,100),('root_nil',16,'2014-10-30 17:49:49','1',1,'Long',1,0,100);

UNLOCK TABLES;

/*Table structure for table `comm_pending_close_options` */

DROP TABLE IF EXISTS `comm_pending_close_options`;

CREATE TABLE `comm_pending_close_options` (
  `userId` varchar(100) NOT NULL,
  `orderId` int(11) NOT NULL AUTO_INCREMENT,
  `trandateandtime` datetime NOT NULL,
  `tradename` varchar(100) NOT NULL,
  `bors` varchar(25) NOT NULL,
  `quantity` int(11) NOT NULL,
  `pendingPrice` double NOT NULL,
  `levearage` int(11) NOT NULL,
  `triggerPrice` double NOT NULL,
  `tranPrice` double NOT NULL,
  PRIMARY KEY (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `comm_pending_close_options` */

LOCK TABLES `comm_pending_close_options` WRITE;

UNLOCK TABLES;

/*Table structure for table `comm_pending_options` */

DROP TABLE IF EXISTS `comm_pending_options`;

CREATE TABLE `comm_pending_options` (
  `pendingId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` varchar(100) NOT NULL,
  `tradename` varchar(100) NOT NULL,
  `quantity` int(11) NOT NULL,
  `bors` varchar(10) NOT NULL,
  `pendingPrice` double NOT NULL,
  `triggerPrice` double NOT NULL,
  `levearage` int(11) NOT NULL,
  PRIMARY KEY (`pendingId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `comm_pending_options` */

LOCK TABLES `comm_pending_options` WRITE;

UNLOCK TABLES;

/*Table structure for table `commodity_base` */

DROP TABLE IF EXISTS `commodity_base`;

CREATE TABLE `commodity_base` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `commodity` varchar(100) NOT NULL,
  `price` double NOT NULL,
  `tradename` varchar(100) NOT NULL,
  `open` double NOT NULL,
  `identifier` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `commodity_base` */

LOCK TABLES `commodity_base` WRITE;

insert  into `commodity_base`(`id`,`name`,`commodity`,`price`,`tradename`,`open`,`identifier`) values (1,'BRENT CRUDE (USD/bbl)','1',1,'BRENT CRUDE (USD/bbl)',1,'1');

UNLOCK TABLES;

/*Table structure for table `commodity_trading` */

DROP TABLE IF EXISTS `commodity_trading`;

CREATE TABLE `commodity_trading` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commodity` varchar(75) NOT NULL,
  `tradename` varchar(50) NOT NULL,
  `price` double NOT NULL,
  `graph_id` int(11) NOT NULL,
  `open` double NOT NULL,
  `newName` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `commodity_trading` */

LOCK TABLES `commodity_trading` WRITE;

UNLOCK TABLES;

/*Table structure for table `django_admin_log` */

DROP TABLE IF EXISTS `django_admin_log`;

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_refs_id_c0d12874` (`user_id`),
  KEY `content_type_id_refs_id_93d2d1f8` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_93d2d1f8` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `user_id_refs_id_c0d12874` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `django_admin_log` */

LOCK TABLES `django_admin_log` WRITE;

UNLOCK TABLES;

/*Table structure for table `django_content_type` */

DROP TABLE IF EXISTS `django_content_type`;

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;

/*Data for the table `django_content_type` */

LOCK TABLES `django_content_type` WRITE;

insert  into `django_content_type`(`id`,`name`,`app_label`,`model`) values (1,'log entry','admin','logentry'),(2,'permission','auth','permission'),(3,'group','auth','group'),(4,'user','auth','user'),(5,'content type','contenttypes','contenttype'),(6,'session','sessions','session'),(7,'site','sites','site'),(8,'job','django_cron','job'),(9,'cron','django_cron','cron'),(10,'users','index','users'),(11,'comm closed options','index','commclosedoptions'),(12,'comm opened options','index','commopenedoptions'),(13,'comm pending close options','index','commpendingcloseoptions'),(14,'comm pending options','index','commpendingoptions'),(15,'commodity base','index','commoditybase'),(16,'commodity trading','index','commoditytrading'),(17,'forex base','index','forexbase'),(18,'forex closed options','index','forexclosedoptions'),(19,'indices','index','indices'),(20,'forex opened options','index','forexopenedoptions'),(21,'forex pending options','index','forexpendingoptions'),(22,'forex rankings','index','forexrankings'),(23,'forex user deductions','index','forexuserdeductions'),(24,'forexbasedata','index','forexbasedata'),(25,'futures base','index','futuresbase'),(26,'futures opened or closed','index','futuresopenedorclosed'),(27,'market open','index','marketopen'),(28,'options base','index','optionsbase'),(29,'options opened or closed','index','optionsopenedorclosed'),(30,'rediff links','index','redifflinks'),(31,'status','index','status'),(32,'stockprice','index','stockprice'),(33,'stocks rate','index','stocksrate'),(34,'wud closed options','index','wudclosedoptions'),(35,'wud opened options','index','wudopenedoptions'),(36,'wud pending close options','index','wudpendingcloseoptions'),(37,'wud pending options','index','wudpendingoptions'),(38,'wud rankings','index','wudrankings'),(39,'wud user deductions','index','wuduserdeductions'),(40,'notifications','index','notifications'),(41,'users','algotrade','users'),(42,'comm closed options','algotrade','commclosedoptions'),(43,'comm opened options','algotrade','commopenedoptions'),(44,'comm pending close options','algotrade','commpendingcloseoptions'),(45,'comm pending options','algotrade','commpendingoptions'),(46,'commodity base','algotrade','commoditybase'),(47,'commodity trading','algotrade','commoditytrading'),(48,'forex base','algotrade','forexbase'),(49,'forex closed options','algotrade','forexclosedoptions'),(50,'indices','algotrade','indices'),(51,'forex opened options','algotrade','forexopenedoptions'),(52,'forex pending options','algotrade','forexpendingoptions'),(53,'forex rankings','algotrade','forexrankings'),(54,'forex user deductions','algotrade','forexuserdeductions'),(55,'forexbasedata','algotrade','forexbasedata'),(56,'futures base','algotrade','futuresbase'),(57,'futures opened or closed','algotrade','futuresopenedorclosed'),(58,'market open','algotrade','marketopen'),(59,'options base','algotrade','optionsbase'),(60,'options opened or closed','algotrade','optionsopenedorclosed'),(61,'rediff links','algotrade','redifflinks'),(62,'status','algotrade','status'),(63,'stockprice','algotrade','stockprice'),(64,'stocks rate','algotrade','stocksrate'),(65,'wud closed options','algotrade','wudclosedoptions'),(66,'wud opened options','algotrade','wudopenedoptions'),(67,'wud pending close options','algotrade','wudpendingcloseoptions'),(68,'wud pending options','algotrade','wudpendingoptions'),(69,'wud rankings','algotrade','wudrankings'),(70,'wud user deductions','algotrade','wuduserdeductions'),(71,'notifications','algotrade','notifications'),(72,'auth group','data','authgroup'),(73,'auth group permissions','data','authgrouppermissions'),(74,'auth permission','data','authpermission'),(75,'auth user','data','authuser'),(76,'auth user groups','data','authusergroups'),(77,'auth user user permissions','data','authuseruserpermissions'),(78,'django admin log','data','djangoadminlog'),(79,'django content type','data','djangocontenttype'),(80,'django session','data','djangosession'),(81,'django site','data','djangosite'),(82,'futures base','data','futuresbase'),(83,'futures opened or closed','data','futuresopenedorclosed'),(84,'market open','data','marketopen'),(85,'options base','data','optionsbase'),(86,'options opened or closed','data','optionsopenedorclosed'),(87,'rediff link','data','redifflink'),(88,'rediff links','data','redifflinks'),(89,'status','data','status'),(90,'stockprice','data','stockprice'),(91,'stocks rate','data','stocksrate'),(92,'wud closed options','data','wudclosedoptions'),(93,'wud opened options','data','wudopenedoptions'),(94,'wud pending close options','data','wudpendingcloseoptions'),(95,'wud pending options','data','wudpendingoptions'),(96,'wud rankings','data','wudrankings'),(97,'wud user deductions','data','wuduserdeductions'),(98,'nonce','piston','nonce'),(99,'resource','piston','resource'),(100,'consumer','piston','consumer'),(101,'token','piston','token');

UNLOCK TABLES;

/*Table structure for table `django_cron_cron` */

DROP TABLE IF EXISTS `django_cron_cron`;

CREATE TABLE `django_cron_cron` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `executing` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `django_cron_cron` */

LOCK TABLES `django_cron_cron` WRITE;

insert  into `django_cron_cron`(`id`,`executing`) values (1,1);

UNLOCK TABLES;

/*Table structure for table `django_cron_job` */

DROP TABLE IF EXISTS `django_cron_job`;

CREATE TABLE `django_cron_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `run_frequency` int(10) unsigned NOT NULL,
  `last_run` datetime NOT NULL,
  `instance` longtext NOT NULL,
  `args` longtext NOT NULL,
  `kwargs` longtext NOT NULL,
  `queued` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

/*Data for the table `django_cron_job` */

LOCK TABLES `django_cron_job` WRITE;

insert  into `django_cron_job`(`id`,`name`,`run_frequency`,`last_run`,`instance`,`args`,`kwargs`,`queued`) values (1,'<class \'index.cron.UpdateOpenPrice\'>',86400,'2014-10-14 04:47:42','ccopy_reg\n_reconstructor\np1\n(cindex.cron\nUpdateOpenPrice\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(2,'<class \'index.cron.UpdateStockPrice\'>',60,'2014-10-14 05:16:41','ccopy_reg\n_reconstructor\np1\n(cindex.cron\nUpdateStockPrice\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(3,'<class \'index.cron.UpdateCommodityPrice\'>',62,'2014-10-14 05:16:42','ccopy_reg\n_reconstructor\np1\n(cindex.cron\nUpdateCommodityPrice\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(4,'<class \'index.cron.UpdateIndices\'>',3600,'2014-10-14 05:16:43','ccopy_reg\n_reconstructor\np1\n(cindex.cron\nUpdateIndices\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(5,'<class \'index.cron.UpdateForexRanks\'>',1800,'2014-10-14 05:16:43','ccopy_reg\n_reconstructor\np1\n(cindex.cron\nUpdateForexRanks\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(6,'<class \'index.cron.ForexTrigClose\'>',600,'2014-10-14 05:16:43','ccopy_reg\n_reconstructor\np1\n(cindex.cron\nForexTrigClose\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(7,'<class \'index.cron.CommTrigClose\'>',700,'2014-10-14 05:16:43','ccopy_reg\n_reconstructor\np1\n(cindex.cron\nCommTrigClose\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(8,'<class \'index.cron.CommPending\'>',710,'2014-10-14 05:16:43','ccopy_reg\n_reconstructor\np1\n(cindex.cron\nCommPending\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(9,'<class \'index.cron.ForexPending\'>',610,'2014-10-14 05:16:43','ccopy_reg\n_reconstructor\np1\n(cindex.cron\nForexPending\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(10,'<class \'algotrade.cron.UpdateForexRanks\'>',1800,'2014-09-28 17:56:55','ccopy_reg\n_reconstructor\np1\n(calgotrade.cron\nUpdateForexRanks\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',0),(11,'<class \'algotrade.cron.ForexTrigClose\'>',300,'2014-10-14 05:16:44','ccopy_reg\n_reconstructor\np1\n(calgotrade.cron\nForexTrigClose\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(12,'<class \'algotrade.cron.CommTrigClose\'>',300,'2014-09-28 17:56:55','ccopy_reg\n_reconstructor\np1\n(calgotrade.cron\nCommTrigClose\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',0),(13,'<class \'algotrade.cron.CommPending\'>',300,'2014-09-28 17:56:55','ccopy_reg\n_reconstructor\np1\n(calgotrade.cron\nCommPending\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',0),(14,'<class \'algotrade.cron.ForexPending\'>',300,'2014-09-28 17:56:55','ccopy_reg\n_reconstructor\np1\n(calgotrade.cron\nForexPending\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',0),(15,'<class \'woodstock.cron.UpdateOpeningPrice\'>',86400,'2014-10-14 04:48:04','ccopy_reg\n_reconstructor\np1\n(cwoodstock.cron\nUpdateOpeningPrice\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(16,'<class \'woodstock.cron.UpdateRanks\'>',1800,'2014-10-14 05:16:44','ccopy_reg\n_reconstructor\np1\n(cwoodstock.cron\nUpdateRanks\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(17,'<class \'woodstock.cron.UpdateLastFriday\'>',86400,'2014-10-14 04:48:04','ccopy_reg\n_reconstructor\np1\n(cwoodstock.cron\nUpdateLastFriday\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(18,'<class \'woodstock.cron.UpdateLastMonday\'>',86400,'2014-10-14 04:48:04','ccopy_reg\n_reconstructor\np1\n(cwoodstock.cron\nUpdateLastMonday\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(19,'<class \'woodstock.cron.StrigClose\'>',600,'2014-10-14 05:16:44','ccopy_reg\n_reconstructor\np1\n(cwoodstock.cron\nStrigClose\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(20,'<class \'woodstock.cron.UpdateOpenOrders\'>',300,'2014-10-14 05:16:44','ccopy_reg\n_reconstructor\np1\n(cwoodstock.cron\nUpdateOpenOrders\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(21,'<class \'woodstock.cron.UpdateCloseOrders\'>',86400,'2014-10-14 04:48:05','ccopy_reg\n_reconstructor\np1\n(cwoodstock.cron\nUpdateCloseOrders\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(22,'<class \'woodstock.cron.PriceUpdate\'>',900,'2014-10-14 05:14:53','ccopy_reg\n_reconstructor\np1\n(cwoodstock.cron\nPriceUpdate\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(23,'<class \'woodstock.cron.Diff\'>',86400,'2014-10-14 05:15:07','ccopy_reg\n_reconstructor\np1\n(cwoodstock.cron\nDiff\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(24,'<class \'woodstock.cron.CancelPendingOrder\'>',300,'2014-10-14 05:15:07','ccopy_reg\n_reconstructor\np1\n(cwoodstock.cron\nCancelPendingOrder\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(25,'<class \'woodstock.cron.ftrigClose\'>',300,'2014-10-14 05:15:07','ccopy_reg\n_reconstructor\np1\n(cwoodstock.cron\nftrigClose\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(26,'<class \'woodstock.cron.WdOpen\'>',86400,'2014-10-14 05:15:07','ccopy_reg\n_reconstructor\np1\n(cwoodstock.cron\nWdOpen\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(27,'<class \'woodstock.cron.WriteStock\'>',300,'2014-10-14 05:15:07','ccopy_reg\n_reconstructor\np1\n(cwoodstock.cron\nWriteStock\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(28,'<class \'woodstock.cron.FridayOptions\'>',86400,'2014-10-14 05:15:07','ccopy_reg\n_reconstructor\np1\n(cwoodstock.cron\nFridayOptions\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(29,'<class \'woodstock.cron.WeeklyVolatility\'>',86400,'2014-10-14 05:15:07','ccopy_reg\n_reconstructor\np1\n(cwoodstock.cron\nWeeklyVolatility\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(30,'<class \'woodstock.cron.uPrice\'>',300,'2014-10-14 05:15:12','ccopy_reg\n_reconstructor\np1\n(cwoodstock.cron\nuPrice\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(31,'<class \'woodstock.cron.UpdateM\'>',86400,'2014-10-14 05:15:12','ccopy_reg\n_reconstructor\np1\n(cwoodstock.cron\nUpdateM\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(32,'<class \'woodstock.cron.CurrentPremium\'>',300,'2014-10-14 05:16:05','ccopy_reg\n_reconstructor\np1\n(cwoodstock.cron\nCurrentPremium\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(33,'<class \'woodstock.cron.FutureDiff\'>',300,'2014-10-14 05:16:09','ccopy_reg\n_reconstructor\np1\n(cwoodstock.cron\nFutureDiff\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(34,'<class \'woodstock.cron.ContractFutures\'>',300,'2014-10-14 05:16:09','ccopy_reg\n_reconstructor\np1\n(cwoodstock.cron\nContractFutures\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(35,'<class \'woodstock.cron.DailyFutures\'>',86400,'2014-10-14 05:16:10','ccopy_reg\n_reconstructor\np1\n(cwoodstock.cron\nDailyFutures\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(36,'<class \'woodstock.cron.WriteFutures\'>',300,'2014-10-14 05:16:10','ccopy_reg\n_reconstructor\np1\n(cwoodstock.cron\nWriteFutures\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(37,'<class \'woodstock.cron.StartUpdateAll\'>',86400,'2014-10-14 05:16:10','ccopy_reg\n_reconstructor\np1\n(cwoodstock.cron\nStartUpdateAll\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(38,'<class \'woodstock.cron.FridayFutures\'>',86400,'2014-10-14 05:16:10','ccopy_reg\n_reconstructor\np1\n(cwoodstock.cron\nFridayFutures\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1),(39,'<class \'woodstock.cron.StartUpdate\'>',86400,'2014-10-14 05:16:10','ccopy_reg\n_reconstructor\np1\n(cwoodstock.cron\nStartUpdate\np2\nc__builtin__\nobject\np3\nNtRp4\n.','(t.','(dp1\n.',1);

UNLOCK TABLES;

/*Table structure for table `django_session` */

DROP TABLE IF EXISTS `django_session`;

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `django_session` */

LOCK TABLES `django_session` WRITE;

insert  into `django_session`(`session_key`,`session_data`,`expire_date`) values ('3urzk9t7stihqiyk8hrtr098qwnf4lew','YTE4MTJjZjYwNTVmOWJiYjMxZTE2Y2RjMDkyMmU4OGEwNTdiOWRjZTqAAn1xAS4=','2014-11-12 17:49:25'),('6yav2p09n767rv54dycp997mb8a5of0d','NDdmMjhiMDlkM2NiYWI2M2UwNzNmNTI4NWNiOTRhZjRhYmE5ZmM5ZDqAAn1xAShYDwAAAF9zZXNzaW9uX2V4cGlyeXECSwBVEl9hdXRoX3VzZXJfYmFja2VuZHEDVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEEVQd1c2VyX2lkcQWKAs0jVQR1c2VycQZYCQAAAG5pbG1hZGhhYnEHVQ1fYXV0aF91c2VyX2lkcQiKAs0jdS4=','2014-10-31 03:16:23'),('8ymzkzsynhlr8x8opqbe28ll7xxt5fvy','ZTllMjA0NmRjNjUxNDg3MDdjMDQwMjA0Mzg3ZDdlODMwNmI4NDQ5NTqAAn1xAShVB3VzZXJfaWSKAs0jVQ1fYXV0aF91c2VyX2lkigLNI1gKAAAAdGVzdGNvb2tpZVgGAAAAd29ya2VkcQJVEl9hdXRoX3VzZXJfYmFja2VuZFUpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmRVBHVzZXJYCQAAAG5pbG1hZGhhYlgPAAAAX3Nlc3Npb25fZXhwaXJ5SwB1Lg==','2014-11-01 11:25:18'),('bi4hwvuv20876aygwe3putkq3ubvon34','MzkzYTU2ZGY4ZTgzMTE3YzA4ZWE0MmFkMmFiNGY1OGNmMDVhYWJhNzqAAn1xAVgKAAAAdGVzdGNvb2tpZVgGAAAAd29ya2VkcQJzLg==','2014-11-12 18:14:49'),('cwdcrgrm292dpoj6xxf3zm5w9ao7q3ut','NjAxOTMxODExNWExOTI2MzM5ODM2MGU4NTI1OTY2ZWFiMmQ2YjY5MTqAAn1xAShYDwAAAF9zZXNzaW9uX2V4cGlyeXECSwBVEl9hdXRoX3VzZXJfYmFja2VuZHEDVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEEVQd1c2VyX2lkcQWKAQFVBHVzZXJxBlgIAAAAcm9vdF9uaWxxB1UNX2F1dGhfdXNlcl9pZHEIigEBdS4=','2014-11-13 16:49:37'),('d1d0fx1twfkhl3uanf486zer1ygi0zoc','YTE4MTJjZjYwNTVmOWJiYjMxZTE2Y2RjMDkyMmU4OGEwNTdiOWRjZTqAAn1xAS4=','2014-11-12 17:38:25'),('dw1vn4tss3meq9fj55czksluwh24owdj','YTE4MTJjZjYwNTVmOWJiYjMxZTE2Y2RjMDkyMmU4OGEwNTdiOWRjZTqAAn1xAS4=','2014-11-12 16:41:27'),('ffa8p91oq2dlksgo1oev5ng3vk885kmj','YjA3NzQ0ZDc0MWY5NGFlZGY0MzVhOWMzODMzNmQ2YzliMGJhZDA1OTqAAn1xAShYDwAAAF9zZXNzaW9uX2V4cGlyeXECSwBVEl9hdXRoX3VzZXJfYmFja2VuZHEDVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEEVQd1c2VyX2lkcQWKAvtNVQR1c2VycQZYCgAAAG5pbG1hZGhhYjFxB1UNX2F1dGhfdXNlcl9pZHEIigL7TXUu','2014-11-03 17:27:35'),('jnabrtlvatq5z59vpdwqnhvnbo6uadeo','NDdmMjhiMDlkM2NiYWI2M2UwNzNmNTI4NWNiOTRhZjRhYmE5ZmM5ZDqAAn1xAShYDwAAAF9zZXNzaW9uX2V4cGlyeXECSwBVEl9hdXRoX3VzZXJfYmFja2VuZHEDVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEEVQd1c2VyX2lkcQWKAs0jVQR1c2VycQZYCQAAAG5pbG1hZGhhYnEHVQ1fYXV0aF91c2VyX2lkcQiKAs0jdS4=','2014-11-12 16:37:16'),('jtu86goelo2ubxx1huxkoo6md60ldzac','NDdmMjhiMDlkM2NiYWI2M2UwNzNmNTI4NWNiOTRhZjRhYmE5ZmM5ZDqAAn1xAShYDwAAAF9zZXNzaW9uX2V4cGlyeXECSwBVEl9hdXRoX3VzZXJfYmFja2VuZHEDVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEEVQd1c2VyX2lkcQWKAs0jVQR1c2VycQZYCQAAAG5pbG1hZGhhYnEHVQ1fYXV0aF91c2VyX2lkcQiKAs0jdS4=','2014-10-31 13:15:32'),('k24nvje87kmlz1x7wa8a5wjatogczyxb','YTE4MTJjZjYwNTVmOWJiYjMxZTE2Y2RjMDkyMmU4OGEwNTdiOWRjZTqAAn1xAS4=','2014-11-12 16:27:32'),('oosr2fdb8hkitsmodvo2fes43exro38f','NjAxOTMxODExNWExOTI2MzM5ODM2MGU4NTI1OTY2ZWFiMmQ2YjY5MTqAAn1xAShYDwAAAF9zZXNzaW9uX2V4cGlyeXECSwBVEl9hdXRoX3VzZXJfYmFja2VuZHEDVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEEVQd1c2VyX2lkcQWKAQFVBHVzZXJxBlgIAAAAcm9vdF9uaWxxB1UNX2F1dGhfdXNlcl9pZHEIigEBdS4=','2014-11-13 17:12:20'),('q2he6ycv951lwmwpb7pp8ksyy7xocolw','NmRkMWJhODMzYzI2YTk1MDU0YmQ1ZmZkZTlmNDcyZjhmZTQ5OWViYjqAAn1xAShYDwAAAF9zZXNzaW9uX2V4cGlyeXECSwBVB3VzZXJfaWRxA4oBAVUNX2F1dGhfdXNlcl9pZIoBAVUSX2F1dGhfdXNlcl9iYWNrZW5kVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZFUEdXNlcnEEWAQAAAByb290dS4=','2014-11-13 16:46:47'),('w6jtmrp0qu7bqdf1fiatg7b69h42h995','NDdmMjhiMDlkM2NiYWI2M2UwNzNmNTI4NWNiOTRhZjRhYmE5ZmM5ZDqAAn1xAShYDwAAAF9zZXNzaW9uX2V4cGlyeXECSwBVEl9hdXRoX3VzZXJfYmFja2VuZHEDVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEEVQd1c2VyX2lkcQWKAs0jVQR1c2VycQZYCQAAAG5pbG1hZGhhYnEHVQ1fYXV0aF91c2VyX2lkcQiKAs0jdS4=','2014-11-01 04:18:20'),('wbhmrf5rnoqvm9jqu5llqmqg70g83px0','MDE1NDcxNTFhY2FlNjA1N2JlZGY1YzdjYzMwNzMwNzdjNjRkMTMzZTqAAn1xAShYDwAAAF9zZXNzaW9uX2V4cGlyeXECSwBVB3VzZXJfaWRxA4oCzSNVDV9hdXRoX3VzZXJfaWSKAs0jVRJfYXV0aF91c2VyX2JhY2tlbmRVKWRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kcQRVBHVzZXJxBVgJAAAAbmlsbWFkaGFicQZ1Lg==','2014-11-12 19:44:26'),('y9w3z5dtcom8z1749y4cjck4zkm3a27p','YTE4MTJjZjYwNTVmOWJiYjMxZTE2Y2RjMDkyMmU4OGEwNTdiOWRjZTqAAn1xAS4=','2014-11-12 16:53:30');

UNLOCK TABLES;

/*Table structure for table `django_site` */

DROP TABLE IF EXISTS `django_site`;

CREATE TABLE `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `django_site` */

LOCK TABLES `django_site` WRITE;

UNLOCK TABLES;

/*Table structure for table `forex_base` */

DROP TABLE IF EXISTS `forex_base`;

CREATE TABLE `forex_base` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `currency` varchar(50) NOT NULL,
  `bid` double NOT NULL,
  `ask` double NOT NULL,
  `bidvar` varchar(50) NOT NULL,
  `askvar` varchar(50) NOT NULL,
  `open` double NOT NULL,
  `identifier` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `currency` (`currency`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `forex_base` */

LOCK TABLES `forex_base` WRITE;

insert  into `forex_base`(`id`,`currency`,`bid`,`ask`,`bidvar`,`askvar`,`open`,`identifier`) values (1,'USD to INR',61.02,61.03,'usd to inrbid','usd to inrask',61.02,'usdinr'),(2,'USD to CAD',1.1204,1.1205,'usd to cadbid','usd to cadask',1.1204,'usdcad'),(3,'USD to JPY',107.09,107.14,'usd to jpybid','usd to jpyask',107.09,'usdjpy'),(4,'USD to GBP',0.6219,0.6221,'usd to gbpbid','usd to gbpask',0.6219,'usdgbp'),(5,'USD to EUR',0.7858,0.7859,'usd to eurbid','usd to eurask',0.7858,'usdeur'),(6,'USD to CHF',0.9496,0.9501,'usd to chfbid','usd to chfask',0.9496,'usdchf'),(7,'USD to AUD',1.1361,1.1369,'usd to audbid','usd to audask',1.1361,'usdaud'),(8,'USD to NZD',1.2652,1.2655,'usd to nzdbid','usd to nzdask',1.2652,'usdnzd');

UNLOCK TABLES;

/*Table structure for table `forex_closed_options` */

DROP TABLE IF EXISTS `forex_closed_options`;

CREATE TABLE `forex_closed_options` (
  `userid` longtext NOT NULL,
  `orderno` int(11) NOT NULL AUTO_INCREMENT,
  `opendateandtime` datetime NOT NULL,
  `currency` longtext NOT NULL,
  `levearage` int(11) NOT NULL,
  `bors` varchar(10) NOT NULL,
  `lots` int(11) NOT NULL,
  `openprice` double NOT NULL,
  `closeprice` double NOT NULL,
  `fltprofit` double NOT NULL,
  `closedateandtime` datetime NOT NULL,
  `fltstorage` double DEFAULT NULL,
  PRIMARY KEY (`orderno`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `forex_closed_options` */

LOCK TABLES `forex_closed_options` WRITE;

insert  into `forex_closed_options`(`userid`,`orderno`,`opendateandtime`,`currency`,`levearage`,`bors`,`lots`,`openprice`,`closeprice`,`fltprofit`,`closedateandtime`,`fltstorage`) values ('srinivasan',1,'2013-11-05 19:06:07','USD to INR',100,'Short',1,61.83,5,1136600,'2013-11-05 19:17:37',NULL),('srinivasan',2,'2013-11-05 19:07:08','USD to INR',100,'Long',1,61.85,61.83,-32.35,'2013-11-05 19:23:54',NULL),('srinivasan',3,'2013-11-05 19:21:52','USD to INR',100,'Short',1,61.63,5,1132600,'2013-11-05 19:24:12',NULL),('srinivasan',4,'2013-11-05 19:25:18','USD to INR',100,'Short',1,61.625,5,-16.22,'2013-11-05 19:25:30',NULL),('srinivasan',5,'2013-11-05 19:07:26','USD to INR',100,'Short',1,61.83,61.83,-594.86,'2013-11-05 19:34:36',NULL),('srinivasan',6,'2013-11-05 19:26:25','USD to INR',100,'Short',1,61.625,62.2,-924.44,'2013-11-05 19:42:49',NULL),('srinivasan',7,'2013-11-05 19:43:22','USD to INR',100,'Short',1,62.16,62.2,-64.31,'2013-11-05 19:43:33',NULL),('srinivasan',8,'2013-11-05 20:18:54','USD to INR',100,'Short',1,62.16,62.2,-64.31,'2013-11-05 20:20:28',NULL),('srinivasan',9,'2013-11-05 20:18:58','USD to INR',100,'Short',1,62.16,62.2,-64.31,'2013-11-05 20:21:04',NULL),('srinivasan',10,'2013-11-05 20:21:27','USD to INR',100,'Short',1,62.16,62.2,-64.31,'2013-11-05 20:21:32',NULL),('srinivasan',11,'2013-11-05 20:21:28','USD to INR',100,'Short',1,62.16,62.2,-64.31,'2013-11-05 20:21:32',NULL),('srinivasan',12,'2013-11-05 20:21:28','USD to INR',100,'Short',1,62.16,62.2,-64.31,'2013-11-05 20:21:32',NULL),('nilmadhab',13,'2014-10-14 22:48:13','USD to EUR',100,'Short',1,1,0.7859,27242.65,'2014-10-17 04:01:39',NULL),('nilmadhab',14,'2014-10-12 11:36:42','USD to INR',100,'Long',1,1,61.02,98361.19,'2014-10-17 04:01:46',NULL),('nilmadhab',15,'2014-10-12 23:41:36','USD to INR',100,'Long',1,1,61.02,98361.19,'2014-10-18 05:00:33',NULL),('nilmadhab',16,'2014-10-12 23:41:30','USD to INR',100,'Long',1,1,61.02,98361.19,'2014-10-18 05:00:37',NULL),('nilmadhab',17,'2014-10-14 00:14:37','USD to INR',100,'Long',1,1,61.02,98361.19,'2014-10-18 05:00:41',NULL),('nilmadhab',18,'2014-10-12 23:41:43','USD to INR',100,'Long',1,1,61.02,98361.19,'2014-10-18 05:00:46',NULL),('nilmadhab',19,'2014-10-14 10:04:35','USD to INR',100,'Long',1,1,61.02,98361.19,'2014-10-29 20:30:28',NULL),('nilmadhab',20,'2014-10-12 20:54:29','USD to GBP',100,'Long',1,1,0.6219,-60797.56,'2014-10-29 20:31:05',NULL);

UNLOCK TABLES;

/*Table structure for table `forex_opened_options` */

DROP TABLE IF EXISTS `forex_opened_options`;

CREATE TABLE `forex_opened_options` (
  `userid` longtext NOT NULL,
  `orderno` int(11) NOT NULL AUTO_INCREMENT,
  `opendateandtime` datetime NOT NULL,
  `currency` longtext NOT NULL,
  `levearage` int(11) NOT NULL,
  `bors` varchar(10) NOT NULL,
  `lots` int(11) NOT NULL,
  `openprice` double NOT NULL,
  `triggerPrice` double NOT NULL,
  PRIMARY KEY (`orderno`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;

/*Data for the table `forex_opened_options` */

LOCK TABLES `forex_opened_options` WRITE;

insert  into `forex_opened_options`(`userid`,`orderno`,`opendateandtime`,`currency`,`levearage`,`bors`,`lots`,`openprice`,`triggerPrice`) values ('srinivasan',55,'2013-11-05 04:46:45','USD to INR',100,'Long',1,61.76,0),('srinivasan',56,'2013-11-05 19:06:05','USD to INR',100,'Long',1,61.85,5),('srinivasan',60,'2013-11-05 19:21:22','USD to INR',100,'Long',1,61.64,5),('srinivasan',62,'2013-11-05 19:25:16','USD to INR',100,'Long',1,61.635,5),('srinivasan',65,'2013-11-05 19:27:39','USD to INR',100,'Long',1,62.2,5),('nilmadhab',75,'2014-10-17 04:00:50','USD to INR',100,'Long',1,61.03,0),('nilmadhab',76,'2014-10-17 04:00:56','USD to GBP',100,'Long',1,0.6221,0),('nilmadhab',77,'2014-10-17 04:01:04','USD to GBP',100,'Long',1,0.6221,0),('nilmadhab',78,'2014-10-17 04:03:59','USD to INR',100,'Long',1,61.03,0),('nilmadhab',79,'2014-10-18 05:01:15','USD to EUR',100,'Short',1,0.7858,0),('root_nil',80,'2014-10-30 17:42:34','USD to CHF',200,'Long',9,0.9501,0);

UNLOCK TABLES;

/*Table structure for table `forex_pending_options` */

DROP TABLE IF EXISTS `forex_pending_options`;

CREATE TABLE `forex_pending_options` (
  `pendingid` int(11) NOT NULL AUTO_INCREMENT,
  `userid` longtext NOT NULL,
  `currency` longtext NOT NULL,
  `lots` int(11) NOT NULL,
  `levearage` int(11) NOT NULL,
  `bors` varchar(10) NOT NULL,
  `pendingprice` double NOT NULL,
  `triggerPrice` double NOT NULL,
  PRIMARY KEY (`pendingid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `forex_pending_options` */

LOCK TABLES `forex_pending_options` WRITE;

UNLOCK TABLES;

/*Table structure for table `forex_rankings` */

DROP TABLE IF EXISTS `forex_rankings`;

CREATE TABLE `forex_rankings` (
  `userid` varchar(50) NOT NULL,
  `balance` double NOT NULL,
  `equity` double NOT NULL,
  `margin` double NOT NULL,
  `percentage` double NOT NULL,
  `rank` int(11) NOT NULL,
  `ex_bal` double NOT NULL,
  `ex_wor` double NOT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `forex_rankings` */

LOCK TABLES `forex_rankings` WRITE;

insert  into `forex_rankings`(`userid`,`balance`,`equity`,`margin`,`percentage`,`rank`,`ex_bal`,`ex_wor`) values ('nilmadhab',3056486.1355,3056502.4055,126090,0,2,0,0),('nilmadhab1',2500000,2500000,0,0,1,0,0),('root',2500000,2500000,0,0,1,0,0),('root_nil',2499990.4486,2499052.2295,18101.8,0,1,0,0),('srinivasan',2500000,2500000,0,0,1,0,0);

UNLOCK TABLES;

/*Table structure for table `forex_user_deductions` */

DROP TABLE IF EXISTS `forex_user_deductions`;

CREATE TABLE `forex_user_deductions` (
  `Seq_no` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(55) NOT NULL,
  `orderno` int(11) NOT NULL,
  `bal_ded` double NOT NULL,
  `worth_ded` double NOT NULL,
  PRIMARY KEY (`Seq_no`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `forex_user_deductions` */

LOCK TABLES `forex_user_deductions` WRITE;

insert  into `forex_user_deductions`(`Seq_no`,`userid`,`orderno`,`bal_ded`,`worth_ded`) values (1,'srinivasan',57,1136600,1136600),(2,'srinivasan',58,-32.35,-32.35),(3,'srinivasan',61,1132600,1132600),(4,'srinivasan',63,-16.22,-16.22),(5,'srinivasan',59,-594.86,-594.86),(6,'srinivasan',64,-924.44,-924.44),(7,'srinivasan',66,-64.31,-64.31),(8,'srinivasan',67,-64.31,-64.31),(9,'srinivasan',68,-64.31,-64.31),(10,'srinivasan',69,-64.31,-64.31),(11,'srinivasan',70,-64.31,-64.31),(12,'srinivasan',71,-64.31,-64.31);

UNLOCK TABLES;

/*Table structure for table `forexbasedata` */

DROP TABLE IF EXISTS `forexbasedata`;

CREATE TABLE `forexbasedata` (
  `commodityId` int(11) NOT NULL,
  `country` varchar(65) NOT NULL,
  `commodity` varchar(65) NOT NULL,
  PRIMARY KEY (`commodityId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `forexbasedata` */

LOCK TABLES `forexbasedata` WRITE;

UNLOCK TABLES;

/*Table structure for table `futures_base` */

DROP TABLE IF EXISTS `futures_base`;

CREATE TABLE `futures_base` (
  `stockId` int(11) NOT NULL,
  `stock` varchar(200) NOT NULL,
  `javaName` varchar(65) NOT NULL,
  `closePrice` double NOT NULL,
  `startPrice2` double NOT NULL,
  `contractPrice2` double NOT NULL,
  `diff2` double NOT NULL,
  PRIMARY KEY (`stockId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `futures_base` */

LOCK TABLES `futures_base` WRITE;

insert  into `futures_base`(`stockId`,`stock`,`javaName`,`closePrice`,`startPrice2`,`contractPrice2`,`diff2`) values (1,'ABAN.NS','ABAN.NS',832.35,852.55,852.55,-245.6),(2,'ABB.NS','ABB.NS',901.9,914.25,914.25,155.75),(4,'ACC.NS','ACC.NS',983.55,996.5,996.5,396.5),(6,'ALBK.NS','ALBK.NS',251.5,258.25,258.25,-155.15),(7,'AMBUJACEM.NS','AMBUJACEM.NS',139.5,141.5,141.5,68.85),(9,'APIL.NS','APIL.NS',789.45,826.8,805.75,-150.5),(10,'ASHOKLEY.NS','ASHOKLEY.NS',75.4,77.45,18.6,26.2),(13,'AXISBANK.BO','AXISBANK.BO',1464,1503,1273.65,-881.65),(15,'BAJAJHIND.NS','BAJAJHIND.NS',128.1,128.8,14.9,3.1),(18,'BANKINDIA.NS','BANKINDIA.NS',537.45,533.7,242.25,10.15),(24,'BHUSANSTL.NS','BHUSANSTL.NS',533.6,507.4,514.85,-407.05),(25,'BIOCON.NS','BIOCON.NS',444.4,423.5,389.8,87.85),(27,'BPCL.NS','BPCL.NS',719.5,765.5,388.35,277.65),(28,'BRFL.NS','BRFL.NS',248.05,255.4,205.9,-61.5),(29,'CAIRN.NS','CAIRN.NS',334.75,338.35,334.1,-48.65),(30,'CANBK.NS','CANBK.NS',732,799.6,282.75,96.35),(32,'CESC.NS','CESC.NS',380.2,384.2,409.4,311.65),(35,'CIPLA.NS','CIPLA.NS',344.35,374.55,403.4,178.7),(36,'COLPAL.NS','COLPAL.NS',860.4,915.75,1179.1,539.3),(38,'CORPBANK.NS','CORPBANK.NS',770.9,775.9,328.65,0),(41,'DABUR.NS','DABUR.NS',103.5,102.5,161.5,52.9),(42,'DCHL.NS','DCHL.NS',125.9,132.5,144.45,-10.5),(43,'DENABANK.NS','DENABANK.NS',124.7,155.2,65.5,-6),(44,'DISHTV.NS','DISHTV.NS',55.35,56.1,58.4,-3.65),(45,'DIVISLAB.NS','DIVISLAB.NS',713.25,715.4,978.15,782.85),(46,'DLF.NS','DLF.NS',363.4,374.15,163.7,-48.95),(47,'DRREDDY.NS','DRREDDY.NS',1609.5,1735.85,2277.8,684.2),(48,'EDUCOMP.NS','EDUCOMP.NS',614.5,627.2,26.8,2.75),(49,'EKC.NS','EKC.NS',127.95,128.1,12.85,1.7),(50,'ESSAROIL.NS','ESSAROIL.NS',149.7,155.25,58.5,60.9),(52,'FINANTECH.NS','FINANTECH.NS',1146.45,1165.6,179.6,45.9),(53,'FSL.NS','FSL.NS',27.7,28.45,24,-1.15),(54,'GAIL.NS','GAIL.NS',504.45,500.05,362.4,80.95),(55,'GESHIP.NS','GESHIP.NS',326.95,317.6,303.15,105.7),(57,'GMRINFRA.NS','GMRINFRA.NS',53.65,56.5,24.35,-1.15),(58,'GRASIM.NS','GRASIM.NS',2256.7,2396.4,2883.05,552.4),(59,'GSPL.NS','GSPL.NS',123.4,115.15,65.2,23.95),(61,'GTLINFRA.NS','GTLINFRA.NS',44.5,45.2,1.9,0.85),(63,'GVKPIL.NS','GVKPIL.NS',44.2,43.65,8.8,1.35),(64,'HCC.NS','HCC.NS',63.3,63.6,15.5,15.9),(65,'HCLTECH.NS','HCLTECH.NS',416.85,432.35,1028.9,711.75),(68,'HDIL.NS','HDIL.NS',267,271.85,46.9,31.7),(69,'HEROHONDA.NS','HEROHONDA.NS',1853.95,1943.6,1770.9,67.45),(70,'HINDALCO.NS','HINDALCO.NS',211.9,212.65,121,28.6),(71,'HINDPETRO.NS','HINDPETRO.NS',500.05,488.4,239.45,269.45),(73,'HOTELEELA.NS','HOTELEELA.NS',53,54.65,16.65,4.4),(74,'IBREALEST.NS','IBREALEST.NS',210.3,195.8,68.1,-1.14999999999999),(75,'ICICIBANK.NS','ICICIBANK.NS',1131.85,1201.9,1134.4,344.7),(76,'ICSA.NS','ICSA.NS',129.2,132.5,4.55,-0.35),(77,'IDBI.NS','IDBI.NS',171.1,201.75,72,-9.75),(78,'IDEA.NS','IDEA.NS',73.3,72.45,158.2,-3.09999999999999),(79,'IDFC.NS','IDFC.NS',200.85,205.5,115.9,24.65),(80,'IFCI.NS','IFCI.NS',73.65,72.3,26.65,7.5),(81,'INDHOTEL.NS','INDHOTEL.NS',99.45,100.7,56.2,40.15),(82,'INDIACEM.NS','INDIACEM.NS',113.35,113.5,58.65,49.65),(83,'INDIAINFO.NS','INDIAINFO.NS',118.3,118.35,63.4,8.6),(84,'INDIANB.NS','INDIANB.NS',302.8,292.15,108.35,45.15),(87,'IOB.NS','IOB.NS',160.95,160.45,56.25,2.3),(88,'IOC.NS','IOC.NS',428.1,434.75,226.95,150.95),(89,'ISPATIND.NS','ISPATIND.NS',21.15,21.9,21.1,-5.65),(90,'ITC.NS','ITC.NS',171.2,171.95,302.75,44),(96,'JSWSTEEL.NS','JSWSTEEL.NS',1255.1,1369.3,907,213.55),(98,'KOTAKBANK.NS','KOTAKBANK.NS',494.45,509.05,694.9,322.05),(99,'KSOILS.NS','KSOILS.NS',55.4,60.85,63.65,-22.2),(100,'LICHSGFIN.NS','LICHSGFIN.NS',1355.15,1380.75,235,98.6),(101,'LITL.NS','LITL.NS',66.45,67.85,7.05,0.3),(102,'LT.NS','LT.NS',2019.35,2067.95,1012,439.5),(103,'LUPIN.NS','LUPIN.NS',442.35,451.2,830.4,494.6),(104,'MARUTI.NS','MARUTI.NS',1503.3,1585.15,1533.8,1446.7),(106,'MLL.NS','MLL.NS',65.3,65.65,71.1,-15.85),(107,'MOSERBAER.NS','MOSERBAER.NS',69.25,69.6,3.5,3.5),(108,'MPHASIS.NS','MPHASIS.NS',627.85,625.7,440.55,-22.5),(109,'MRPL.NS','MRPL.NS',82.8,81.6,42.4,18.15),(110,'MTNL.NS','MTNL.NS',67.25,67.9,16.65,10.9),(112,'NAGARFERT.NS','NAGARFERT.NS',34.6,35.9,35.8,-4.35),(115,'NOIDATOLL.NS','NOIDATOLL.NS',33.4,34.1,33.8,-1.59999999999999),(116,'NTPC.NS','NTPC.NS',204,204.55,202,-8.80000000000001),(118,'ONGC.NS','ONGC.NS',1343.85,1341.55,1385.3,-981.4),(124,'PATNI.NS','PATNI.NS',464.2,463.15,491.3,-46.35),(125,'PETRONET.NS','PETRONET.NS',127.05,115.3,124.5,55.6),(126,'PFC.NS','PFC.NS',369,364.35,381.4,-135.9),(127,'PIRHEALTH.NS','PIRHEALTH.NS',515.55,513.25,502.9,-53.55),(128,'PNB.NS','PNB.NS',1332.35,1305.7,1377.5,-347.5),(129,'POLARIS.NS','POLARIS.NS',172,174,171.95,-8.19999999999999),(130,'POWERGRID.NS','POWERGRID.NS',106.9,107,105.7,29.4),(131,'PRAJIND.NS','PRAJIND.NS',71.75,72.2,73.5,-11.35),(132,'PTC.NS','PTC.NS',137.2,146.05,141.95,-58.2),(133,'PUNJLLOYD.NS','PUNJLLOYD.NS',129.35,130.15,128.7,-91.7),(134,'RANBAXY.NS','RANBAXY.NS',605.45,582.75,632.4,-30.85),(135,'RCOM.NS','RCOM.NS',179.15,184.35,192.75,-9.19999999999999),(136,'RECLTD.NS','RECLTD.NS',383.3,381.1,368.35,-113.85),(138,'RELIANCE.NS','RELIANCE.NS',1081.8,1153,1147.85,-178.35),(139,'RELINFRA.NS','RELINFRA.NS',1053,1067.4,1101.35,-529.4),(140,'RENUKA.NS','RENUKA.NS',84.9,100.25,99,-82.45),(141,'RNRL.NS','RNRL.NS',39.4,39.55,41.3,-6),(142,'ROLTA.NS','ROLTA.NS',164.8,169.25,179.4,-63.25),(144,'RPOWER.NS','RPOWER.NS',159.8,160.4,166.2,-95.15),(145,'SAIL.NS','SAIL.NS',219.35,220.6,202.95,-126.75),(147,'SCI.NS','SCI.NS',179.3,180.5,185.15,-127.75),(148,'SESAGOA.NS','SESAGOA.NS',342.15,375.2,344.45,-88.2),(149,'SIEMENS.NS','SIEMENS.NS',837.65,818.75,870.2,-50.2),(150,'SINTEX.NS','SINTEX.NS',435.75,425.05,221.75,-131.9),(151,'STER.NS','STER.NS',168.2,178.5,179.4,-22.2),(153,'SUNPHARMA.NS','SUNPHARMA.NS',2139.8,2183,2300.25,-1491.65),(154,'SUNTV.NS','SUNTV.NS',516.15,513.15,527.35,-213),(155,'SUZLON.NS','SUZLON.NS',59.55,57.55,61.35,-48.7),(156,'SYNDIBANK.NS','SYNDIBANK.NS',137.45,144.05,149.1,-33.1),(157,'TATACHEM.NS','TATACHEM.NS',429.4,431.85,381,7.25),(158,'TATACOMM.NS','TATACOMM.NS',317.2,329.35,319.6,63.6),(160,'TATAPOWER.NS','TATAPOWER.NS',1428.3,1430.2,1474.9,-1389.3),(161,'TATASTEEL.NS','TATASTEEL.NS',617.5,637.25,621.5,-173.75),(163,'TCS.NS','TCS.NS',1040.2,1156.35,1106.05,1588.85),(164,'TECHM.NS','TECHM.NS',760.3,752.6,772.4,1596.45),(166,'TRIVENI.NS','TRIVENI.NS',117.8,121.55,128.2,-106.2),(167,'TTML.NS','TTML.NS',24.25,24.95,23.9,-14.2),(168,'TULIP.NS','TULIP.NS',194.4,178.6,182.5,-178.55),(169,'TV-18.NS','TV-18.NS',84.9,87.25,89.75,-3),(170,'UCOBANK.NS','UCOBANK.NS',126.6,129.75,141.7,-61.5),(172,'UNIONBANK.NS','UNIONBANK.NS',411,391.15,400.05,-188.55),(173,'UNIPHOS.NS','UNIPHOS.NS',210.35,208.2,214.45,-25.8),(174,'UNITECH.NS','UNITECH.NS',88.6,94.35,93.7,-75.35),(176,'VOLTAS.NS','VOLTAS.NS',238.9,256.2,249.5,-14.2),(178,'WIPRO.NS','WIPRO.NS',448.9,474.9,444.9,142.1),(179,'YESBANK.NS','YESBANK.NS',369.2,372.05,387.8,205.05),(180,'ZEEL.NS','ZEEL.NS',284.1,282.2,298.6,19.4);

UNLOCK TABLES;

/*Table structure for table `futures_opened_or_closed` */

DROP TABLE IF EXISTS `futures_opened_or_closed`;

CREATE TABLE `futures_opened_or_closed` (
  `orderId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` varchar(65) NOT NULL,
  `stock` varchar(65) NOT NULL,
  `quantity` int(11) NOT NULL,
  `margin` double NOT NULL,
  `expiry` varchar(65) NOT NULL,
  `expiryNo` int(11) NOT NULL,
  `lORs` varchar(65) NOT NULL,
  `openCP` double NOT NULL,
  `profitCP` double NOT NULL,
  `time` varchar(65) NOT NULL,
  `status` int(11) NOT NULL,
  `triggerprice` double NOT NULL,
  `profit` double NOT NULL,
  PRIMARY KEY (`orderId`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `futures_opened_or_closed` */

LOCK TABLES `futures_opened_or_closed` WRITE;

insert  into `futures_opened_or_closed`(`orderId`,`userId`,`stock`,`quantity`,`margin`,`expiry`,`expiryNo`,`lORs`,`openCP`,`profitCP`,`time`,`status`,`triggerprice`,`profit`) values (1,'nilmadhab','ABAN.NS',1,1,'12',12,'12',1,894.5,'12',0,12,0),(2,'nilmadhab','ABAN.NS',1,0.4,'Jan 21 2015',2,'Long',23,275.05,'2014-10-11 07:04:26',0,0,252.05),(3,'nilmadhab','ABAN.NS',1,0.3,'Jan 21 2015',2,'Long',894.5,275.05,'2014-10-12 19:04:09',0,0,-619.45),(4,'nilmadhab','ALBK.NS',2,0.4,'Jan 21 2015',2,'Long',298,298,'2014-10-12 19:05:04',0,0,0),(5,'nilmadhab','ABAN.NS',1,0.4,'Jan 21 2015',2,'Long',894.5,275.05,'2014-10-13 19:37:31',0,0,-619.45),(6,'nilmadhab','BRFL.NS',1,0.4,'Oct 12 2015',2,'Long',242.25,242.25,'2014-10-13 19:44:03',0,0,0),(7,'nilmadhab','BANKINDIA.NS',1,0.4,'Jan 21 2015',2,'Short',617.8,617.8,'2014-10-13 21:34:12',0,674,0),(8,'nilmadhab','ASHOKLEY.NS',1,0.4,'Jan 21 2015',2,'Long',84.45,84.45,'2014-10-13 21:42:46',0,0,0),(9,'nilmadhab','ASHOKLEY.NS',1,0.4,'Jan 21 2015',2,'Short',84.45,84.45,'2014-10-13 21:43:01',0,0,0),(10,'nilmadhab','ABAN.NS',1,0.4,'Jan 21 2015',2,'Long',275.05,275.05,'2014-10-14 01:40:56',0,0,0),(11,'nilmadhab','ABAN.NS',12,0.4,'Jan 21 2015',2,'Short',852.55,852.55,'2014-10-14 10:57:33',0,900,0),(12,'nilmadhab','MOSERBAER.NS',1,0.4,'November 15 2013',2,'Long',3.5,3.5,'2014-10-18 10:33:16.571000',1,0,0),(13,'nilmadhab','NOIDATOLL.NS',8,0.3,'November 15 2013',2,'Short',33.8,33.8,'2014-10-18 10:35:02.976000',1,0,0),(14,'nilmadhab1','ICSA.NS',10,0.4,'November 15 2013',2,'Short',4.55,4.55,'2014-10-20 22:37:30.532000',1,0,0),(15,'nilmadhab','GESHIP.NS',9,0.4,'November 15 2013',2,'Short',303.15,303.15,'2014-10-30 00:33:06.329000',1,0,0),(16,'nilmadhab','IOB.NS',9,0.4,'November 15 2013',2,'Short',56.25,56.25,'2014-10-30 00:33:21.124000',1,0,0);

UNLOCK TABLES;

/*Table structure for table `index_notifications` */

DROP TABLE IF EXISTS `index_notifications`;

CREATE TABLE `index_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` time NOT NULL,
  `message` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `index_notifications` */

LOCK TABLES `index_notifications` WRITE;

UNLOCK TABLES;

/*Table structure for table `indices` */

DROP TABLE IF EXISTS `indices`;

CREATE TABLE `indices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `index` varchar(100) NOT NULL,
  `value` double NOT NULL,
  `change` double NOT NULL,
  `percent` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `indices` */

LOCK TABLES `indices` WRITE;

UNLOCK TABLES;

/*Table structure for table `market_open` */

DROP TABLE IF EXISTS `market_open`;

CREATE TABLE `market_open` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oc` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `market_open` */

LOCK TABLES `market_open` WRITE;

insert  into `market_open`(`id`,`oc`) values (1,1);

UNLOCK TABLES;

/*Table structure for table `options_base` */

DROP TABLE IF EXISTS `options_base`;

CREATE TABLE `options_base` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stockid` int(11) NOT NULL,
  `stock` varchar(200) NOT NULL,
  `javaName` varchar(65) NOT NULL,
  `volatility` varchar(200) NOT NULL,
  `CallBPbiweek` varchar(200) NOT NULL,
  `PutBPbiweek` varchar(200) NOT NULL,
  `StrikePrice` varchar(200) NOT NULL,
  `UPrice` double NOT NULL,
  `High` varchar(400) NOT NULL,
  `Low` varchar(400) NOT NULL,
  `randM` varchar(200) NOT NULL,
  `lastupdated` date NOT NULL,
  `currentCall2` varchar(255) NOT NULL,
  `currentPut2` varchar(255) NOT NULL,
  `checkCall` varchar(800) NOT NULL,
  `checkPut` varchar(800) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=latin1;

/*Data for the table `options_base` */

LOCK TABLES `options_base` WRITE;

insert  into `options_base`(`id`,`stockid`,`stock`,`javaName`,`volatility`,`CallBPbiweek`,`PutBPbiweek`,`StrikePrice`,`UPrice`,`High`,`Low`,`randM`,`lastupdated`,`currentCall2`,`currentPut2`,`checkCall`,`checkPut`) values (1,1,'ABAN.NS','ABAN.NS','0.11976047904192','139.6~116.1~93.15~70.05~47.2~22.15~8.5~5.75~2~1.2~0.35','1~1~2.3~3.85~10.95~26.1~50.3~73~97.1~122.75~147.05','669.15~692.7~716.25~739.85~763.4~786.95~810.5~834.05~857.65~881.2~904.75',611,'877.75~878.80~876.05~887.95~885.00~855.00~844.40~828.00~820.00~844.70~838.85~834.00~826.80~794.00~798.00','871.00~862.20~866.90~875.60~866.60~837.20~836.50~811.10~812.30~830.00~830.00~825.00~815.60~785.00~792.00','9.54~10.52~11.73~12.25~16.55~17.45~18.82~19.71~20.05~20.08~23.35','2010-11-01','912.175~827.725~732.875~578.825~512.268~260.459~109.999~76.167~26.4~16.2','0.5~0.5~1~1.15~3.75~7.95~23.65~55.3~83.95~104.7~128.1','970.325~909.425~838.125~707.675~664.668~436.409~309.499~299.217~273.05~286.4','-44.15~-67.7~-91.25~-114.85~-138.4~-161.95~-185.5~-209.05~-232.65~-256.2~-279.75'),(2,2,'ABB.NS','ABB.NS','0.086119582552251','111.1~94.9~76.3~59.75~42.5~22.4~8.05~4.9~1.95~1.05~0.35','0.5~1.15~3~4.8~7.6~21.55~43.5~61.5~81.25~97.75~116.75','733.7~751.4~769.1~786.8~804.5~822.2~839.9~857.6~875.3~893~910.7',1074.7,'935.00~925.00~914.50~932.75~932.00~928.00~922.80~908.00~920.00~929.70~908.90~915.05~905.00~870.05~831.45','922.70~914.35~901.10~921.05~918.50~895.15~914.15~892.30~895.80~901.95~890.80~905.00~892.35~855.05~817.05','6.13~11.47~13.23~17.45~17.83~18.26~20.05~20.43~21.63~21.93~22.05','2010-11-01','393.2~393.2~393.2~393.2~350.25~278.397~259.635~240.873~222.111~203.349','14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0','52.2~69.9~87.6~105.3~80.05~-64.322~-164.55~-173.7~-181.25~-167.7','355.0~337.3~319.6~301.9~284.2~266.5~248.8~231.1~213.4~195.7~178.0'),(3,4,'ACC.NS','ACC.NS','0.084030206274391','130.75~111.6~90.85~70.65~51.5~23.6~10.65~5.4~3.3~1.85~0.65','0.8~1.85~3.2~4.2~8.85~26.2~52.8~72.9~95.4~116.8~137.6','881.85~902.55~923.25~943.95~964.65~985.35~1006.05~1026.75~1047.45~1068.15~1088.85',1394.9,'1030.00~1016.90~1005.85~1035.80~1023.95~994.90~982.80~977.00~965.00~982.00~996.80~1028.00~1012.85~983.70~1004.85','1018.35~1005.15~992.35~1021.30~1004.15~972.25~970.10~967.00~945.10~968.35~974.00~1013.10~1002.15~971.25~989.00','9.52~9.92~10.3~11.66~12.37~13.98~15.18~15.3~17.68~18.35~22.77','2010-11-01','557.782~535.84~513.898~491.956~470.014~448.072~426.13~404.188~382.246~360.304','14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0','-78.1~-105.65~-140.305~-163.005~-200.955~-272.581~-311.059~-336.5~-325.5~-312.75','527.05~506.35~485.65~464.95~444.25~423.55~402.85~382.15~361.45~340.75~320.05'),(4,6,'ALBK.NS','ALBK.NS','0.14568179489806','51.5~42.9~34.15~25.6~16.75~6.85~4.4~3.65~2.5~1.35~0.55','0.25~1.6~2.1~3.2~6.35~10.15~17.3~26.4~35.95~45.15~54.65','202.65~211.65~220.7~229.7~238.75~247.75~256.75~265.8~274.8~283.85~292.85',103.75,'241.35~242.95~242.25~247.95~245.80~241.00~237.50~231.45~235.70~244.50~254.75~260.20~261.80~258.60~254.15','238.55~238.35~239.10~242.05~240.50~235.30~233.65~226.25~232.20~241.90~249.50~256.75~254.00~250.75~250.15','8.14~8.34~8.94~9.09~13.88~14.56~16.03~17.63~18.45~20.38~23.78','2010-11-01','99.91~86.56~73.06~58.4~49.3~28.617~18.96~18.96~18.96~18.648','0.15~0.8~1~1.5~2.7~4.4~6.95~15.75~28.3~37.8~48.75','198.81~194.46~190.01~184.35~184.3~172.617~171.96~181.01~190.01~198.748','-89.9~-98.9~-107.95~-116.95~-126.0~-135.0~-144.0~-153.05~-162.05~-171.1~-180.1'),(5,7,'AMBUJACEM.NS','AMBUJACEM.NS','0.076620121732904','17.2~14.8~12.1~9.5~6.95~3.9~3.3~2.5~1.4~1.3~0.85','0.65~1.2~1.45~1.95~3.7~4~7.1~9.8~12.7~15.4~18.15','126.7~129.35~132.05~134.75~137.4~140.1~142.8~145.45~148.15~150.85~153.5',180.75,'142.80~142.65~140.70~145.50~142.80~139.50~138.95~139.00~137.00~142.00~145.00~144.20~142.50~142.50~142.75','141.50~135.60~139.05~143.05~140.15~136.10~135.50~137.00~134.30~139.00~140.00~141.65~140.35~139.15~141.00','6.69~7.6~10.64~13.5~14.22~14.28~14.36~17.95~21.15~24.12~24.66','2010-11-01','59.1005~56.2915~53.4295~50.5675~47.7585~44.8965~42.0345~39.2255~36.3635~33.5015','9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~12.447~14.797','-35.45~-33.074~-34.6~-34.45~-34.35~-31.65~-28.95~-26.3~-23.6~-20.9','63.05~60.4~57.7~55.0~52.35~49.65~46.95~44.3~41.6~42.347~42.047'),(6,9,'APIL.NS','APIL.NS','0.10372552629964','118.9~99.45~81.4~61.85~42.4~15.5~11.65~3.8~2.15~1.1~0.35','0.8~1.8~1.9~4.9~9.05~17.75~44.35~64.6~84.4~105.4~124.65','658.8~678.45~698.05~717.7~737.3~756.95~776.6~796.2~815.85~835.45~855.1',767.4,'858.00~899.00~844.40~862.00~851.85~826.90~856.90~823.10~830.90~801.25~809.30~802.50~790.10~784.00~784.80','850.00~841.00~836.55~850.65~844.40~815.30~843.20~805.00~811.10~785.10~789.95~792.25~780.00~772.40~760.35','6.46~7.09~10.16~10.22~10.78~17.65~18.44~18.93~19.42~20.37~21.2','2010-11-01','142.735~123.435~102.965~84.835~65.885~46.635~42.65~407.95~256~154.75~41.95','0.45~0.4~0.4~0.85~1.7~3.4~17.5435~42.287~53.7~77.35~94.8','34.135~34.485~33.615~35.135~35.785~36.185~51.85~436.75~304.45~222.8~129.65','109.05~89.35~69.75~50.55~31.8~13.85~8.3435~13.487~5.25~9.2999999999999~7.1'),(7,10,'ASHOKLEY.NS','ASHOKLEY.NS','0.090006618133686','10.6~9.05~7.2~5.65~4~2.65~2.3~1.8~1.4~1.05~0.45','0.95~1.2~1.5~1.95~2.2~2.75~4.05~5.9~7.7~9.4~11.2','67.25~69~70.7~72.4~74.1~75.8~77.5~79.2~80.9~82.6~84.35',44.8,'75.90~77.40~79.00~76.95~75.60~74.40~73.80~75.55~77.00~77.00~76.25~75.80~78.95~76.15~77.45','74.20~76.40~75.45~76.25~75.00~72.15~73.00~74.20~76.00~76.05~75.50~75.15~77.50~75.10~76.05','7.12~8.72~10.03~14.17~16.62~19.06~20.32~21.6~21.67~22.36~24.23','2010-11-01','58.305~58.305~52.905~52.905~46.005~34.655~34.655~29.93~23.085~19.6','0.2~0.2~0.1~0.15~0.15~0.15~2.245~3.15~4.9~6.5~8.25','80.755~82.505~78.805~80.505~75.305~65.655~67.355~64.33~59.185~57.4','-17.45~-19.2~-20.9~-22.6~-24.3~-26.0~-27.7~-29.4~-31.1~-32.8~-34.55'),(8,15,'BAJAJHIND.NS','BAJAJHIND.NS','0.059375','12.55~10.75~9.1~7.3~5.4~3.75~3.05~2.05~1.35~1.05~0.6','0.65~1.25~1.5~1.85~2.35~3.2~5.55~7.55~9.55~11.25~13.1','114.15~116~117.8~119.65~121.45~123.3~125.15~126.95~128.8~130.6~132.45',18,'135.65~134.20~134.15~134.60~131.70~129.30~129.05~126.85~131.00~131.15~130.40~131.30~131.80~126.50~125.80','133.45~132.10~132.70~132.85~128.50~127.10~127.25~125.10~129.05~129.05~128.75~129.60~130.10~124.20~124.50','8.44~9.04~9.37~9.37~10.15~14.77~18.53~19.15~19.83~22.93~23.83','2010-11-01','40.763~37.063~32.763~27.362~21.11~20.962~19.362~19.362~19.362~19.362','0.1~0.15~0.15~0.2~0.2~0.2~0.25~0.8~2.85~4.65~6.45','136.913~135.063~132.563~129.012~124.56~126.262~126.512~128.312~130.162~131.962','-91.15~-93.0~-94.8~-96.65~-98.45~-100.3~-102.15~-103.95~-105.8~-107.6~-109.45'),(9,19,'BANKINDIA.NS','BANKINDIA.NS','0.11733734486649','84.15~70.85~57~42.75~29.05~11.4~5.5~4.65~2.55~1.25~0.35','0.65~1~2.95~5.3~6.2~13.95~29.85~45.3~60.2~74.85~89.45','415.2~429.45~443.75~458~472.3~486.55~500.8~515.1~529.35~543.65~557.9',252.4,'566.65~567.00~557.45~562.50~556.75~535.20~535.75~522.00~542.70~563.00~545.00~529.90~525.90~509.85~497.80','559.25~553.00~552.05~555.00~547.35~527.15~514.00~512.10~528.50~551.00~523.55~520.10~520.45~500.60~483.05','6.81~10.05~10.81~12.81~13.86~14.37~15.52~15.79~16.03~16.4~20.09','2010-11-01','155.963~152.912~130.912~111.563~85.58~51.8~32.362~32.362~26.214~10.198','0.1~0.1~0.25~0.55~0.6~1.3~5.2~25.9~38.7~50.55~69.65','318.763~329.962~322.262~317.163~305.48~285.95~280.762~295.062~303.164~301.448','-153.8~-168.05~-182.35~-196.6~-210.9~-225.15~-239.4~-253.7~-267.95~-282.25~-288.24'),(10,25,'BHUSANSTL.NS','BHUSANSTL.NS','0.12001360478111','88.2~73.45~59.4~44.45~30.05~11.95~7.65~3.95~2.25~1.5~0.25','0.3~1.65~1.95~5.1~5.95~17.4~31.7~46.85~61.55~76.85~93','421.4~436.3~451.15~466.05~480.9~495.8~510.7~525.55~540.45~555.3~570.2',107.8,'514.35~512.00~507.00~514.90~521.55~509.70~520.25~514.85~519.30~545.40~538.95~538.55~527.00~506.85~508.00','501.55~498.40~501.00~509.20~514.35~501.20~509.60~501.50~507.00~531.10~527.20~529.15~522.15~483.65~492.10','6.19~7.16~7.56~9.75~9.9~10.61~13.15~20.22~20.28~20.3~20.85','2010-11-01','9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0','0.05~0.05~0.05~363.64~0.05~393.39~408.29~423.14~438.04~452.89~467.79','322.6~337.5~352.35~367.25~382.1~397.0~411.9~426.75~441.65~456.5','-291.35~-209.6~-202.95~7.8~-95.4~267.85~719.982~1225.9~1256.2~1557.5~1647.8'),(11,26,'BIOCON.NS','BIOCON.NS','0.16629419517506','96.5~80.1~63.4~46.4~30.5~14.55~6.05~3.45~2.05~1~0.45','0.7~1.6~2.5~3.85~8.8~12.3~30.85~48.8~66.05~84.2~102.45','326.1~343.25~360.35~377.45~394.6~411.7~428.8~445.95~463.05~480.15~497.3',477.65,'416.80~404.45~405.00~403.45~404.80~408.40~472.55~455.75~447.80~453.90~448.95~440.00~437.95~425.10~419.70','407.75~398.00~400.00~399.50~400.00~400.00~415.00~443.40~443.10~446.60~440.40~405.55~424.35~417.50~413.00','6.56~7.06~7.9~8.45~9.63~12.19~18.31~20.68~21.48~22.34~22.78','2010-11-01','264.26~232.31~201.06~159.93~120.88~77.83~56.5575~50.075~37.335~34.313','9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~72.45','112.71~97.91~83.76~59.73~37.83~11.88~-1.64~18.375~22.735~36.813','160.55~143.4~126.3~109.2~92.05~74.95~57.85~40.7~23.6~6.5~-10.65'),(12,28,'BPCL.NS','BPCL.NS','0.046308984224026','63.05~55.6~46~38.35~30.75~11.75~6.6~5.35~2.05~1.75~0.3','0.25~1.65~2.2~4.75~9.7~15.55~31.5~38.85~48.7~57~65.15','687.8~696.25~704.7~713.15~721.6~730.05~738.5~746.95~755.4~763.85~772.3',666,'753.80~750.70~748.40~764.00~728.00~707.70~705.70~718.00~718.60~724.65~722.85~725.80~713.50~728.00~740.90','745.75~742.30~740.00~742.50~710.15~701.10~695.05~706.65~713.40~716.05~718.00~718.20~706.05~712.80~734.90','9.07~9.55~12.53~12.82~14.77~15.12~15.65~19.15~21.94~22.59~22.94','2010-11-01','359.04~332.39~332.39~297.99~272.89~138.9~92.599~81.59~34.7~27.2','0.05~0.2~0.25~0.4~0.65~0.95~1.85~2~2.45~11.811~18.6','380.84~362.64~371.09~345.14~328.49~202.95~165.099~162.54~124.1~125.05','-7.8~-16.25~-24.7~-33.15~-41.6~-50.05~-58.5~-66.95~-75.4~-83.85~-92.3'),(13,29,'BRFL.NS','BRFL.NS','0.12525837122778','40.4~33.65~27.1~20.1~13.55~6.2~4.4~3.6~1.95~1.5~0.3','0.5~1.05~2.05~3.15~3.85~8.35~14.2~21.15~28.5~35.7~42.6','185.4~192.25~199.15~206.05~212.9~219.8~226.7~233.55~240.45~247.35~254.2',144.4,'259.00~255.80~258.85~261.10~257.05~256.70~255.50~250.40~248.25~248.40~249.80~246.00~241.70~230.00~219.80','256.35~255.00~257.00~257.80~254.30~255.15~251.00~247.10~246.00~246.35~248.10~244.05~238.00~226.75~211.10','6.44~7.14~8.51~8.98~11.95~12.13~16.61~17.5~20.38~21.34~21.5','2010-11-01','9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0','1.15~2.45~5.15~6.1~5.015~7.565~10.247~17.508~103.27~110.17~117.02','50.0~56.85~63.75~70.65~77.5~84.4~91.3~98.15~105.05~111.95','-32.0~-37.4~-34.65~-35.8~-40.585~-31.585~-11.303~-3.642~12.995~17.095~24.745'),(14,30,'CAIRN.NS','CAIRN.NS','0.059638554216867','32.85~28.4~23.35~18.9~14.4~7.2~5.9~2.95~1.65~1.35~0.4','0.35~1.2~2.45~3.5~6.05~7.25~14.9~19.8~24.9~29.8~34.05','297~301.75~306.55~311.35~316.1~320.9~325.7~330.45~335.25~340.05~344.8',285.45,'355.00~345.45~343.15~344.75~340.00~341.90~340.65~337.90~339.80~339.00~338.00~338.00~330.95~331.75~327.50','340.00~340.25~339.05~338.05~337.45~336.25~337.65~335.00~337.00~336.15~335.60~335.50~328.00~322.10~321.50','6.91~8.43~8.58~10.35~11.55~16.19~17.71~20.51~21.36~21.52~22.45','2010-11-01','13.863~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0','0.1~0.1~0.15~0.3~0.9~1.25~6~9.8~14.15~16.8~22.35','25.413~25.3~30.1~34.9~39.65~44.45~49.25~54.0~58.8~63.6','-2.55~-7.3~-12.1~-16.9~-17.45~-20.75~-14.5~-11.7~-11.15~-9.0~-10.0'),(15,31,'CANBK.NS','CANBK.NS','0.16697750959956','169.4~140.8~111.85~81.55~53.65~21.9~12.25~7.3~2.55~1.05~0.5','0.65~1.85~2.1~4.5~11.6~25.55~54.7~85.85~116.25~148.75~179.15','570.9~601.05~631.15~661.25~691.4~721.5~751.6~781.75~811.85~841.95~872.1',379.1,'625.95~628.00~632.00~649.00~652.90~644.80~655.50~638.20~693.70~707.40~740.70~733.80~737.80~723.00~733.40','620.15~610.15~620.75~638.85~643.00~635.50~645.65~626.55~661.10~694.25~728.00~725.05~728.30~710.30~726.00','5.66~7.53~7.99~9.89~16.28~17.41~17.62~18.37~19.58~21.37~21.83','2010-11-01','460.3~456.5~383.3~330.535~317.9~158.025~158.025~117.21~68.26~43.74','0.25~1.25~1.25~2.65~6.2~13.2~37.4~66.4~98.2~133.3~160.75','652.1~678.45~635.35~612.685~630.2~500.425~530.525~519.86~501.01~506.59','-182.8~-212.95~-243.05~-273.15~-303.3~-333.4~-363.5~-393.65~-423.75~-453.85~-484.0'),(16,33,'CESC.NS','CESC.NS','0.060130718954248','38.05~32.95~27.85~22.5~17.05~10.1~5.9~3.9~1.8~1.15~0.4','0.45~1.05~1.8~3.05~5.35~9~16.8~22.7~28.75~34.6~39.7','343.4~348.95~354.55~360.15~365.7~371.3~376.9~382.45~388.05~393.65~399.2',721.05,'400.80~396.45~395.50~397.25~394.00~385.00~389.00~380.90~379.55~385.90~385.00~385.00~384.40~377.70~377.95','397.00~394.20~393.15~393.55~390.05~377.30~384.00~377.15~375.10~380.45~381.00~381.00~382.05~371.00~374.00','7.55~7.97~11.21~11.56~14~14.78~16.01~16.46~17.58~19.8~20.28','2010-11-01','407.5195~401.6365~395.7005~389.7645~383.8815~377.9455~372.0095~366.1265~360.1905~354.2545','14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0','-195.14~-206.49~-183.69~-207.64~-219.251~-263.888~-93.6~-198.15~-259.35~-286.2','391.65~386.1~380.5~374.9~369.35~363.75~358.15~352.6~347.0~341.4~335.85'),(17,36,'CIPLA.NS','CIPLA.NS','0.09811210729645','52.9~44.45~35.85~28.1~19.15~7.75~4.45~3.95~2.6~1.1~0.8','0.25~1.6~1.65~4~4.9~9.45~20.25~28.55~37.45~47~56','309.65~318.3~327~335.65~344.3~352.95~361.6~370.25~378.9~387.6~396.25',582.1,'339.95~341.50~341.00~342.60~336.65~334.00~329.70~335.85~338.20~348.70~356.20~355.60~359.80~354.95~358.75','330.00~338.15~338.05~338.40~333.05~326.15~326.50~331.90~335.10~342.50~345.00~351.15~355.65~348.10~354.60','6.01~8.61~9.62~15.66~16.15~17.62~17.74~20.88~21.07~23.9~24.21','2010-11-01','294.618~285.449~276.227~267.058~257.889~248.72~239.551~230.382~221.213~211.991','14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0','-121.67~-104.2~-114.37~-88.9~-124.02~-170.72~-173.91~-166.867~-169.089~-177.75','286.45~277.8~269.1~260.45~251.8~243.15~234.5~225.85~217.2~208.5~199.85'),(18,37,'COLPAL.NS','COLPAL.NS','0.052059346489055','82.05~71.85~60.05~48.1~36.8~17.8~8.35~4.8~2.35~1.7~0.35','0.45~1.4~3.2~6.05~7.85~20.6~38.25~50.65~63.05~73.9~86.2','822.85~834.35~845.8~857.25~868.7~880.15~891.6~903.05~914.5~925.95~937.45',1718.4,'875.00~859.90~850.00~863.00~860.00~867.00~867.00~855.00~859.60~877.00~868.95~864.40~877.50~880.00~900.00','858.25~851.65~842.00~850.00~849.00~835.35~858.00~847.25~848.50~854.00~861.00~858.10~858.00~869.25~880.00','5.37~6.53~9.95~10.28~13.27~13.72~15.57~17.02~19.39~20.43~23.02','2010-11-01','966.467~954.277~942.14~930.003~917.866~905.729~893.592~881.455~869.318~857.181','14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0','-668.795~-660.845~-625.195~-653.795~-655.495~-723.295~-756.896~-770.872~-785.85~-778.45','909.55~898.05~886.6~875.15~863.7~852.25~840.8~829.35~817.9~806.45~794.95'),(19,39,'CORPBANK.NS','CORPBANK.NS','0.12527741892742','136.35~113.65~92.15~68.35~47.25~22.45~11.5~3.95~2.25~1.15~1','0.55~1.8~2.95~4.95~8.65~16.75~48.45~71.7~95.35~120.35~144.95','629.3~652.65~676.05~699.4~722.8~746.15~769.5~792.9~816.25~839.65~863',328.65,'726.50~729.70~721.45~747.00~734.80~715.15~729.70~720.00~734.05~760.00~802.00~797.00~788.80~760.00~762.00','720.05~720.55~715.75~732.55~725.15~707.45~722.00~714.00~712.90~739.00~775.00~784.30~782.05~740.00~746.20','5.08~6.56~6.85~7.12~13.9~14.79~18.83~19.44~21.32~22.64~23.9','2010-11-01','185.35~163.6~129.95~104.0~84.35~54.0~54.0~46.475~42.95~36.445','0.55~2~2~4.2~5.5~9.35~32.8725~52.1~80.2725~99.55~124.1','486.0~487.6~477.35~474.75~478.5~471.5~494.85~510.725~530.55~547.445','-291.65~-315.0~-338.4~-361.75~-385.15~-408.5~-431.85~-436.47~-438.428~-449.05~-445.32'),(20,42,'DABUR.NS','DABUR.NS','0.068120558686596','11.25~9.7~7.9~6.25~4.65~3.5~2.15~1.95~1.6~1.05~0.5','0.45~1.2~1.3~2.05~2.5~3.15~4.85~6.45~8.35~10.05~11.9','91.25~92.95~94.65~96.35~98.05~99.75~101.45~103.15~104.85~106.55~108.25',214.4,'109.40~108.00~105.85~104.25~104.35~103.40~102.40~103.00~103.10~104.65~104.50~104.80~105.50~101.00~100.95','106.30~105.80~99.20~103.20~102.85~101.50~101.30~100.50~102.30~103.70~103.50~101.50~104.05~98.55~99.50','6.01~6.66~6.97~8.6~9.8~10.32~12.84~13.25~14.72~19.94~24.05','2010-11-01','132.683~130.881~129.079~127.277~125.475~123.673~121.871~120.069~118.267~116.465','9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0','-97.412~-98.012~-99.663~-100.997~-104.5~-105.65~-103.95~-102.25~-100.55~-98.85','132.15~130.45~128.75~127.05~125.35~123.65~121.95~120.25~118.55~116.85~115.15'),(21,43,'DCHL.NS','DCHL.NS','0.086490623804057','17.75~15.05~12.05~9.4~6.85~4.55~3.35~1.9~1.9~1.05~0.35','0.45~1.3~1.55~2.15~3.05~4.35~7.05~9.7~12.55~15.75~18.45','115.8~118.6~121.45~124.25~127.05~129.85~132.65~135.45~138.25~141.1~143.9',137.55,'138.45~137.50~133.70~130.55~131.45~129.25~127.80~127.25~126.10~127.90~127.00~132.90~136.30~131.50~132.50','136.25~133.50~132.00~129.55~130.25~126.00~125.70~125.00~125.00~126.35~126.20~129.20~132.20~129.30~130.15','6.78~7.65~12.6~13.45~14~15.05~15.18~16.42~20.42~21.03~23.67','2010-11-01','26.3575~23.345~21.8~18.045~14.2225~10.1725~10.685~8.565~7.35~5.35~3.2','0.05~0.35~0.35~0.6~0.6~0.85~1.15~1.45~5.715~6.5~8.65','4.6075~4.395~5.7~4.745~3.7225~2.4725~5.785~6.465~8.05~8.9~9.55','21.8~19.3~16.45~13.9~11.1~8.55~6.05~3.55~5.015~2.95~2.3'),(22,44,'DENABANK.NS','DENABANK.NS','0.23349467570184','43.35~35.6~27.8~20.15~12.45~6.5~4.25~3.4~2.1~1.1~0.35','0.45~1.25~1.9~3~4.55~7.5~13.1~21.25~29.55~37.7~45.9','96.8~104.8~112.75~120.75~128.7~136.7~144.7~152.65~160.65~168.6~176.6',59.5,'117.35~116.75~116.65~122.55~119.90~119.50~117.80~116.35~117.70~123.00~126.00~127.65~144.20~139.00~140.75','116.20~115.35~115.20~119.70~118.40~116.20~116.60~114.05~115.65~120.50~124.40~125.10~136.70~135.10~137.10','7.91~9.87~10.65~13.38~13.81~19.32~19.34~19.63~20.02~23.93~24.88','2010-11-01','29.543~20.903~16.552~10.232~8.602~5.0~5.0~5.0~5.0~5.0','0.3~0.5~0.7~0.9~1.35~2.25~6.3~15.95~24.55~33.205~41.155','66.843~66.203~69.802~71.482~77.802~82.2~90.2~98.15~106.15~114.1','-32.3~-40.3~-48.25~-56.25~-64.2~-67.9~-70.1~-63.35~-61.25~-57.745~-58.245'),(23,45,'DISHTV.NS','DISHTV.NS','0.11878574571051','9.9~8.2~6.65~5~3.3~2.3~2.1~1.65~1.4~1.15~0.75','0.45~1.05~1.5~1.7~1.85~2.5~3.4~5.2~7~8.65~10.45','47.75~49.45~51.1~52.75~54.45~56.1~57.75~59.45~61.1~62.75~64.45',54.75,'55.35~56.85~56.05~56.15~58.55~56.70~55.85~54.50~54.35~56.15~55.80~60.20~59.95~57.35~57.30','54.85~55.40~55.55~55.60~57.50~55.05~54.90~53.45~54.00~54.75~55.40~57.70~57.20~56.40~56.50','5.75~6.16~8.13~9.15~11.09~11.82~14.41~15.18~16.43~24.29~24.68','2010-11-01','11.398~9.797~8.078~6.538~5.0~5.0~5.0~5.0~5.0~5.0','5.0~5.0~5.0~5.0~5.0~4.0875~5.0~5.0~4.25~5.75~7.65','4.398~4.497~4.428~4.538~4.7~6.35~8.0~9.7~11.35~13.0','12.0~10.3~8.65~7.0~5.3~3.65~2.0~0.3~-1.35~-1.4~-1.1'),(24,46,'DIVISLAB.NS','DIVISLAB.NS','0.062196103123797','72.8~62.9~52.9~42.6~30.75~11.7~7.2~4.3~3~1.05~0.35','0.65~1.55~3~5.1~9.2~12.1~32.7~43.2~55.15~64.65~76','638.5~649.25~660~670.75~681.55~692.3~703.05~713.85~724.6~735.35~746.1',1761,'723.65~734.00~730.95~723.60~736.90~718.80~711.00~706.00~708.50~717.10~715.90~725.90~719.90~703.90~707.00','716.05~723.00~724.10~718.75~727.05~709.15~697.10~696.15~701.50~710.10~707.25~718.00~715.00~692.45~695.00','10.39~11.59~11.98~13.21~13.6~14.47~16.27~19.98~20.98~21.21~24.44','2010-11-01','1207.46~1196.065~1184.67~1173.275~1161.827~1150.432~1139.037~1127.589~1116.194~1104.799','14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0','-715.21~-727.12~-771.07~-797.787~-854.75~-960.153~-959.24~-968.055~-970.1~-984.05','1136.5~1125.75~1115.0~1104.25~1093.45~1082.7~1071.95~1061.15~1050.4~1039.65~1028.9'),(25,47,'DLF.NS','DLF.NS','0.097134728713676','52.3~43.75~36.1~27.35~19.15~11.3~6.7~3.4~2.05~1~0.25','0.65~1.2~2~3.25~4.8~10.75~19.5~28.9~37.75~46.05~54.7','308.1~316.65~325.15~333.65~342.2~350.7~359.2~367.75~376.25~384.75~393.3',114.75,'391.00~383.00~379.40~391.00~387.00~380.00~381.35~370.75~367.35~374.00~366.80~368.45~372.00~356.00~358.90','384.00~377.10~375.25~384.10~381.10~370.25~376.60~365.70~363.45~367.05~364.00~365.00~368.60~351.15~354.05','6.62~7.41~11.69~13.27~16.44~17.67~18.16~22.6~23.24~24.36~24.86','2010-11-01','9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0','0.05~0.05~0.05~0.05~0.05~0.35~0.6~9.1~18.05~24.75~32.6','202.35~210.9~219.4~227.9~236.45~244.95~253.45~262.0~270.5~279.0','-184.35~-191.35~-193.7~-192.5~-196.15~-167.5~-128.9~-106.35~-85.2~-122.9~-116.1'),(26,48,'DRREDDY.NS','DRREDDY.NS','0.054460381989722','159.1~136.75~117.7~94.95~73.7~32.3~12.7~7.1~2.1~1~0.3','0.4~1.35~2.4~7.3~13.15~18.4~71.75~96.15~119.45~145.25~165.35','1546.6~1569.2~1591.8~1614.4~1637~1659.6~1682.2~1704.8~1727.4~1750~1772.6',2962,'1580.65~1569.50~1581.90~1644.65~1604.00~1607.10~1633.35~1629.75~1619.00~1630.00~1649.70~1651.00~1660.00~1674.00~1679.90','1567.15~1550.00~1567.00~1587.10~1588.00~1585.25~1620.00~1615.20~1591.00~1616.15~1630.10~1636.00~1646.15~1650.90~1667.00','5.01~5.64~6.76~11.2~11.92~15.24~17.37~18.66~18.9~22.24~22.25','2010-11-01','1529.944~1505.988~1482.032~1458.076~1434.12~1410.164~1386.208~1362.252~1338.296~1314.34','14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0','-1066.287~-1070.838~-1066.537~-1021.987~-1053.387~-1117.95~-1156.698~-1143.805~-1096.805~-1107.505','1429.4~1406.8~1384.2~1361.6~1339.0~1316.4~1293.8~1271.2~1248.6~1226.0~1203.4'),(27,49,'EDUCOMP.NS','EDUCOMP.NS','0.13202603330234','105.85~88.55~70.25~52.5~35.05~16.45~8.35~4.55~2.4~1.6~0.5','0.85~1.15~3.25~3.5~6.95~13.4~36.2~55.2~74~92.15~112.1','459.6~477.8~495.95~514.1~532.3~550.45~568.6~586.8~604.95~623.1~641.3',29.55,'637.00~631.80~639.90~640.70~630.60~625.90~627.80~618.45~617.80~620.80~620.00~611.50~597.65~563.60~559.80','628.70~625.50~635.05~635.00~626.00~615.20~620.05~610.10~612.05~615.00~614.50~606.15~589.00~552.50~553.25','10.1~10.83~11.45~13.14~13.2~13.51~18.02~18.14~20.76~22.82~23.72','2010-11-01','256.512~221.232~179.983~141.782~94.725~46.828~38.511~20.726~14.285~5.0','0.9~1.1~3.2~2.5~5.1~10.2~38.8925~51.895~67.795~82.845~100.345','686.562~669.482~646.383~626.332~597.475~567.728~577.561~577.976~589.685~598.55','-425.05~-443.25~-461.4~-479.55~-497.75~-515.9~-534.05~-552.25~-570.4~-588.55~-606.75'),(28,50,'EKC.NS','EKC.NS','0.087861271676301','17.3~14.5~11.95~9.2~6.55~4.3~3.4~2.55~1.7~1.2~0.25','0.4~1.05~1.4~2.15~2.95~4.2~6.6~9.7~12.4~15.35~18.1','112.1~114.9~117.65~120.4~123.2~125.95~128.7~131.5~134.25~137~139.8',14.55,'126.30~124.35~130.50~129.40~132.60~130.70~130.00~126.90~128.35~128.60~129.10~135.45~132.75~131.20~128.25','124.65~121.65~123.45~127.50~128.70~126.60~128.10~124.05~127.00~127.60~127.55~133.00~129.00~128.00~126.50','5.11~6.3~7.23~10.62~14.2~15.08~15.71~15.94~19.67~20.94~23.61','2010-11-01','33.883~31.633~26.913~26.288~23.582~15.468~12.598~9.817~9.817~8.4','0.05~0.5~0.25~0.65~0.4~1.3~2.95~6.65~8.85~11.7~14.3','131.433~131.983~130.013~132.138~132.232~126.868~126.748~126.767~129.517~130.85','-92.55~-95.35~-98.1~-100.85~-103.65~-106.4~-109.15~-111.95~-114.7~-117.45~-120.25'),(29,51,'ESSAROIL.NS','ESSAROIL.NS','0.098901098901099','22.4~18.75~15.15~11.55~8.25~4.85~3.15~2.8~1.85~1.25~0.95','0.6~1.2~1.55~2.45~3.4~4.1~8.25~12.25~15.85~19.7~23.45','129~132.65~136.3~139.9~143.55~147.2~150.85~154.5~158.1~161.75~165.4',119.4,'143.85~142.05~141.70~143.50~139.50~142.45~146.35~141.50~140.90~143.90~152.80~150.75~146.90~146.30~150.00','140.55~140.00~139.00~140.70~138.40~139.50~144.25~139.30~139.10~142.00~149.05~147.40~143.65~139.40~148.10','6.11~6.12~6.9~9.51~11.07~12.91~15.96~17.8~19.01~19.41~19.52','2010-11-01','103.442~88.142~79.043~78.293~64.942~46.06~36.093~36.093~36.093~30.877','0.55~0.3~0.25~0.6~0.5~0.65~0.95~2.8~6.85~10.7~14.25','113.042~101.392~95.943~98.793~89.092~73.86~67.543~71.193~74.793~73.227','-0.6~-4.25~-7.9~-11.5~-15.15~-18.8~-22.45~-26.1~-29.7~-33.35~-37.0'),(30,53,'FINANTECH.NS','FINANTECH.NS','0.13932584269663','197.85~163.4~130.65~98.15~64.2~21.75~8.25~6.55~3.8~1~0.85','0.3~1.55~2.1~7.95~14~30.6~66.75~101.55~138.85~173.85~208.1','811.25~845.5~879.7~913.9~948.15~982.35~1016.55~1050.8~1085~1119.2~1153.45',225.5,'1187.00~1184.00~1172.95~1195.45~1170.00~1167.90~1190.00~1170.00~1154.80~1159.90~1155.00~1149.90~1128.00~1066.00~1018.00','1178.00~1171.00~1160.05~1167.00~1157.25~1156.10~1175.00~1151.00~1146.25~1152.00~1149.10~1128.00~1094.90~1035.00~987.00','5.22~5.78~6.43~10.69~11.56~14.62~18.67~20.23~21.4~22.67~24.83','2010-11-01','450.85~405.796~334.6~334.6~248.787~118.85~49.25~41.45~24.65~9.0','0.95~3.3~4.45~14.65~23.65~41.45~57.575~88.3~124.975~157.85~190.95','1036.6~1025.796~988.8~1023.0~971.437~875.7~840.3~866.75~884.15~902.7','-576.75~-611.0~-645.2~-679.4~-713.65~-747.85~-782.05~-816.3~-850.5~-884.7~-918.95'),(31,54,'FSL.NS','FSL.NS','0.094903339191564','4.8~3.55~2.75~2.4~1.7~1.5~1.3~1.3~1.15~1.05~0.35','0.95~1.05~1.1~1.25~1.3~1.45~1.6~2.6~3.55~4.55~5.8','23.95~24.6~25.25~25.9~26.55~27.2~27.85~28.5~29.15~29.8~30.45',22.85,'29.35~30.50~30.35~30.15~29.80~28.50~28.90~28.00~27.90~28.25~28.00~28.10~29.75~28.10~28.00','29.05~29.75~29.80~29.85~29.40~27.80~28.35~27.50~27.25~27.75~27.80~27.70~28.90~27.10~27.35','5.07~5.55~10.62~16.23~19.63~22.08~22.24~23.4~24.13~24.38~24.81','2010-11-01','5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0','2.2425~2.8925~3.5425~4.1925~4.8425~5.0~5.0~1.3~1.86~2.41~3.06','6.1~6.75~7.4~8.05~8.7~9.35~10.0~10.65~11.3~11.95','3.9~3.25~2.6~1.95~1.3~0.65~0.0~-0.65~-1.3~-1.95~-2.6'),(32,55,'GAIL.NS','GAIL.NS','0.051507726158924','45.3~39.8~33.55~26.75~21.2~8.65~6.15~3.55~2.15~1.65~0.6','0.75~1.65~2.2~2.9~7.65~12.5~21.15~28.1~35.05~40.5~48.05','459.9~466.25~472.55~478.9~485.2~491.55~497.9~504.2~510.55~516.85~523.2',443.35,'495.35~497.00~502.95~512.90~505.70~500.95~505.00~512.80~503.95~506.95~507.50~506.65~497.50~496.85~496.15','489.25~492.55~496.50~504.90~492.00~490.05~498.10~505.35~495.70~500.05~502.30~498.40~494.00~487.05~492.00','6.64~10.05~10.61~12~12.77~13.35~15.74~16.66~16.88~17.53~22.95','2010-11-01','123.563~123.563~123.163~109.063~91.613~46.0~46.0~46.0~36.99~20.138','0.4~0.35~1.3~0.85~3~6.05~10.8~14.95~21~24.8~35.95','140.113~146.463~152.363~144.613~133.463~94.2~100.55~106.85~104.19~93.638','-7.55~-13.9~-20.2~-26.55~-32.85~-39.2~-45.55~-51.85~-58.2~-64.5~-70.85'),(33,56,'GESHIP.NS','GESHIP.NS','0.11524163568773','54.45~45.45~36.55~27.95~19.25~10.1~5.8~3.1~2.05~1.5~0.95','0.9~1.5~2.05~3.85~4.5~7.45~19.25~29.3~38.6~47.7~57.25','271.8~280.95~290.1~299.25~308.4~317.55~326.7~335.85~345~354.15~363.3',408.85,'330.90~328.50~335.50~333.00~330.60~319.00~324.75~321.90~325.00~330.00~330.90~341.40~333.70~328.45~328.30','325.55~323.00~329.05~329.85~327.00~304.20~314.35~313.30~321.20~324.00~328.00~336.15~328.55~316.35~321.00','9.06~11.91~13.08~15.76~16.87~17.28~18.14~19.29~20.52~21.38~22.51','2010-11-01','216.635~216.635~193.935~173.835~131.03~100.8665~91.1675~81.4685~71.7695~62.0705','9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0','79.585~88.735~75.185~64.235~30.58~-13.57~-33.97~-31.947~-37.428~-36.8','146.05~136.9~127.75~118.6~109.45~100.3~91.15~82.0~72.85~63.7~54.55'),(34,58,'GMRINFRA.NS','GMRINFRA.NS','0.10830324909747','8.75~7.25~5.85~4.45~3.05~2.3~1.75~1.5~1.3~1.15~0.55','0.45~1.2~1.4~1.75~1.8~2.5~3.2~4.65~6.15~7.65~9.25','46.25~47.7~49.15~50.6~52.05~53.5~54.95~56.4~57.85~59.3~60.75',23.2,'59.40~59.25~59.00~59.10~58.40~58.00~55.55~54.45~54.40~54.70~54.20~53.65~52.90~54.25~54.10','59.00~58.50~58.10~58.15~57.05~55.60~55.05~53.50~53.85~53.85~53.75~53.15~52.40~53.60~53.75','5.23~6.78~8.18~8.88~10.24~16.76~18.58~18.93~19.42~22.68~24.16','2010-11-01','9.945~8.495~7.045~5.0~5.0~5.0~5.0~5.0~5.0~5.0','0.15~0.3~0.3~0.3~0.3~0.55~1.3~3.044~4.4~5.8~7.405','32.995~32.995~32.995~32.4~33.85~35.3~36.75~38.2~39.65~41.1','-18.05~-19.5~-20.95~-22.4~-23.85~-25.3~-26.75~-28.2~-29.65~-31.1~-30.145'),(35,59,'GRASIM.NS','GRASIM.NS','0.057898836099562','225.55~193.25~160.5~132~97.85~33.8~14.15~5.65~3.55~1.05~0.7','0.5~1.7~4.45~5.75~14.55~30.75~102.15~132.7~168.4~199.65~233.45','2078.35~2110.8~2143.2~2175.65~2208.05~2240.5~2272.95~2305.35~2337.8~2370.2~2402.65',3435.45,'2385.00~2354.3999~2355.00~2342.00~2344.95~2319.80~2294.95~2270.00~2248.00~2260.00~2288.00~2282.6499~2272.00~2259.80~2272.00','2350.00~2340.25~2326.00~2315.00~2325.00~2276.00~2269.6001~2221.20~2232.05~2236.05~2260.00~2246.75~2241.1001~2213.00~2245.00','6.66~6.87~12.6~13.38~13.8~15.43~15.61~15.68~18.1~20.02~24.72','2010-11-01','1472.8805~1438.4835~1404.1395~1369.7425~1335.3985~1301.0015~1266.6045~1232.2605~1197.8635~1163.5195','14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0','-695.59~-730.59~-591.14~-642.59~-726.04~-895.635~-961.745~-996.245~-964.458~-894.15','1371.1~1338.65~1306.25~1273.8~1241.4~1208.95~1176.5~1144.1~1111.65~1079.25~1046.8'),(36,60,'GSPL.NS','GSPL.NS','0.1441743503772','23.3~19.45~15.35~11.45~7.6~3.9~2.95~2.5~1.8~1.3~0.95','0.7~1.35~1.9~2.6~3.4~5.5~7.9~12~16.1~20.35~24.7','92.25~96.3~100.4~104.45~108.5~112.55~116.6~120.65~124.7~128.8~132.85',89.15,'118.40~116.60~120.60~120.50~119.25~118.60~119.95~124.25~127.25~125.10~124.95~127.90~122.90~113.85~114.70','116.30~115.40~118.00~118.40~117.80~115.80~117.25~120.35~125.50~122.10~123.50~123.20~120.20~110.70~112.70','6.67~10.51~10.92~12.92~17.06~17.12~18.3~19.68~20.56~22.52~22.77','2010-11-01','75.112~75.112~70.563~60.712~51.163~28.082~24.48~23.995~19.067~15.935','5.0~0.2~0.25~0.3~0.4~0.55~2.85~7.05~11.15~15.55~19.3','78.212~82.262~81.813~76.012~70.513~51.482~51.93~55.495~54.617~55.585','1.9~-2.15~-6.25~-10.3~-14.35~-18.4~-22.45~-26.5~-30.55~-34.65~-38.7'),(37,62,'GTLINFRA.NS','GTLINFRA.NS','0.042269187986652','3.6~3.2~2.65~2.3~1.75~1.45~1.35~1.25~1.15~1.05~0.7','0.45~1.05~1.1~1.35~1.4~1.6~1.85~2.3~2.75~3.3~3.75','42.4~42.85~43.35~43.8~44.3~44.75~45.2~45.7~46.15~46.65~47.1',2.75,'45.40~44.90~45.20~45.00~44.40~44.45~44.60~44.85~44.55~45.90~45.00~44.95~44.60~45.60~45.30','44.65~44.55~44.70~44.60~44.15~44.00~44.15~44.00~44.10~44.70~44.25~44.55~44.10~44.85~44.50','5.18~12.53~13.17~16.06~18.66~19.06~22.05~22.36~22.6~22.95~24.65','2010-11-01','10.933~10.933~10.933~10.933~10.933~10.933~10.933~10.933~10.933~10.933','0.05~0.6~0.65~0.7~0.75~0.95~1.6~2.05~2.5~2.4~3.3','50.583~51.033~51.533~51.983~52.483~52.933~53.383~53.883~54.333~54.833','-34.65~-35.1~-35.6~-36.05~-36.55~-37.0~-37.45~-37.95~-38.4~-38.9~-39.35'),(38,64,'GVKPIL.NS','GVKPIL.NS','0.068965517241379','4.9~4.15~3.4~2.7~2~1.6~1.45~1.4~1.15~1.05~0.6','0.75~1.1~1.15~1.4~1.5~1.65~2.05~2.75~3.6~4.3~5.05','38.9~39.6~40.35~41.1~41.8~42.55~43.3~44~44.75~45.5~46.2',10.15,'46.80~46.05~46.40~46.90~45.00~44.00~43.25~42.75~42.80~44.80~44.95~43.65~43.90~42.80~43.50','46.05~45.60~46.05~46.15~44.10~43.00~42.65~42.30~42.35~43.50~44.15~43.20~43.10~42.00~42.45','5.7~7.21~8.92~9.44~12.26~13.9~13.98~14.67~15.55~20.15~21.85','2010-11-01','11.733~11.383~10.732~9.133~8.282~7.133~6.033~6.033~6.033~6.033','0.1~0.1~0.1~0.1~0.1~0.1~0.1~0.8075~1.1325~1.8775~2.5695','40.483~40.833~40.932~40.083~39.932~39.533~39.183~39.883~40.633~41.383','-23.75~-24.45~-25.2~-25.95~-26.65~-27.4~-28.15~-28.85~-29.6~-30.35~-31.05'),(39,65,'HCC.NS','HCC.NS','0.09098164405427','8.65~7.2~5.95~4.6~3.25~2.55~1.9~1.7~1.3~1.05~0.8','0.55~1.2~1.25~1.55~1.9~2.4~3.35~4.8~6.2~7.7~9.1','54.15~55.55~56.95~58.3~59.7~61.1~62.5~63.9~65.25~66.65~68.05',31.4,'67.70~68.00~67.30~67.80~65.50~64.35~65.25~64.25~64.10~64.70~63.90~63.90~63.60~62.80~62.50','66.00~66.50~66.00~66.00~62.90~62.80~64.25~63.15~59.80~63.90~63.30~63.40~63.00~61.70~61.60','5.33~6.14~6.52~6.61~10.27~13.1~13.57~18.22~19.03~22.73~24.28','2010-11-01','32.623~30.223~26.373~20.873~20.873~19.523~19.523~19.523~19.523~19.4','0.9~1.9~1.65~1.9~1.65~2.25~2.95~4.8115~5.85~7.71~9.11','55.373~54.373~51.923~47.773~49.173~49.223~50.623~52.023~53.373~54.65','-17.75~-19.15~-20.55~-21.9~-23.3~-24.7~-26.1~-27.5~-28.85~-30.25~-31.65'),(40,66,'HCLTECH.NS','HCLTECH.NS','0.11071849234393','67.2~56.05~45.45~34.1~23.35~12.95~7.65~4~1.85~1.4~0.45','0.25~1.6~2.35~3.3~6.7~9~24.6~35.4~47.1~58.95~70.25','347.9~359.1~370.25~381.45~392.6~403.8~415~426.15~437.35~448.5~459.7',1740.65,'436.70~434.30~431.40~455.50~447.60~433.00~448.00~444.75~428.80~425.85~421.00~425.85~423.50~409.00~407.55','430.00~428.25~426.20~445.50~438.00~425.15~436.05~433.30~425.10~419.00~418.25~420.15~420.10~401.00~403.80','5.6~6.48~11.44~11.44~15.75~17.18~19.41~22.61~23.66~24.21~24.52','2010-11-01','1493.7215~1481.8495~1470.0305~1458.1585~1446.3395~1434.4675~1422.5955~1410.7765~1398.9045~1387.0855','14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0','-1178.58~-1186.73~-1121.16~-1173.68~-1184.68~-1236.13~-1250.778~-1271.25~-1282.85~-1275.9','1406.75~1395.55~1384.4~1373.2~1362.05~1350.85~1339.65~1328.5~1317.3~1306.15~1294.95'),(41,69,'HDIL.NS','HDIL.NS','0.10152859030006','38.4~32.05~26.2~19.9~13.75~8.4~4.9~3.05~1.7~1.25~1','0.4~1.5~1.55~3.05~5.3~6.2~14.5~20.75~27.05~33.9~40.1','216.1~222.35~228.65~234.95~241.2~247.5~253.8~260.05~266.35~272.65~278.9',78.6,'279.35~273.95~271.95~275.70~278.40~272.90~275.55~270.50~271.80~277.55~269.65~270.90~267.10~254.75~254.40','275.25~270.65~269.10~271.50~273.05~268.05~272.00~268.00~269.80~270.10~267.50~268.55~263.00~251.50~249.00','5.89~7.21~8.53~11.05~11.32~12.28~12.28~13.16~14.09~16.84~24.19','2010-11-01','148.3~144.15~134.6~126.75~92.75~63.75~36.95~24.25~14.35~12.7','0.05~0.05~0.05~0.1~0.1~0.1~0.2~7.1~13.4~19.65~26.3','285.8~287.9~284.65~283.1~255.35~232.65~212.15~205.7~202.1~206.75','-132.5~-138.75~-145.05~-151.35~-157.6~-163.9~-170.2~-176.45~-182.75~-189.05~-195.3'),(42,70,'HEROHONDA.NS','HEROHONDA.NS','0.0684082951791','212.45~179.95~149.55~119.45~86.55~39.45~17.5~7.5~2.15~1.75~0.25','0.4~1.95~2.5~8.35~17.5~37.2~91.4~121.35~154.6~189.35~222.65','1705.75~1737.65~1769.55~1801.45~1833.35~1865.25~1897.15~1929.05~1960.95~1992.85~2024.75',1838.35,'1864.90~1884.95~1885.00~1893.90~1875.00~1815.00~1820.00~1848.95~1851.80~1871.00~1870.00~1878.85~1867.00~1920.00~1850.00','1845.00~1858.35~1861.00~1881.15~1843.50~1793.00~1800.25~1830.00~1840.50~1841.00~1843.05~1855.00~1857.00~1845.55~1815.55','7.23~14.34~14.34~15.52~15.86~15.96~17.73~18.44~18.64~21.13~21.81','2010-11-01','189.45~190.25~138.3~93.35~46.35~76.61~91.213~83.313~72.266~50.116~30.219','1.1~5.35~6.3~21.6~44.9~89.3~108.03~125.545~146.445~161.395~230.13','56.85~89.55~69.5~56.45~41.35~103.51~150.013~174.013~194.866~204.616~216.619','133.7~106.05~75.1~58.5~49.9~62.4~49.23~34.845~23.845~6.895~43.73'),(43,71,'HINDALCO.NS','HINDALCO.NS','0.095909732016925','31.25~26.15~21.1~16.5~11.4~5.5~3.75~2.35~1.7~1.45~0.7','0.9~1.3~2.25~2.35~4.15~5.55~11.9~17~22.2~27.45~32.5','185.25~190.3~195.35~200.4~205.45~210.5~215.55~220.6~225.65~230.7~235.75',149.6,'219.00~215.75~213.45~219.05~217.95~222.90~213.20~206.30~206.00~214.40~215.95~222.00~216.85~217.25~216.15','215.00~210.55~212.00~214.50~214.95~209.70~209.65~202.50~203.75~211.10~213.25~217.20~214.40~211.10~212.75','5.5~7.34~8.44~8.98~13.32~16.76~18.23~20.91~22.31~23.79~23.89','2010-11-01','75.847~75.147~66.998~55.547~51.547~33.593~24.947~21.7~16.8~14.95','0.7~1~1.35~1.6~2.25~2.65~4.55~9.5~15.55~22.4~26.1','111.497~115.847~112.748~106.347~107.397~94.493~90.897~92.7~92.85~96.05','-26.65~-31.7~-36.75~-41.8~-46.85~-51.9~-56.95~-62.0~-67.05~-72.1~-77.15'),(44,72,'HINDPETRO.NS','HINDPETRO.NS','0.028248587570621','30.95~27.3~24.9~20.7~18.25~7~6.45~3.65~2.4~1.5~0.9','0.55~1.25~2.35~4.25~4.45~7.9~18.55~21.65~25.1~28.2~32.55','470.1~473.55~477~480.4~483.85~487.3~490.75~494.2~497.6~501.05~504.5',508.9,'516.65~516.80~518.85~521.00~500.00~499.80~495.00~499.00~495.65~499.95~502.60~502.40~495.50~495.75~495.00','512.90~510.65~512.00~512.65~488.60~490.45~489.55~490.00~491.85~494.60~498.10~499.00~490.10~489.50~489.55','5.27~5.54~8.35~9.07~10.98~12.72~18.51~19.42~20.81~24.07~24.47','2010-11-01','135.615~125.865~125.865~125.865~125.865~77.477~77.477~55.35~38.15~27.2','14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0','96.815~90.515~93.965~97.365~100.815~55.877~59.327~40.65~26.85~19.35','52.8~49.35~45.9~42.5~39.05~35.6~32.15~28.7~25.3~21.85~18.4'),(45,75,'HOTELEELA.NS','HOTELEELA.NS','0.10980760206476','8.5~7.1~5.75~4.4~2.95~2.25~1.95~1.6~1.35~1.2~0.45','0.85~1.05~1.35~1.55~1.8~2.2~3.05~4.5~6.05~7.45~9','44.5~45.95~47.35~48.75~50.2~51.6~53~54.45~55.85~57.25~58.7',21.05,'58.30~58.30~57.30~57.65~56.20~54.40~54.90~53.60~53.50~54.00~53.50~54.40~54.40~52.90~52.50','57.15~56.80~56.30~56.50~55.55~53.60~53.55~50.35~52.70~53.30~53.05~53.10~53.65~52.05~50.65','6.22~6.85~6.91~10.67~14.72~15.63~17.13~20.28~21.11~23.14~24.52','2010-11-01','23.407~20.907~17.308~17.308~15.407~11.907~11.907~11.907~11.907~11.907','0.05~0.05~0.05~0.05~0.05~0.05~0.85~2.25~3.7~5.05~6.5','46.857~45.807~43.608~45.008~44.557~42.457~43.857~45.307~46.707~48.107','-18.45~-19.9~-21.3~-22.7~-24.15~-25.55~-26.95~-28.4~-29.8~-31.2~-32.65'),(46,76,'IBREALEST.NS','IBREALEST.NS','0.13705457263892','38.05~31.5~25.4~18.8~12.6~7.3~4.25~3.25~2.2~1.45~0.7','0.75~1.3~1.6~2.35~3.6~5.85~12.9~19.65~26.55~33.45~39.8','158.6~165.15~171.75~178.3~184.85~191.4~197.95~204.5~211.05~217.65~224.2',66.95,'193.05~189.30~191.65~196.40~201.90~199.10~210.35~210.45~212.00~214.40~212.80~211.40~204.50~197.10~197.00','190.75~186.50~186.50~192.20~194.30~194.25~206.00~186.90~207.15~209.15~208.65~208.50~201.50~193.50~193.50','7.53~7.98~8.41~11.79~12.56~12.66~15.89~20.59~23.09~23.1~23.2','2010-11-01','60.32~52.72~45.003~38.67~30.152~21.552~14.002~14.002~14.002~14.002','0.1~0.1~0.1~0.15~0.15~0.2~0.35~5.1~11.45~17.8~24.4','151.97~150.92~149.803~150.02~148.052~146.002~145.002~151.552~158.102~164.702','-86.65~-93.2~-99.8~-106.35~-112.9~-119.45~-126.0~-132.55~-139.1~-141.29~-142.25'),(47,77,'ICICIBANK.NS','ICICIBANK.NS','0.068284804163489','132.05~112.7~94.55~74.9~55.7~22.95~8.35~3.85~3.4~1.9~0.75','0.35~1.3~3.15~4.7~12.65~15.65~56.85~76.25~95.75~117.3~138.8','1063.75~1083.6~1103.45~1123.3~1143.15~1163~1182.85~1202.7~1222.55~1242.4~1262.25',1479.1,'1145.95~1143.25~1144.95~1169.20~1143.00~1124.00~1126.90~1121.50~1121.00~1139.00~1152.50~1143.00~1129.30~1098.30~1224.00','1130.00~1133.00~1136.20~1152.20~1120.25~1107.25~1115.30~1108.45~1113.00~1133.00~1135.00~1131.80~1116.40~1076.40~1186.00','6.81~12.68~15.34~15.98~17.54~19.66~19.77~20.24~20.52~21.15~24.75','2010-11-01','472.25~472.25~472.25~472.25~413.8~349.857~328.816~307.775~286.734~265.693','14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0','56.9~76.75~96.6~116.45~77.85~-63.9~-155.502~-207.6~-202.8~-202.95','429.35~409.5~389.65~369.8~349.95~330.1~310.25~290.4~270.55~250.7~230.85'),(48,78,'ICSA.NS','ICSA.NS','0.078454106280193','16.4~13.85~11.3~8.85~6.35~3.7~2.7~1.95~1.55~1.3~0.5','0.6~1.25~1.7~2.45~3~3.55~6.55~9.3~11.9~14.6~17.15','116.95~119.5~122~124.55~127.1~129.65~132.2~134.75~137.3~139.8~142.35',4.2,'133.45~131.00~131.20~131.95~132.20~129.45~129.90~131.00~130.80~131.40~130.70~132.00~134.45~132.95~131.90','131.40~129.15~130.00~130.40~129.20~124.30~127.35~128.10~127.10~129.00~128.50~130.05~131.65~129.40~130.60','7.28~8.74~9.88~12.36~12.99~13.11~14.74~18.54~19.41~22.04~24.29','2010-11-01','16.135~13.515~11.375~8.885~7.705~6.745~5.0~5.0~5.0~5.0','0.3~0.55~0.55~0.8~0.8~0.8~1.25~4.25~6.85~9.6~12.15','128.885~128.815~129.175~129.235~130.605~132.195~133.0~135.55~138.1~140.6','-107.75~-110.3~-112.8~-115.35~-117.9~-120.45~-123.0~-122.2~-122.05~-121.45~-121.55'),(49,79,'IDBI.NS','IDBI.NS','0.1896800230614','47.55~39.3~31~22.7~14.2~7.4~5.7~3.65~2~1.45~0.6','0.4~1.15~2.45~3.1~5.15~8.1~14.95~23.55~32.65~41.65~50.1','137.8~146.35~154.9~163.45~172.05~180.6~189.15~197.75~206.3~214.85~223.4',62.25,'159.40~158.30~157.00~164.50~161.50~162.00~160.40~160.15~169.80~170.45~172.70~170.75~173.10~189.90~184.30','155.30~156.50~155.75~161.55~160.00~157.00~158.50~157.80~166.50~167.85~170.85~169.15~170.50~183.20~181.00','7.66~9.51~9.61~9.78~9.85~9.95~12.88~15.24~15.54~21.42~23.81','2010-11-01','8.233~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0','0.6~0.85~2.2~2.65~4.4~7.45~13.75~23.45~33.6025~42.0025~50.5525','83.783~89.1~97.65~106.2~114.8~123.35~131.9~140.5~149.05~157.6','-70.55~-79.1~-86.6~-93.75~-98.7~-102.75~-98.25~-88.75~-78.698~-70.498~-71.747'),(50,80,'IDEA.NS','IDEA.NS','0.11975618501255','12~10~7.95~6.05~4.05~3.2~2.15~1.85~1.25~1.05~0.55','0.45~1.25~1.45~1.7~2.4~3.05~4.25~6.4~8.45~10.45~12.6','57.35~59.35~61.4~63.4~65.45~67.45~69.45~71.5~73.5~75.55~77.55',155.1,'71.95~72.85~73.50~74.30~73.85~72.40~73.60~72.25~71.30~73.10~73.90~72.00~69.50~66.60~68.40','70.90~71.00~72.00~73.05~72.75~70.00~72.05~71.25~70.25~72.40~72.55~69.75~68.40~65.55~67.75','6.24~8.66~9.41~11.82~12.19~12.41~17.13~18.2~18.22~19.32~22.72','2010-11-01','105.166~103.046~100.873~98.753~96.58~94.46~92.34~90.167~88.047~85.874','9.0~9.0~9.0~9.0~9.0~9.0~9.0~10.1~12.8~15.85~16.8','-88.75~-86.75~-84.7~-82.7~-80.65~-78.65~-76.65~-60.74~-55.74~-53.48','106.75~104.75~102.7~100.7~98.65~96.65~94.65~93.7~94.4~95.4~94.35'),(51,81,'IDFC.NS','IDFC.NS','0.064690689952035','22~18.65~15.65~12.45~9.4~6.05~3.25~2.45~1.7~1.05~0.3','0.4~1.3~2.05~2.4~3.8~5.95~9.55~12.75~15.9~19.5~22.95','183.85~187.1~190.35~193.6~196.8~200.05~203.3~206.5~209.75~213~216.25',140.55,'209.95~208.70~207.00~213.60~209.85~204.80~208.85~202.25~202.50~204.10~204.00~209.20~209.50~203.45~202.95','207.10~206.75~205.15~209.55~206.75~196.70~204.05~199.10~199.70~201.90~201.20~206.20~207.25~201.00~201.00','5.1~9.76~14.73~15.47~15.99~20.41~20.46~21.09~22.01~23.16~23.63','2010-11-01','49.163~49.163~49.163~49.163~46.102~37.153~25.3~21.877~15.3~9.0','0.05~0.05~0.95~1~1.2~2.25~3.8~7.35~11.15~14.35~17.5','92.463~95.713~98.963~102.213~102.352~96.653~88.05~87.827~84.5~81.45','-34.3~-37.55~-40.8~-44.05~-47.25~-50.5~-53.75~-56.95~-60.2~-63.45~-66.7'),(52,82,'IFCI.NS','IFCI.NS','0.060773480662983','7.15~6.25~5.15~4.2~3.15~2.15~1.8~1.55~1.35~1.2~0.4','0.25~1.15~1.45~1.7~1.9~2.35~3.15~4.3~5.35~6.4~7.55','63.9~64.95~66~67.05~68.1~69.15~70.2~71.25~72.3~73.35~74.4',34.15,'74.85~74.15~75.85~76.70~74.20~73.40~73.35~72.65~72.05~74.60~74.40~74.20~74.00~72.60~71.40','73.50~73.25~74.55~75.40~72.60~72.40~72.70~70.20~71.00~73.70~73.75~73.20~72.80~70.90~69.60','5.1~5.83~8.01~8.4~11.23~15.91~16.16~17.12~17.2~24.05~24.35','2010-11-01','20.892~19.843~19.843~17.442~16.393~15.043~12.692~10.92~10.92~10.92','0.1~0.1~0.1~0.1~0.1~0.1~0.1~0.1~1.1955~1.5~3.054','50.642~50.643~51.693~50.342~50.343~50.043~48.742~48.02~49.07~50.12','-24.75~-25.8~-26.85~-27.9~-28.95~-30.0~-31.05~-32.1~-33.15~-34.2~-35.25'),(53,83,'INDHOTEL.NS','INDHOTEL.NS','0.044444444444444','8.45~7.45~6.2~5.15~4.1~2.6~2.5~1.65~1.55~1~0.3','0.3~1.25~1.55~1.9~2.2~2.95~4.1~5.35~6.5~7.55~8.85','94.55~95.65~96.75~97.9~99~100.1~101.2~102.3~103.45~104.55~105.65',96.35,'103.80~103.75~103.00~104.00~102.80~101.50~101.40~100.45~100.10~100.85~100.35~99.90~103.50~100.30~101.50','102.40~102.30~102.30~102.60~101.90~99.75~100.30~99.20~99.15~99.55~99.55~99.05~101.25~99.00~100.10','5.38~5.59~8.14~10.79~13.52~15.04~15.59~16.36~17.71~19.03~20.37','2010-11-01','28.078~25.027~25.027~25.027~25.027~20.56~20.56~17.13~17.13~17.13','5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.35~6.6~7.45~8.65','26.278~24.327~25.427~26.577~27.677~24.31~25.41~23.08~24.23~25.33','6.8~5.7~4.6~3.45~2.35~1.25~0.15~-0.95~-2.1~-3.2~-4.3'),(54,84,'INDIACEM.NS','INDIACEM.NS','0.097070771862305','16.3~13.85~11.15~8.55~5.95~3.65~3.25~2.15~1.7~1.3~0.65','0.9~1.25~1.5~1.85~2.5~3.6~6.3~8.95~11.55~14.6~17.15','96.75~99.4~102.1~104.75~107.45~110.1~112.75~115.45~118.1~120.8~123.45',108.3,'121.85~119.50~118.60~119.85~118.25~118.80~116.95~114.30~112.95~117.90~122.60~117.55~115.85~113.50~112.50','120.25~117.90~117.55~118.10~117.10~111.60~114.00~112.80~111.25~113.10~113.40~115.35~114.80~111.30~111.10','6.6~8.61~8.66~13.97~16.5~16.91~17.45~18.59~18.86~24.35~24.66','2010-11-01','74.053~74.053~63.352~63.352~58.802~38.502~33.402~32.95~24.9~23.25','9.0~9.0~9.0~9.0~9.0~7.215~9.0~9.0~5.1~7.75~10.45','62.503~65.153~57.152~59.802~57.952~40.302~37.852~40.1~34.7~35.75','20.55~17.9~15.2~12.55~9.85~7.2~4.55~1.85~-0.8~-3.5~-6.15'),(55,85,'INDIAINFO.NS','INDIAINFO.NS','0.067980082268889','13.2~11.2~9.25~7.45~5.45~4.1~2.55~2.1~1.55~1.2~0.85','0.2~1~1.6~2.05~3.15~3.25~5.75~7.7~9.7~11.7~13.85','106.8~108.75~110.75~112.75~114.7~116.7~118.7~120.65~122.65~124.65~126.6',72,'122.75~124.00~120.00~121.40~118.50~116.00~117.00~114.40~114.40~118.00~119.40~118.60~116.25~115.90~118.70','121.50~117.20~118.30~120.05~117.00~114.30~114.55~111.55~112.85~115.55~118.25~116.80~114.70~112.90~117.00','5.56~6.37~9.81~10.49~11.2~12.7~13.09~13.25~13.77~13.81~21.81','2010-11-01','30.317~28.067~28.067~25.168~20.468~17.017~12.017~12.017~12.017~11.65','0.1~0.15~0.1~0.1~0.15~0.15~0.2~1.4~4.6915~5.4~7.35','65.117~64.817~66.817~65.918~63.168~61.717~58.717~60.667~62.667~64.3','-29.8~-31.75~-33.75~-35.75~-37.7~-39.7~-41.7~-43.65~-45.65~-47.65~-49.6'),(56,86,'INDIANB.NS','INDIANB.NS','0.12646048109966','54~45.3~36.05~27.3~18.4~10~6.15~3.75~2.5~1.25~0.25','0.4~1.35~2.6~2.65~5.8~10.35~19.2~28.75~37.7~47.8~56.8','245.65~254.9~264.1~273.35~282.55~291.8~301.05~310.25~319.5~328.7~337.95',153.5,'305.55~299.80~295.30~306.90~300.00~292.00~303.00~294.50~292.00~309.40~307.20~306.90~307.85~297.30~296.95','301.60~294.60~291.00~302.00~295.10~288.10~299.00~290.00~287.15~304.00~301.25~301.50~304.05~272.60~292.00','7.67~7.98~8.56~11.87~12.48~15.92~16.58~18.2~20.34~20.74~24.96','2010-11-01','183.13~158.83~130.58~125.88~88.133~56.27~38.85~26.428~18.65~9.0','0.65~2.25~5.3~4.8~10.25~15.025~23.6975~32.575~41.925~50.825~59.825','275.28~260.23~241.18~245.73~217.183~194.57~186.4~183.178~184.65~184.2','-83.15~-92.4~-101.6~-110.85~-120.05~-129.3~-138.55~-147.75~-157.0~-166.2~-175.45'),(57,89,'IOB.NS','IOB.NS','0.11430317848411','27.2~22.85~18.3~13.9~9.6~4.7~4.4~2.85~1.55~1.2~0.5','0.2~1~1.55~3.05~3.15~4.85~9.75~14.35~19.15~23.75~28.75','136.95~141.55~146.1~150.65~155.25~159.8~164.35~168.95~173.5~178.05~182.65',58.55,'152.40~150.90~157.70~163.20~162.90~160.45~160.80~157.45~157.75~160.45~164.70~164.35~172.95~168.00~161.60','150.75~148.75~155.05~158.10~160.30~157.20~158.60~154.25~155.00~158.60~161.10~162.90~167.75~159.10~158.10','7.1~7.81~8.12~8.16~8.45~9.96~11~12.51~12.74~21.6~21.9','2010-11-01','43.49~37.84~30.508~23.807~16.74~8.19~8.19~8.19~8.19~8.19','0.35~1.25~2.05~3.95~3.95~6.35~10~14.15~18.44~22.54~27.29','121.89~120.84~118.058~115.907~113.44~109.44~113.99~118.59~123.14~127.69','-73.4~-78.0~-82.55~-87.1~-91.7~-96.25~-100.8~-105.4~-109.95~-112.79~-114.22'),(58,90,'IOC.NS','IOC.NS','0.075505457147969','50.8~43~35.75~28.6~20.6~9.3~7.3~2.85~1.7~1.25~0.35','0.7~1.5~1.9~2.9~5.3~9.1~21.05~29.5~37.45~45.55~53.85','378.6~386.5~394.4~402.25~410.15~418.05~425.95~433.85~441.7~449.6~457.5',377.9,'425.30~419.85~416.60~423.10~411.50~410.70~411.00~407.95~411.80~420.90~435.00~432.50~426.75~427.50~422.95','421.35~415.00~414.00~417.95~405.00~404.00~407.30~403.35~407.45~415.65~426.65~428.65~420.80~419.00~419.95','5.33~6.64~9.78~10.49~11.06~15.13~15.37~16.82~23.92~24.26~24.58','2010-11-01','169.135~166.685~166.685~156.835~121.335~80.235~65.0~39.942~32.21~22.059','9.0~9.0~2.3~2.6~4.75~6.6~17.9~25.8~35.55~42~50.65','169.835~175.285~183.185~181.185~153.585~120.385~113.05~95.892~96.01~93.759','8.3~0.4~-7.5~-15.35~-23.25~-31.15~-39.05~-46.95~-54.8~-62.7~-70.6'),(59,91,'ISPATIND.NS','ISPATIND.NS','0.11764705882353','4.2~3.45~2.85~2.2~1.8~1.55~1.45~1.25~1.1~1~0.75','0.9~1.05~1.1~1.3~1.55~1.65~1.95~2.75~3.4~4.45~5.35','16.7~17.3~17.85~18.45~19~19.6~20.2~20.75~21.35~21.9~22.5',20.1,'23.10~22.95~22.80~22.75~22.50~22.10~22.40~21.55~21.45~21.60~21.35~21.20~21.20~20.40~20.15','22.65~22.50~22.05~22.20~22.25~21.70~21.30~21.20~21.20~21.10~21.15~20.50~20.50~20.00~19.75','8.59~9.27~13.5~13.69~13.74~14.52~15.67~16.49~18.84~19.63~20.64','2010-11-01','4.3575~3.7575~3.2075~2.655~2.105~1.505~24.95~27.8~27.6~25.85~20.05','0.1~0.1~0.1~0.1~0.1~0.1~0.1475~0.7975~1.6475~1.9475~2.7975','0.9575~0.9575~0.9575~1.005~1.005~1.005~25.05~28.45~28.85~27.65~22.45','3.5~2.9~2.35~1.75~1.2~0.6~0.047500000000002~0.1475~0.3975~0.1475~0.3975'),(60,92,'ITC.NS','ITC.NS','0.052178982655589','16.1~14~11.85~9.4~7.2~4.1~3.5~2.3~1.8~1.1~0.75','0.4~1.05~1.65~2.35~3.5~4~7.35~9.7~12.1~14.35~16.7','160.05~162.25~164.5~166.75~168.95~171.2~173.45~175.65~177.9~180.15~182.35',346.75,'174.45~172.80~173.00~176.85~176.00~172.85~171.90~171.30~169.85~174.50~173.40~170.60~171.90~169.70~173.50','173.00~169.25~170.50~174.40~172.25~170.05~169.10~168.75~167.30~171.65~171.10~169.10~170.45~167.05~171.80','7.21~7.77~15.9~16.34~16.39~17.34~20.82~21.8~23.28~23.36~24.95','2010-11-01','201.3695~199.0375~196.6525~194.2675~191.9355~189.551~189.551~189.551~176.65~180.0635','9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0','-156.908~-157.508~-151.057~-153.907~-156.757~-160.558~16.251~18.451~7.8~-8.95','195.7~193.5~191.25~189.0~186.8~184.55~182.3~180.1~177.85~175.6~173.4'),(61,98,'JSWSTEEL.NS','JSWSTEEL.NS','0.11415507420467','229.1~189.75~153~115.4~80.05~25.55~13.05~6.5~4.05~1.25~0.7','0.2~1.85~3.5~5.85~17.85~19.25~83.05~121.6~161.15~199.05~239.45','1150.45~1188.75~1227.05~1265.35~1303.65~1341.95~1380.25~1418.55~1456.85~1495.15~1533.45',1120.55,'1393.00~1371.55~1343.50~1368.65~1357.00~1318.00~1329.90~1287.00~1276.00~1296.00~1274.50~1248.55~1261.00~1364.00~1362.00','1344.05~1333.35~1331.15~1349.45~1338.00~1292.10~1313.35~1273.50~1260.90~1256.70~1260.00~1232.70~1216.70~1303.00~1332.00','5.67~10.22~12.2~14.36~16.79~16.86~18.32~20.11~22.95~23.58~24.45','2010-11-01','552.01~552.01~552.01~525.55~415.35~163.85~99.85~55.1~31.15~14.0','0.05~1.65~2.55~4.55~15.1~16.85~36.5075~78.1575~113.9075~154.1575~191.6075','581.91~620.21~658.51~670.35~598.45~385.25~359.55~353.1~367.45~388.6','-15.9~-54.2~-92.5~-130.8~-169.1~-207.4~-245.7~-284.0~-322.3~-360.6~-398.9'),(62,100,'KOTAKBANK.NS','KOTAKBANK.NS','0.095838844236443','68.7~57.45~46.85~36.15~25.25~8.75~5.2~3.35~1.9~1.7~0.8','0.7~1.45~2.55~4.6~6.65~9.85~25.85~36.9~49.4~60.7~71.8','408.65~419.75~430.9~442~453.15~464.25~475.35~486.5~497.6~508.75~519.85',1016.95,'512.90~523.15~509.80~526.55~515.00~511.00~514.00~512.15~506.00~506.45~500.85~503.20~493.85~480.00~474.30','506.65~512.50~503.15~515.80~508.00~502.25~504.15~502.00~500.60~502.00~495.20~498.15~485.50~467.90~469.00','6.15~7.31~9.29~9.6~10.47~12.38~14.24~16.36~16.89~19.27~24.16','2010-11-01','654.9675~643.2015~631.3825~619.6165~607.7975~596.0315~584.2655~572.4465~560.6805~548.8615','14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0','-408.79~-409.24~-405.39~-429.39~-452.74~-505.0~-512.3~-505.333~-502.967~-494.2','622.3~611.2~600.05~588.95~577.8~566.7~555.6~544.45~533.35~522.2~511.1'),(63,101,'KSOILS.NS','KSOILS.NS','0.13866666666667','11.35~9.5~7.55~5.6~3.7~2.45~2.2~1.6~1.25~1.1~0.65','0.9~1.2~1.55~1.75~2.05~3.05~3.85~6~8~10~12.1','47.1~49.05~51.05~53~55~56.95~58.9~60.9~62.85~64.85~66.8',60.6,'54.90~56.40~55.45~56.30~54.00~53.90~53.95~53.50~52.85~56.25~58.40~60.15~59.60~57.90~61.50','53.25~55.05~54.90~55.15~53.20~52.55~53.20~52.45~52.35~54.85~56.20~58.55~58.45~56.50~59.10','6.07~6.88~7.02~9.49~12.57~13.93~15.46~22.09~23.57~24.2~24.49','2010-11-01','13.8925~11.7425~10.0125~8.0625~6.2625~4.8125~2.8625~1.8025~10.785~10.05~5.8','1.25~1.55~1.8~2.05~1.65~1.95~2.9~3.3275~5.2775~7.2775~9.2275','0.3925~0.1925~0.4625~0.4625~0.6625~1.1625~1.1625~2.1025~13.035~14.3~12','14.75~13.1~11.35~9.65~7.25~5.6~4.6~3.0275~3.0275~3.0275~3.0275'),(64,102,'LICHSGFIN.NS','LICHSGFIN.NS','0.11769804438506','233.2~196.75~157~119.7~81.15~20.45~13.95~4.45~2.65~1.85~0.6','0.35~2.05~2.15~5.75~15.1~39.45~84.1~122.6~165.55~206.65~246.65','1143.3~1182.75~1222.2~1261.65~1301.1~1340.55~1380~1419.45~1458.9~1498.35~1537.8',333.6,'1439.00~1431.30~1431.30~1466.00~1465.95~1392.50~1367.45~1334.70~1334.80~1350.80~1375.90~1382.35~1396.25~1360.00~1359.35','1420.00~1401.65~1400.00~1425.55~1436.35~1343.20~1343.95~1303.00~1320.00~1340.00~1355.70~1370.00~1377.75~1338.00~1335.25','6.81~10.75~11.09~18.13~18.48~21.47~23.38~23.76~23.85~24.31~24.88','2010-11-01','782.491~782.491~758.657~758.657~591.497~180.25~131.2~45.4~29.5~20.0','0.05~1.25~1.3~3.8~12.05~32.05~49.7675~77.1175~118.7175~156.0175~203.0675','1592.191~1631.641~1647.257~1686.707~1558.997~1187.2~1177.6~1131.25~1154.8~1184.75','-800.7~-840.15~-879.6~-919.05~-958.5~-997.95~-1037.4~-1076.85~-1116.3~-1155.75~-1195.2'),(65,103,'LITL.NS','LITL.NS','0.08030303030303','8.15~6.85~5.75~4.4~3.15~2.55~2.05~1.55~1.45~1~0.75','0.25~1.15~1.25~1.6~1.95~2.3~3.25~4.65~6~7.2~8.55','57.35~58.65~59.9~61.2~62.45~63.75~65.05~66.3~67.6~68.85~70.15',7.35,'71.20~70.50~70.45~70.30~68.40~68.00~68.25~67.45~67.90~68.65~67.30~66.80~68.00~64.70~64.65','70.30~69.20~69.55~69.55~67.50~66.70~67.25~66.50~67.25~67.80~66.75~63.35~66.65~63.40~63.75','8.13~9.16~13.44~15.59~16.08~17.04~19.81~21.7~22.29~22.5~23.56','2010-11-01','14.52~12.92~12.727~10.685~8.27~6.555~6.555~6.3~6.15~5.0','0.4~0.85~0.85~1.05~1.4~1.4~2.733~3.748~5.3525~6.1~7.8','64.52~64.22~65.277~64.535~63.37~62.955~64.255~65.25~66.4~66.5','-45.0~-46.3~-47.55~-48.85~-50.1~-51.4~-52.7~-53.95~-55.25~-56.5~-57.8'),(66,104,'LT.NS','LT.NS','0.042989508959274','165.2~144.25~125.95~101.05~80.6~36.1~12.05~7.9~3.95~1.55~1','0.6~1.65~3.8~8.3~18.25~31.9~84.8~107.65~126.6~151.75~173.9','1918.85~1940.65~1962.4~1984.2~2006~2027.8~2049.6~2071.4~2093.2~2114.95~2136.75',1451.5,'2065.00~2040.15~2009.40~2089.00~2024.85~1997.10~2038.00~2006.00~2017.85~2042.30~2035.90~2033.00~2037.95~2018.00~2059.8999','1942.45~2007.00~1985.30~2052.30~2006.90~1956.35~2001.10~1985.80~2005.00~2021.00~2018.00~2015.15~2018.00~2000.10~2044.00','5.88~6.34~8.65~8.85~9.81~12.88~13.34~14~14.77~15.55~19.33','2010-11-01','593.45~552.45~552.45~502.05~445.2~311.365~110.7~66.7~36.2~15.8','0.05~0.05~0.05~0.05~0.05~0.05~0.05~0.05~21.3795~47.0345~68.4925','1060.8~1041.6~1063.35~1034.75~999.7~887.665~708.8~686.6~677.9~679.25','-453.35~-475.15~-496.9~-518.7~-540.5~-562.3~-584.1~-605.9~-627.7~-649.45~-671.25'),(67,105,'LUPIN.NS','LUPIN.NS','0.11180551573878','73.4~61.15~49.9~37.8~26.15~9.3~5.55~3.65~2.05~1.2~0.6','0.65~1.6~1.95~4.05~7.3~9.05~26.1~38.95~52.05~64.95~77.25','377.05~389.3~401.55~413.8~426.05~438.3~450.55~462.8~475.05~487.3~499.55',1325,'416.80~414.90~430.00~428.80~433.75~435.00~419.50~429.10~445.80~446.80~449.80~460.40~449.80~448.95~459.00','412.00~409.80~425.00~424.00~427.70~420.05~411.65~421.00~434.75~437.00~443.25~449.20~440.25~439.10~440.00','6.52~6.73~7.89~8.73~8.96~10.76~12.23~19.27~20.48~21.68~23.33','2010-11-01','1018.077~1005.092~992.107~979.122~966.137~953.152~940.167~927.182~914.197~901.212','14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0','-709.317~-728.915~-734.115~-753.515~-782.015~-824.225~-831.315~-807.558~-807.677~-798.367','961.95~949.7~937.45~925.2~912.95~900.7~888.45~876.2~863.95~851.7~839.45'),(68,107,'MARUTI.NS','MARUTI.NS','0.058139153803297','157~133.95~113.4~89.65~70.45~24.6~16.55~7.5~2.1~1.7~0.95','0.75~1.35~3.7~7.9~12.35~19.3~72.15~93.15~115.05~138.45~163.3','1438.85~1461.4~1483.95~1506.5~1529.05~1551.6~1574.15~1596.7~1619.25~1641.8~1664.35',2980.5,'1513.00~1529.00~1554.00~1567.50~1560.55~1520.00~1505.00~1508.00~1501.65~1523.50~1520.80~1565.00~1570.00~1545.00~1560.00','1495.00~1508.00~1518.00~1541.35~1551.05~1491.00~1486.00~1486.00~1481.30~1515.00~1504.90~1511.00~1556.00~1516.05~1501.00','5.3~6.87~14.5~15.48~17.61~19.49~21.37~22.39~23.21~23.75~24.24','2010-11-01','1663.954~1640.051~1616.148~1592.245~1568.342~1544.439~1520.536~1496.633~1472.73~1448.827','14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0','-1071.6~-1048.325~-713.97~-836.1~-880.9~-1206.368~-1228.69~-1256.11~-1269.493~-1273.2','1555.65~1533.1~1510.55~1488.0~1465.45~1442.9~1420.35~1397.8~1375.25~1352.7~1330.15'),(69,109,'MLL.NS','MLL.NS','0.10394397346111','10.1~8.5~6.9~5.2~3.6~2.85~2.15~1.85~1.55~1.05~0.9','0.35~1~1.55~1.9~1.95~2.6~3.75~5.45~7.1~8.85~10.6','56.1~57.75~59.45~61.1~62.8~64.45~66.1~67.8~69.45~71.15~72.8',67.7,'69.00~68.20~67.80~66.85~67.40~65.70~65.45~66.45~66.35~66.80~66.25~71.35~69.75~68.35~65.65','67.40~66.00~66.60~65.40~66.15~64.60~64.30~65.00~65.65~65.85~65.55~70.10~68.70~66.85~64.80','5.39~7.89~8.08~10.98~11.44~14.43~15.34~17.27~19.69~22.03~22.18','2010-11-01','14.985~13.335~11.635~9.985~8.285~6.635~4.985~9.6~11.5175~33.9~21.1','0.05~0.1~0.1~0.1~0.1~0.1~0.1~0.15~1.9275~3.6375~5.15','3.385~3.385~3.385~3.385~3.385~3.385~3.385~9.7~13.2675~37.35~26.2','11.65~10.05~8.35~6.7~5~3.35~1.7~0.050000000000006~0.1775~0.1875~0.050000000000006'),(70,110,'MOSERBAER.NS','MOSERBAER.NS','0.075471698113208','7.95~6.7~5.55~4.3~3.15~2.4~1.8~1.75~1.4~1.15~0.75','0.7~1.05~1.3~1.6~2.15~2.55~3.2~4.55~5.75~7~8.25','58.7~59.9~61.15~62.35~63.6~64.8~66~67.25~68.45~69.7~70.9',7,'70.80~74.80~72.70~72.90~71.50~69.85~69.55~69.40~70.75~70.85~70.00~69.50~68.20~67.00~65.10','69.50~73.30~72.00~71.35~70.50~68.30~69.00~68.20~69.50~69.70~69.25~68.65~67.50~66.30~62.90','5.68~6.51~11.92~13.07~13.93~15.06~16.18~17.8~18.5~23.52~23.89','2010-11-01','31.364~29.292~29.292~29.292~25.55~21.1~17.3~17.3~17.3~17.3','1.25~2.35~2.6~3.2~1.1725~2.05~3.15~3.1675~4.2675~6.85~8.0225','83.064~82.192~83.442~84.642~82.15~78.9~76.3~77.55~78.75~80.0','-46.7~-47.9~-49.15~-50.35~-51.6~-52.8~-54.0~-55.25~-56.45~-57.7~-58.9'),(71,111,'MPHASIS.NS','MPHASIS.NS','0.06906966577912','69.8~59~48.75~38.9~28.5~9.55~6~3.6~2.2~1.05~0.7','0.45~1.3~2.95~3.15~8.95~13.7~29.1~40.1~51.8~62.5~72.75','557.7~568.25~578.8~589.3~599.85~610.4~620.95~631.5~642~652.55~663.1',418.05,'672.00~649.50~649.00~661.40~650.80~641.25~639.60~625.70~627.60~632.90~631.60~629.70~629.80~614.90~615.30','657.00~632.25~642.00~655.00~631.70~625.10~630.10~614.90~622.00~626.05~628.00~622.00~625.25~607.35~612.00','5.37~6.11~6.45~8.17~9.1~12.18~14.49~16.99~20.56~23.12~23.45','2010-11-01','63.925~53.87~44.845~34.675~31.555~27.99~20.893~9.0~9.0~9.0','0.8~1.3~2.45~2.65~8.4~12.55~27.7~38.4~50.25~60.05~70.95','203.575~204.07~205.595~205.925~213.355~220.34~223.793~222.45~232.95~243.5','-130.65~-141.2~-151.75~-162.25~-172.8~-179.2~-174.25~-173.85~-172.5~-173.1~-172.7'),(72,112,'MRPL.NS','MRPL.NS','0.15994962216625','18.4~15.2~12.15~9~5.8~3.5~2.75~2.15~1.35~1.3~0.5','0.95~1.2~1.55~2.05~3~4~5.95~9.3~12.8~16~19.4','64.95~68.2~71.45~74.7~77.95~81.2~84.45~87.7~90.95~94.2~97.45',60.55,'82.85~82.25~82.00~82.85~80.95~81.45~82.00~80.75~80.45~81.10~83.70~84.80~85.75~82.65~82.70','82.00~81.30~80.80~81.65~80.30~80.30~80.90~73.05~79.80~80.45~82.55~83.35~82.65~80.15~81.55','6.28~9.03~11.76~13.61~15.02~15.96~16.25~18.01~21.79~23.15~24.31','2010-11-01','56.313~56.313~56.313~49.163~34.547~22.398~21.762~19.123~15.037~15.037','5.0~1.4~1.65~2.15~2.1~3~5.55~9.0125~12.1625~15.4625~18.8125','60.713~63.963~67.213~63.313~51.947~43.048~45.662~46.273~45.437~48.687','0.6~-2.65~-5.9~-9.15~-12.4~-15.65~-18.9~-22.15~-25.4~-28.65~-31.9'),(73,113,'MTNL.NS','MTNL.NS','0.08','8.6~7.3~6.1~4.75~3.45~2.75~1.85~1.75~1.35~1.15~0.25','0.95~1.15~1.45~1.75~2.2~2.8~3.45~4.85~6.25~7.65~9.1','60.75~62.1~63.45~64.8~66.15~67.5~68.85~70.2~71.55~72.9~74.25',27.55,'66.70~66.40~71.20~69.80~67.65~67.40~68.25~67.60~67.60~68.70~68.00~71.50~69.95~69.45~69.90','65.80~65.70~68.05~68.35~66.75~66.00~66.55~66.25~66.80~66.20~67.50~70.55~68.85~68.50~68.30','5.37~5.81~6.85~7.12~8.75~11.95~16.43~18.04~18.04~22.09~23.61','2010-11-01','30.722~27.773~26.172~21.37~18.47~18.27~18.27~18.27~18.27~18.27','0.15~0.15~0.15~0.15~0.1~0.15~1.161~1.9~3.7205~4.5~6.435','63.922~62.323~62.072~58.62~57.07~58.22~59.57~60.92~62.27~63.62','-28.2~-29.55~-30.9~-32.25~-33.6~-34.95~-36.3~-37.65~-39.0~-40.35~-41.7'),(74,115,'NAGARFERT.NS','NAGARFERT.NS','0.11095305832148','5.6~4.75~3.75~2.9~2~1.75~1.4~1.25~1.15~1.05~0.6','1~1.1~1.25~1.45~1.6~1.65~2.05~3~3.95~4.9~5.9','29.1~30.05~31~31.9~32.85~33.8~34.75~35.7~36.6~37.55~38.5',34.1,'32.15~34.45~34.45~35.00~37.10~36.20~34.65~34.65~34.45~34.50~34.95~34.45~34.10~33.95~34.95','31.50~33.50~33.80~33.90~35.95~35.50~34.30~33.25~33.80~34.00~34.25~33.80~33.80~33.20~34.00','6.38~9.45~13.26~15.56~17.94~20.99~22.03~22.57~22.91~24.59~24.7','2010-11-01','5.6975~4.31~3.36~2.46~1.9475~0.9975~12.3875~10.3375~10.5375~9.8375~6.8875','0.95~0.95~0.9~1.15~1.15~1.15~1.85~3.1~4.0975~5~5.95','0.6975~0.26~0.26~0.26~0.6975~0.6975~13.0375~11.9375~13.0375~13.2875~11.2875','5.95~5~4~3.35~2.4~1.45~1.2~1.5~1.5975~1.55~1.55'),(75,118,'NOIDATOLL.NS','NOIDATOLL.NS','0.10682492581602','5.1~4.3~3.45~2.65~1.8~1.65~1.4~1.35~1.2~1.1~0.35','0.75~1.05~1.2~1.35~1.45~1.65~1.85~2.75~3.6~4.55~5.35','27.45~28.3~29.15~30~30.85~31.7~32.55~33.4~34.25~35.1~35.95',32.2,'35.40~36.10~35.75~35.65~35.50~34.55~34.45~34.10~34.05~34.05~33.90~33.65~33.25~32.30~32.30','34.55~35.00~35.10~35.25~34.95~34.00~33.80~33.70~33.60~33.50~33.40~32.75~32.85~31.90~31.00','6.3~6.46~9.14~9.66~9.84~13.96~16.93~19.91~20.53~22.18~23.06','2010-11-01','6.315~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0','5.0~5.0~5.0~5.0~5.0~5.0~1.96~2.81~3.66~4.51~5.0','1.565~1.1~1.95~2.8~3.65~4.5~5.35~6.2~7.05~7.9','9.75~8.9~8.05~7.2~6.35~5.5~4.65~3.8~2.95~2.1~1.25'),(76,119,'NTPC.NS','NTPC.NS','0.067120743034056','21.75~18.85~15.35~12.2~9.2~6.2~3.3~2.6~1.55~1.3~0.95','0.9~1.1~1.7~3~3.7~6.05~9.3~12.6~16~19.4~22.9','178.9~182.2~185.45~188.75~192~195.3~198.6~201.85~205.15~208.4~211.7',193.2,'216.50~213.50~211.10~210.50~205.80~205.00~208.00~205.65~208.00~206.80~207.25~208.65~205.60~198.00~198.10','211.40~210.35~208.55~208.25~203.00~202.05~204.10~203.25~205.00~204.75~204.10~207.15~203.15~195.10~196.10','6.93~9.19~9.3~12.43~14.56~14.65~19.35~20~20.77~22.72~24.99','2010-11-01','18.57~13.315~11.14~9.0~9.0~9.0~9.0~9.0~9.0~9.0','9.0~9.0~9.0~9.0~9.0~9.0~10.55~14.55~17.9~22.85~24.55','4.27~2.315~3.39~4.55~7.8~11.1~14.4~17.65~20.95~24.2','23.3~20.0~16.75~13.45~10.2~6.9~5.15~5.9~5.95~7.65~6.05'),(77,121,'ONGC.NS','ONGC.NS','0.061033215865637','135.35~116.75~96.85~78.5~59.95~28.75~10.8~5.75~3.5~1.6~0.7','0.65~1.05~3.4~6.05~11.1~30.9~61.55~81.8~102.7~122.85~140.75','1204.55~1224.45~1244.35~1264.25~1284.15~1304.05~1323.95~1343.85~1363.75~1383.65~1403.55',403.9,'1384.50~1371.60~1377.90~1387.60~1370.00~1344.75~1374.40~1363.95~1357.95~1368.40~1360.00~1354.90~1323.65~1327.45~1324.00','1367.50~1346.05~1360.35~1366.35~1351.90~1327.50~1351.10~1340.00~1342.10~1355.05~1345.00~1340.00~1315.00~1293.00~1310.00','5.51~5.56~12.02~12.03~13.63~15.55~19.95~20~20.06~22.02~22.31','2010-11-01','9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0','0.65~0.75~1.9~3.85~6.75~920.345~940.245~960.145~980.045~999.945~1019.845','809.65~829.55~849.45~869.35~889.25~909.15~929.05~948.95~968.85~988.75','-767.15~-767.4~-683.95~-582.25~-371.6~203.5~1006.6~1341.15~1916.45~658.9~862.8'),(78,127,'PATNI.NS','PATNI.NS','0.086502934144751','63.1~53.35~43.5~34.15~24.55~14.3~7.85~3.8~2.65~1~0.3','0.9~1.5~2.4~4~7.05~10.75~24.15~35.1~45.5~56.1~66.55','412.65~422.7~432.7~442.7~452.7~462.7~472.7~482.7~492.7~502.7~512.75',467.9,'455.00~452.80~447.80~460.00~480.00~474.80~465.60~464.65~463.00~472.85~469.70~469.95~475.00~464.50~469.30','440.10~444.45~443.00~452.00~472.05~440.20~457.00~457.55~458.05~465.15~461.90~464.30~463.50~453.15~463.10','6.74~7.51~8.77~9.93~11.35~13~13.05~20.01~21.55~22.9~24.51','2010-11-01','78.645~68.595~58.595~48.595~38.595~28.595~69.069~47.849~32.782~18.162~4.4','0.05~0.05~0.05~0.1~0.1~0.1~5.2~15.4~25.827~36.327~45','23.395~23.395~23.395~23.395~23.395~23.395~73.869~62.649~57.582~52.962~49.25','55.3~45.25~35.25~25.3~15.3~5.3~0.39999999999999~0.59999999999999~1.027~1.527~0.14999999999998'),(79,128,'PETRONET.NS','PETRONET.NS','0.14710743801653','23.4~19.4~15.45~11.65~7.6~4.1~3.3~2.7~1.5~1.1~0.5','0.95~1.2~1.4~2.8~3.45~4.9~7.9~12~16.3~20.55~24.9','90.75~94.85~98.95~103~107.1~111.2~115.3~119.4~123.45~127.55~131.65',180.1,'114.50~114.20~116.90~121.40~118.40~115.60~125.85~124.70~124.30~129.20~129.90~122.20~118.20~115.50~114.45','109.75~113.00~113.20~119.55~116.10~112.10~122.50~119.00~122.80~126.25~126.20~119.55~116.00~112.15~112.25','5.79~5.84~7.86~10.1~12.46~13.98~15.5~16.05~19.93~21.32~24.86','2010-11-01','96.512~92.166~87.82~83.527~79.181~74.835~70.489~66.143~61.85~57.504','9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0','-12.208~-19.525~-16.725~-18.025~-25.225~-36.81~-38.61~-41.425~-35.177~-36.27','98.35~94.25~90.15~86.1~82.0~77.9~73.8~69.7~65.65~61.55~57.45'),(80,129,'PFC.NS','PFC.NS','0.058220582205822','36.45~31.25~26.2~21.35~15.75~6.8~4.75~3.7~2.1~1.5~0.8','0.45~1.15~2.1~3.15~4.1~9.25~16.75~22~27.35~32.2~37.75','333.8~339.05~344.3~349.5~354.75~360~365.25~370.5~375.7~380.95~386.2',245.5,'376.40~378.00~371.00~385.00~376.50~365.00~367.70~360.45~367.65~372.00~370.90~376.00~374.85~363.50~367.00','364.05~372.25~366.15~375.30~374.25~355.20~362.00~355.60~364.05~365.30~368.05~371.80~369.60~359.00~361.40','5.89~6.99~8.55~10~12.37~13.44~13.49~15.18~17.99~18.85~20.17','2010-11-01','9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0','0.1~0.5~0.15~0.05~0.05~0.25~2.1~10.705~16.317~21.982~27.497','97.3~102.55~107.8~113.0~118.25~123.5~128.75~134.0~139.2~144.45','-79.3~-82.65~-80.55~-81.0~-82.65~-54.6~-18.25~-8.795~-1.733~-5.518~-6.553'),(81,130,'PIRHEALTH.NS','PIRHEALTH.NS','0.17433508056688','115.95~96.45~76.3~55.6~35.6~16.7~6.25~5.3~2.15~1.55~0.85','0.85~1.4~2.8~4.65~8.45~15.2~37.65~58.75~80.15~102.35~123.7','373.25~394.05~414.85~435.65~456.45~477.25~498.05~518.85~539.65~560.45~581.25',478.95,'524.55~522.90~522.45~519.85~524.00~524.60~538.00~560.00~553.00~549.50~510.00~499.00~492.75~480.00~481.00','517.50~514.25~514.15~515.30~515.30~515.50~526.10~545.00~545.00~519.00~491.35~491.20~485.10~470.20~470.70','6.08~6.37~9.4~10.57~12.27~12.57~13.13~13.67~13.73~17.75~20.36','2010-11-01','128.35~108.15~86.7~66.7~43.0705~23.72~20.3925~23.575~23.0145~21.185~11.666','0.15~0.25~1.05~1.55~4.05~8.15~20.2~43.5~65.95~83.7~107.55','22.65~23.25~22.6~23.4~20.5705~22.02~39.4925~63.475~83.7145~102.685~113.966','105.85~85.15~65.15~44.85~26.55~9.85~1.1~3.6~5.25~2.1999999999999~5.25'),(82,131,'PNB.NS','PNB.NS','0.067964872088583','145.15~124.9~103.95~83.35~61.95~19.55~10.35~7.5~3.85~1.35~0.5','0.55~1.5~2.35~7.2~10.4~26.3~62.5~84.55~108.45~129.3~152.55','1181.15~1203.05~1225~1246.95~1268.85~1290.8~1312.75~1334.65~1356.6~1378.55~1400.45',1030,'1319.00~1322.50~1318.00~1360.00~1339.35~1311.20~1317.00~1311.80~1305.50~1324.70~1344.00~1354.00~1347.00~1307.55~1310.00','1311.55~1311.60~1307.05~1344.80~1319.55~1289.50~1295.00~1297.00~1292.05~1315.00~1335.00~1339.00~1335.80~1265.00~1297.00','5.95~6.46~12.22~13.74~15.74~16.9~16.99~17.78~18.73~21.93~22.83','2010-11-01','14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0','0.1~0.6~0.65~2.1~3.05~7.35~334.25~356.15~378.1~400.05~421.95','165.15~187.05~209.0~230.95~252.85~274.8~296.75~318.65~340.6~362.55','-137.15~-159.05~-176.4~-162.65~-163.75~-72.2~135.1~207.25~274.5~67.5~98.8'),(83,132,'POLARIS.NS','POLARIS.NS','0.15613382899628','35.7~29.65~23.6~17.5~11.45~5.45~4.9~2.45~1.95~1.05~0.4','0.55~1.25~2.05~2.5~3.55~5.6~11.7~18.2~24.95~31.4~37.8','130.2~136.5~142.8~149.1~155.45~161.75~168.05~174.4~180.7~187~193.3',163.75,'174.50~177.20~176.80~182.00~181.25~176.35~177.00~174.30~172.40~173.00~174.95~173.55~173.40~166.45~164.60','172.00~174.10~175.00~180.10~178.70~170.00~174.50~170.25~171.00~171.50~173.80~172.50~171.10~155.00~162.60','5.37~8.28~9.61~10.63~12.71~13.21~16.02~18.62~20.29~21.51~23.5','2010-11-01','39.13~32.038~26.18~20.23~14.352~9.0~9.0~9.0~9.0~9.0','9.0~9.0~9.0~9.0~9.0~9.0~9.0~13.25~20.15~27.0~32.4','5.58~4.788~5.23~5.58~6.052~7.0~13.3~19.65~25.95~32.25','42.55~36.25~29.95~23.65~17.3~11.0~4.7~2.6~3.2~3.75~2.85'),(84,133,'POWERGRID.NS','POWERGRID.NS','0.08337326305702','13.15~11.15~9.25~7.2~5.25~3.85~2.65~1.85~1.55~1.2~0.65','0.7~1.25~1.35~1.8~2.3~3.15~5.15~7.35~9.55~11.65~14','89.95~92.05~94.1~96.2~98.3~100.4~102.5~104.6~106.7~108.75~110.85',135.1,'111.75~110.15~110.30~110.80~107.50~108.70~107.50~105.95~107.00~107.45~107.90~104.40~103.25~100.70~101.70','109.80~109.00~109.30~109.25~106.35~107.10~106.50~105.00~105.90~105.70~105.40~103.00~102.50~100.00~100.80','5.17~5.55~6.17~9.54~13.24~16.85~19.02~19.5~19.77~22.14~24.34','2010-11-01','49.21~46.984~44.811~42.585~40.359~38.133~38.133~38.133~38.133~38.133','9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0','-12.819~-14.395~-16.068~-12.744~-12.67~-13.525~5.533~7.633~9.733~11.783','54.15~52.05~50.0~47.9~45.8~43.7~41.6~39.5~37.4~35.35~33.25'),(85,134,'PRAJIND.NS','PRAJIND.NS','0.067586206896552','7.85~6.65~5.5~4.4~3.15~2.3~2~1.5~1.35~1.15~0.6','0.55~1.1~1.35~1.5~2~2.5~3.25~4.5~5.7~6.8~8.2','63.1~64.3~65.45~66.6~67.8~68.95~70.1~71.3~72.45~73.6~74.8',62.15,'81.80~77.40~75.10~75.80~74.95~72.05~72.00~72.20~71.95~71.75~72.30~72.10~72.55~72.00~72.00','79.20~75.45~74.30~74.40~74.05~70.20~70.70~71.00~70.65~71.10~71.30~71.55~71.50~70.05~69.35','5.89~6.3~9.65~10~10.18~11.01~11.32~16.79~19.92~22.73~24.69','2010-11-01','5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0','4.0575~5.0~5.0~5.0~0.3~0.25~0.35~1.35~3.1945~3.65~5.5355','5.95~7.15~8.3~9.45~10.65~11.8~12.95~14.15~15.3~16.45','4.05~2.85~1.7~0.55~-0.65~-1.8~-2.95~-2.1~-0.155~-2.4~-1.014'),(86,135,'PTC.NS','PTC.NS','0.14547559328574','27.95~23.35~18.6~13.9~9.1~5.75~4.15~2.55~2.05~1.3~0.95','0.8~1.15~1.75~2.9~3.8~6.1~9.45~14.35~19.4~24.7~29.7','110.1~115~119.85~124.75~129.65~134.55~139.45~144.35~149.25~154.1~159',83.75,'125.90~124.35~122.50~123.80~122.60~124.15~130.00~132.40~133.00~136.00~139.00~139.00~137.90~136.75~137.80','123.55~122.30~120.70~121.50~120.15~121.00~127.00~128.30~131.00~132.65~121.00~136.45~135.10~134.25~135.25','6.51~7.61~10.05~11.36~12.71~12.88~14.37~15.51~22.81~23.79~24.02','2010-11-01','5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0','0.05~0.1~0.1~0.1~0.1~0.15~59.8875~64.7875~69.6875~74.5375~79.4375','31.35~36.25~41.1~46.0~50.9~55.8~60.7~65.6~70.5~75.35','-14.5~-14.35~-11.5~-13.25~-12.25~-2.4~22.715~48.75~68.65~64.25~67.65'),(87,136,'PUNJLLOYD.NS','PUNJLLOYD.NS','0.0990021522207','17.7~14.9~12.15~9.4~6.35~3.55~3.3~2.3~1.7~1.05~0.7','0.7~1.35~1.55~2.2~3~4.2~6.6~9.75~12.75~15.5~18.75','102.85~105.8~108.7~111.6~114.5~117.4~120.3~123.2~126.1~129~131.95',37,'135.10~136.00~135.90~137.50~134.10~132.00~129.40~128.00~127.30~130.95~130.65~128.25~127.00~128.90~122.00','133.10~133.90~134.70~135.10~131.25~128.20~128.30~126.20~125.90~129.00~129.30~127.00~125.50~121.45~118.65','5.11~5.95~6.36~9.84~14~16.11~16.63~16.9~19.22~20.12~20.22','2010-11-01','5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0','0.1~0.1~0.1~76.45~79.35~82.25~85.15~88.05~90.95~93.85~96.8','70.85~73.8~76.7~79.6~82.5~85.4~88.3~91.2~94.1~97.0','-33.0~-5.9~-2.7~11.5~38.0~76.25~130.6~136.45~101.95~127.735~136.162'),(88,137,'RANBAXY.NS','RANBAXY.NS','0.072276977508213','68.8~58.2~48.05~38.7~28.25~14.8~8.25~3.5~2.55~1.7~0.4','0.65~1.75~2.65~4.95~6.45~12.55~28.25~39.75~50.7~60.7~71.95','527.35~537.85~548.3~558.8~569.25~579.75~590.25~600.7~611.2~621.65~632.15',601.55,'592.80~602.00~601.40~612.40~603.40~586.25~585.40~586.40~594.90~602.00~613.45~615.00~601.90~587.65~588.40','587.50~596.00~593.20~604.05~593.00~573.00~578.30~572.10~588.10~595.00~608.15~611.00~594.80~576.00~582.25','5.18~5.61~7.95~14.24~16.85~17.65~18.19~19.77~19.8~21.32~24.63','2010-11-01','103.26~92.82~82.08~70.41~60.51~51.48~39.028~31.168~16.53~14.0','14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~22.0~39.689','29.06~29.12~28.83~27.66~28.21~29.68~27.728~30.318~26.18~34.1','88.2~77.7~67.25~56.75~46.3~35.8~25.3~14.85~4.35~1.9~9.089'),(89,138,'RCOM.NS','RCOM.NS','0.076709282934964','22.35~18.85~15.45~12.1~8.8~5.1~3.8~2.7~2.05~1.1~1','0.3~1.3~2.05~2.65~4.2~5.55~9~12.55~16.3~19.6~23.55','162.85~166.3~169.75~173.2~176.65~180.1~183.55~187~190.45~193.9~197.35',183.55,'182.10~185.70~187.75~188.00~186.80~176.90~178.00~175.50~176.30~180.40~179.90~180.30~176.90~182.50~182.50','178.35~182.65~185.20~183.05~180.00~173.00~175.00~173.35~174.35~178.30~177.20~178.35~174.35~178.25~180.25','7.28~7.85~10.76~12.33~13.09~18.9~19.49~20.07~21.5~22.68~24.67','2010-11-01','29.878~26.427~22.977~19.527~16.078~12.627~9.0~9.0~9.0~9.0','9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~12.806~13.8','9.178~9.177~9.177~9.177~9.178~9.177~9.0~12.45~15.9~19.35','29.7~26.25~22.8~19.35~15.9~12.45~9.0~5.55~2.1~2.456~1.7763568394e-14'),(90,139,'RECLTD.NS','RECLTD.NS','0.078531912022669','46.7~39.4~32.4~25.9~18.25~10.9~6.3~3.4~2~1.3~0.4','0.45~1.6~2.2~3.9~4.9~10.65~18.95~26.55~33.55~41.85~48.75','334.25~341.55~348.8~356.1~363.35~370.65~377.95~385.2~392.5~399.75~407.05',254.5,'409.00~413.80~384.70~385.00~379.70~365.00~377.20~366.90~371.00~376.00~385.10~374.00~371.60~367.95~375.90','387.00~393.00~377.60~381.00~373.00~356.00~369.00~362.00~361.85~371.10~378.75~364.10~365.15~363.40~372.50','6.15~7.88~8.47~10.64~14.33~14.54~19.86~21.38~21.84~22.21~22.71','2010-11-01','9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0','1~3.85~5.05~8.8~9.9~15.95~136.175~143.425~150.725~157.975~165.275','88.75~96.05~103.3~110.6~117.85~125.15~132.45~139.7~147.0~154.25','-70.75~-67.55~-68.1~-56.1~-56.1~-32.0~25.828~30.628~31.377~51.477~38.277'),(91,141,'RELIANCE.NS','RELIANCE.NS','0.067101584342964','122.5~104.35~87.7~68.8~52.6~23.25~13.65~6.8~2.55~1.35~0.95','0.4~1.85~2.9~5.55~9.95~15.95~52.75~70.95~89.75~109.35~127.1','1004.3~1022.7~1041.1~1059.45~1077.85~1096.25~1114.65~1133.05~1151.4~1169.8~1188.2',969.5,'1069.00~1069.40~1064.30~1080.00~1067.95~1053.00~1068.70~1048.85~1064.30~1093.80~1098.50~1096.90~1109.00~1090.90~1124.90','1053.10~1057.20~1054.70~1072.00~1051.20~1037.00~1057.65~1038.00~1049.20~1082.25~1087.35~1087.10~1094.20~1080.50~1107.15','6.73~11.91~14.27~14.93~15.98~17.04~17.58~18.26~18.48~20.98~21.85','2010-11-01','14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0','0.3~2.05~3.05~4.75~8.55~13.55~168.399~212.025~230.375~248.775~249.362','48.8~67.2~85.6~103.95~122.35~140.75~159.15~177.55~195.9~214.3','-20.8~-39.2~-57.6~-72.25~-77.5~-78.5~23.249~60.35~92.45~96.8~30.662'),(92,142,'RELINFRA.NS','RELINFRA.NS','0.073971014492754','124.8~105.85~87.4~68.45~49.9~25.35~13.45~5.3~3.2~1.35~0.4','0.55~1.5~2~6~8.1~18.35~51.65~71.9~91.75~110.15~131.05','940.2~959.35~978.5~997.7~1016.85~1036~1055.15~1074.3~1093.5~1112.65~1131.8',571.95,'1092.25~1103.45~1086.40~1107.90~1085.00~1118.00~1094.00~1067.25~1064.50~1072.95~1064.10~1074.25~1076.50~1064.00~1051.50','1075.85~1055.15~1078.55~1093.30~1072.00~1051.10~1074.60~1058.00~1054.00~1057.30~1057.00~1060.50~1062.40~1038.25~1042.55','6.17~6.2~8.87~9.09~13.04~17.02~19.13~21.25~22.34~23.39~24.59','2010-11-01','14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0','0.05~0.3~0.25~0.7~0.7~1.6~511.7975~530.9475~550.1475~569.2975~588.4475','382.25~401.4~420.55~439.75~458.9~478.05~497.2~516.35~535.55~554.7','-354.25~-357.85~-369.05~-318.75~-315.0~-202.0~98.646~72.95~207.755~109.947~201.1'),(93,143,'RENUKA.NS','RENUKA.NS','0.17498532002349','22.2~18.35~14.5~10.65~6.8~4.85~2.75~2.55~1.4~1.25~0.8','1~1.05~1.55~2.5~3.35~3.75~7.05~11.25~15.3~19.35~23.55','70.75~74.7~78.65~82.65~86.6~90.55~94.5~98.45~102.45~106.4~110.35',16.55,'86.35~85.40~84.95~85.45~82.90~81.80~81.45~81.30~85.25~85.80~87.20~90.45~92.05~92.60~93.75','82.95~84.20~84.20~84.10~80.75~77.70~79.20~79.10~83.90~84.50~85.20~89.20~90.75~89.20~91.65','6.59~7.09~17.74~17.85~20.95~21.21~21.76~22.49~23.54~24.73~24.94','2010-11-01','5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0','55.0275~58.9775~62.9275~66.9275~70.8775~74.8275~78.7775~82.7275~86.7275~90.6775~94.6275','59.2~63.15~67.1~71.1~75.05~79.0~82.95~86.9~90.9~94.85','63.05~63.95~109.4~198.15~272.55~299.8~616.2~866.45~1197.895~567.15~652.1'),(94,144,'RNRL.NS','RNRL.NS','0.061146496815287','4.05~3.5~2.9~2.3~1.75~1.55~1.4~1.25~1.1~1.05~0.8','0.45~1.05~1.1~1.35~1.35~1.5~1.8~2.4~3~3.6~4.2','35.6~36.2~36.8~37.35~37.95~38.55~39.15~39.75~40.3~40.9~41.5',39.35,'40.95~40.85~40.55~41.05~40.30~40.00~40.45~39.50~39.40~39.75~39.70~39.85~39.60~39.00~39.45','40.45~40.25~40.25~40.65~39.90~39.25~39.80~38.90~39.10~39.30~39.35~39.35~39.00~38.05~38.75','5.57~6.21~9.49~13.24~13.8~14.47~15.42~19.81~21.16~22.92~23.88','2010-11-01','5.7175~5.1175~4.5175~3.9675~3.3675~2.7675~2.1675~6.2~4.5~4.5~3.65','0.05~0.1~0.1~0.1~0.1~0.1~0.1~0.47~0.95~1.889~2.525','1.9675~1.9675~1.9675~1.9675~1.9675~1.9675~1.9675~6.6~5.45~6.05~5.8','3.8~3.25~2.65~2.1~1.5~0.9~0.3~0.070000000000001~4.2188474935756E-15~0.339~0.375'),(95,145,'ROLTA.NS','ROLTA.NS','0.062833432128038','17.85~15.25~12.6~10.2~7.7~4.6~3.1~2.2~1.7~1.3~0.9','0.85~1.05~1.6~2.3~3.45~4.9~7.7~10.65~13.3~16.15~18.7','154.15~156.8~159.4~162.05~164.65~167.3~169.95~172.55~175.2~177.8~180.45',116.15,'171.00~173.00~170.50~175.00~174.00~171.00~170.00~166.90~164.95~167.90~167.80~173.90~173.70~169.50~169.80','169.30~170.70~169.15~171.90~173.50~166.30~167.00~163.55~163.40~164.65~164.30~169.55~172.10~165.55~167.00','5.03~5.75~6.18~6.31~11.7~17.09~17.25~20.13~21.74~23.54~23.91','2010-11-01','9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0','0.1~0.1~0.1~0.1~0.15~0.15~0.2~1.8~4.6195~7.3~9.6','47.0~49.65~52.25~54.9~57.5~60.15~62.8~65.4~68.05~70.65','-29.0~-28.9~-26.75~-24.0~-20.3~-11.55~-11.15~-22.95~-15.73~-10.6~-10.4'),(96,147,'RPOWER.NS','RPOWER.NS','0.051282051282051','14.4~12.4~10.5~8.8~6.7~4~3.4~2.3~1.65~1.1~1','0.3~1.2~1.85~1.9~3.4~4.6~6.85~9.05~11~12.85~14.95','147~149~151~153~155.05~157.05~159.05~161.1~163.1~165.1~167.1',71.05,'166.90~165.70~164.80~167.10~164.00~161.45~162.60~159.00~158.90~161.25~161.60~161.40~160.40~157.80~158.70','162.50~163.90~163.50~165.45~162.75~159.40~160.80~157.60~157.65~159.20~160.10~159.50~159.00~155.80~156.70','6.53~7.63~8.86~9.33~10.82~13.89~17.01~20.62~20.77~23.3~24.91','2010-11-01','5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0','0.1~0.35~0.65~0.65~1~2.05~91.5525~93.6025~95.6025~97.6025~99.6025','80.95~82.95~84.95~86.95~89.0~91.0~93.0~95.05~97.05~99.05','-66.65~-43.25~-32.1~-33.2~-12.0~-5.5~7.25~19.5~35.1~35.65~35.25'),(97,148,'SAIL.NS','SAIL.NS','0.15212474705392','42.05~35~27.7~20.75~13.65~7.85~4.2~3~1.8~1.25~0.6','0.45~1.2~1.95~3.4~5.75~6.7~13.95~21.45~29~37~44.25','157.25~164.6~172~179.4~186.75~194.15~201.55~208.9~216.3~223.7~231.05',76.2,'226.70~227.25~223.50~230.00~226.00~221.00~225.30~219.60~218.70~222.75~221.90~221.55~219.00~201.70~198.00','221.60~221.70~222.00~226.75~223.05~215.00~219.00~217.00~215.50~219.20~219.80~219.15~214.40~194.05~195.10','5.2~5.66~6.69~9.67~13.41~14.28~17.47~19.45~19.46~20.25~22.69','2010-11-01','5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0','0.75~1.65~2.15~106.15~114.36~121.76~129.16~136.51~143.91~151.31~158.66','86.05~93.4~100.8~108.2~115.55~122.95~130.35~137.7~145.1~152.5','-64.6~-49.4~-35.35~2.95~50.6~35.3~179.85~211.247~190.697~214.347~246.097'),(98,150,'SCI.NS','SCI.NS','0.05121107266436','15.95~13.65~11.65~9.45~7.4~4.15~3~2.2~1.95~1.35~0.9','0.65~1.15~1.6~2.7~3.7~4.9~7.45~9.9~12.3~14.15~16.85','162~164.25~166.45~168.65~170.9~173.1~175.3~177.55~179.75~181.95~184.2',57.4,'187.90~185.40~190.85~189.25~185.25~180.65~180.00~182.00~181.90~182.45~180.90~183.85~182.35~178.65~177.40','184.55~182.00~187.85~186.40~182.70~176.30~177.35~177.45~179.15~179.35~179.15~181.50~180.10~176.00~175.00','5.69~10.84~10.91~12.4~14.95~15.48~17.03~18.26~22.21~22.45~23.35','2010-11-01','5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0','0.75~1.5~2.2~2.45~116.37~118.57~120.77~123.02~125.22~127.42~129.67','109.6~111.85~114.05~116.25~118.5~120.7~122.9~125.15~127.35~129.55','-72.4~-51.85~-33.2~-6.65~20.4~45.1~118.5~142.9~166.4~205.35~83.85'),(99,151,'SESAGOA.NS','SESAGOA.NS','0.1865070884092','83~68.85~54.5~40~24.9~10.55~7.15~2.95~2.45~1.7~0.95','0.2~1.2~2.1~3.2~7.4~8.85~25.85~41.5~57.35~72.55~88.2','246.6~261.6~276.6~291.6~306.6~321.6~336.6~351.6~366.6~381.6~396.6',328.05,'359.50~357.45~354.60~366.45~383.65~379.00~380.85~357.65~351.20~354.00~350.80~338.90~329.00~325.00~329.30','352.20~351.00~351.60~357.75~374.15~372.30~369.10~352.15~346.65~346.30~345.35~334.50~325.50~318.20~325.00','9.55~10.56~11.08~11.89~11.92~12.71~14.11~14.3~14.91~15.63~16.91','2010-11-01','96.55~81.75~67.1~52.45~37.8525~22.8525~33.236~22.618~19.553~17.875~14.207','0.05~0.1~0.15~0.2~0.35~0.4~13.3~24.4525~39.5025~55.4025~71.1525','15.1~15.3~15.65~16~16.4025~16.4025~41.786~46.168~58.103~71.425~82.757','81.5~66.55~51.6~36.65~21.8~6.85~4.75~0.90249999999999~0.95249999999999~1.8525~2.6025'),(100,152,'SIEMENS.NS','SIEMENS.NS','0.064330879433057','88~76.3~64.1~50.55~36.95~14.55~8.75~5.95~1.95~1.55~0.5','0.4~1.8~2.05~5.7~11.3~14.85~39.45~52.15~66.1~78.8~92','751.8~764.95~778.1~791.25~804.4~817.55~830.7~843.85~857~870.15~883.3',820,'827.80~825.00~826.50~843.80~831.25~824.90~819.00~799.95~809.80~809.90~842.00~844.75~834.10~819.90~830.00','818.20~816.00~815.00~834.10~825.00~808.10~810.05~792.10~802.00~803.05~830.25~834.35~820.00~811.10~821.20','5.63~8.63~8.96~10.92~14.64~14.86~15.44~17.52~19.97~20.52~23.54','2010-11-01','110.19~92.31~82.16~69.59~56.73~48.73~39.915~39.915~39.915~27.17','14.0~14.0~14.0~14.0~14.0~14.0~19.55~31.05~44.15~56.55~66.85','41.99~37.26~40.26~40.84~41.13~46.28~50.615~63.765~76.915~77.32','82.2~69.05~55.9~42.75~29.6~16.45~8.85~7.2~7.15~6.4~3.55'),(101,153,'SINTEX.NS','SINTEX.NS','0.75123018042646','192.25~155.65~119.2~82.6~46.05~14.1~10.1~3.85~2.1~1.55~0.4','0.35~1.55~2.55~5.3~10.7~24.4~49.4~89.55~130.15~169.95~210.3','12.45~50.8~89.15~127.45~165.8~204.15~242.5~280.85~319.15~357.5~395.85',89.85,'434.00~437.30~427.80~436.70~429.80~424.00~419.90~430.75~440.30~440.00~439.40~430.00~212.95~206.65~208.85','427.00~431.15~417.25~423.00~425.10~419.05~414.50~414.00~432.15~435.05~428.45~421.95~202.00~199.85~204.50','5.61~6.02~9.24~10.52~10.59~11.64~12.1~19.25~19.42~21.4~23.17','2010-11-01','82.9425~42.2915~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0','11.05~44.95~67.1~42.0925~80.4425~118.7925~157.1425~195.4925~233.7925~272.1425~310.4925','-72.4~-34.05~4.3~42.6~80.95~119.3~157.65~196.0~234.3~272.65','88.45~84.0~67.8~100.6~99.4~270.3~587.002~1157.252~1507.703~1266.302~1477.003'),(102,154,'STER.NS','STER.NS','0.085920884729902','22.65~19.4~15.55~12.45~8.85~4.55~3.95~2.75~1.75~1.1~0.25','0.6~1.2~1.95~2.35~3.8~6.1~8.85~12.75~16.4~20.15~23.75','150.55~154.15~157.8~161.4~165.05~168.65~172.25~175.9~179.5~183.15~186.75',170.85,'178.20~183.00~180.65~185.30~183.90~179.85~180.30~174.45~173.00~172.40~172.10~173.70~172.45~172.00~172.50','175.00~179.00~179.00~183.55~182.20~177.70~178.50~171.25~171.10~170.00~170.20~172.10~170.10~168.75~170.60','8.47~9.58~11.55~14.77~18.28~19.05~20.84~20.95~21.49~23.53~23.89','2010-11-01','27.175~23.305~19.705~16.105~12.955~9.955~11.8275~53.6~35.5~29.9~19.45','0.4~0.1~0.4~0.75~0.8~2.15~4.58~8.5~12.295~15.945~19.295','6.875~6.605~6.655~6.655~7.155~7.755~13.2275~58.65~44.15~42.2~35.35','20.7~16.8~13.45~10.2~6.6~4.35~3.18~3.45~3.645~3.645~3.395'),(103,156,'SUNPHARMA.NS','SUNPHARMA.NS','0.06758490068448','236~201.15~169.2~131.4~100.2~22.85~15.8~6.15~4.5~1.8~0.85','0.4~1.7~3.7~8.85~20.75~27.9~103.75~139~177.45~209.65~247.9','1931.8~1967.45~2003.1~2038.75~2074.4~2110.05~2145.7~2181.35~2217~2252.65~2288.3',808.6,'2099.00~2099.80~2117.8501~2124.75~2095.05~2099.00~2067.00~2067.00~2047.75~2125.00~2156.00~2147.00~2132.00~2130.00~2174.00','2069.00~2052.00~2098.00~2109.00~2046.20~2018.00~2021.10~2015.05~2016.40~2105.1499~2130.05~2120.00~2105.55~2096.00~2152.00','9.4~12.7~12.72~15.72~15.75~16.4~17.33~19.11~19.55~19.68~21.5','2010-11-01','14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0','0.1~0.2~0.4~0.85~1.75~2.25~1377.53~1413.18~1448.83~1484.48~1520.13','1137.2~1172.85~1208.5~1244.15~1279.8~1315.45~1351.1~1386.75~1422.4~1458.05','-1109.2~-1101.45~-1070.45~-940.25~-649.4~-517.1~1463.75~2372.55~2482.6~3194.4~2602.55'),(104,157,'SUNTV.NS','SUNTV.NS','0.062992125984252','53~45.5~38~30.75~23.2~9.25~4.95~3.7~1.9~1.15~0.55','0.6~1.6~2.2~4.25~7.7~8.85~23.75~31.15~39.25~48.2~56.05','460~467.85~475.7~483.55~491.45~499.3~507.15~515.05~522.9~530.75~538.6',314.35,'540.00~532.00~525.00~527.55~517.00~512.70~514.80~513.60~510.00~514.00~524.00~519.60~517.80~502.00~507.00','528.10~520.90~520.00~520.20~507.05~503.05~508.05~506.25~505.00~507.95~519.20~514.05~512.95~492.00~500.10','6.16~6.32~7.78~9.33~15.38~17.14~19.17~19.3~19.98~20.87~21.16','2010-11-01','9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0','0.05~0.1~0.1~0.15~0.25~0.75~208.5175~16.4~23.65~32.6~239.9675','154.65~162.5~170.35~178.2~186.1~193.95~201.8~209.7~217.55~225.4','-136.65~-133.45~-135.0~-120.0~-88.6~-93.55~30.9~-10.6~-2.35~-1.7~20.7'),(105,158,'SUZLON.NS','SUZLON.NS','0.069319640564827','6.35~5.5~4.5~3.55~2.65~1.95~1.7~1.5~1.25~1.05~0.4','0.25~1~1.25~1.5~1.65~2.15~2.7~3.6~4.7~5.75~6.6','50.65~51.6~52.55~53.55~54.5~55.45~56.4~57.35~58.35~59.3~60.25',12.65,'60.30~59.65~59.00~59.80~59.15~58.45~60.45~58.80~58.15~59.50~60.20~59.00~58.20~57.50~56.00','57.80~58.60~57.95~58.80~58.55~57.25~59.50~57.60~57.55~58.70~59.35~58.35~57.15~56.40~55.10','9.64~10.29~11.64~12.62~14.37~14.47~15.95~17.84~17.93~19.21~22.5','2010-11-01','5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0','0.1~39.5825~40.5325~41.5325~42.4825~43.4325~44.3825~45.3325~46.3325~47.2825~48.2325','43.0~43.95~44.9~45.9~46.85~47.8~48.75~49.7~50.7~51.65','-17.55~30.75~41.45~56.2~53.65~70.1~97.0~120.1~152.75~169.05~184.81'),(106,159,'SYNDIBANK.NS','SYNDIBANK.NS','0.116711833785','24.1~20.2~16.15~12.2~8.4~5.55~2.9~2.55~2~1.3~0.55','0.5~1.4~1.9~2.4~3.75~5.85~8.55~12.65~16.85~21.15~25.25','118.55~122.6~126.65~130.7~134.75~138.8~142.85~146.9~150.95~155~159.05',116,'130.90~131.40~131.90~139.00~137.65~135.20~133.65~131.95~133.85~138.85~141.65~142.30~146.45~139.90~141.50','129.95~129.20~130.10~135.60~135.10~131.10~132.05~130.30~132.55~136.50~137.60~140.30~142.05~134.05~139.30','5.93~9.24~12.53~13.09~13.47~14.84~17.77~18.89~19.23~21.08~23.11','2010-11-01','9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0','8.35~9.0~0.55~0.55~0.8~1~28.321~36.7~40.75~44.8~48.85','11.55~15.6~19.65~23.7~27.75~31.8~35.85~39.9~43.95~48.0','6.45~2.4~-1.65~-4.0~-3.0~-2.35~1.471~12.95~23.55~20.2~11.396'),(107,160,'TATACHEM.NS','TATACHEM.NS','0.064620917686641','42.9~36.5~29.9~24.5~18.3~9.1~5.45~3.9~1.75~1.15~0.45','0.9~1.1~1.85~3.85~6~8.6~18.4~24.95~31.4~37.75~44.45','358.9~365.2~371.55~377.85~384.15~390.45~396.75~403.05~409.35~415.7~422',388.25,'414.60~426.00~424.70~425.50~438.80~435.00~430.50~424.95~433.60~435.00~432.50~433.50~444.90~430.60~398.00','412.60~416.95~421.00~419.25~431.50~429.20~424.50~417.05~425.00~431.15~429.75~430.30~440.30~420.45~383.30','7.53~8.92~9.87~14.16~14.69~15.36~18.33~19.43~20.99~23.75~24.84','2010-11-01','42.047~35.419~28.289~23.75~18.45~9.0~9.0~9.0~9.0~9.0','9.0~9.0~9.0~13.0~15.02~20.38~19.98~22.48~30.94~36.01~42.23','12.697~12.369~11.589~13.35~14.35~11.2~17.5~23.8~30.1~36.45','38.35~32.05~25.7~23.4~19.12~18.18~11.48~7.68~9.84~8.56~8.48'),(108,161,'TATACOMM.NS','TATACOMM.NS','0.09609984399376','45.5~37.9~31.1~23.85~16.45~8.5~6.35~3.75~2~1.55~0.7','0.25~1.1~2.05~3.6~5.8~10.7~17.2~25~32.05~39.6~47.35','269.45~276.8~284.2~291.55~298.9~306.25~313.6~320.95~328.3~335.7~343.05',383.2,'337.70~334.15~333.45~341.00~335.90~335.00~332.90~320.95~322.85~323.50~322.50~322.60~317.00~312.00~312.00','330.60~328.30~330.25~332.05~330.00~327.05~328.20~314.70~314.80~318.00~318.05~317.05~312.10~305.10~308.55','6.54~8.11~8.68~10.36~13.11~14.51~14.7~14.95~17.07~18.09~23.39','2010-11-01','124.407~116.616~108.772~100.981~93.19~85.399~77.608~69.817~62.026~54.182','9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0','-5.388~-3.587~-10.088~-14.337~-20.997~-39.083~-28.2~-36.05~-35.275~-26.375','122.75~115.4~108.0~100.65~93.3~85.95~78.6~71.25~63.9~56.5~49.15'),(109,163,'TATAPOWER.NS','TATAPOWER.NS','0.04763597582652','121.8~108.15~90.6~73.8~57.2~25.45~7.7~6.8~2.2~1.9~0.2','0.3~1.8~2.65~5.1~10.65~18.2~60.5~76.45~92.25~110.35~127.7','1314.15~1330.8~1347.45~1364.05~1380.7~1397.35~1414~1430.65~1447.25~1463.9~1480.55',85.6,'1438.80~1424.00~1432.40~1440.00~1435.70~1433.45~1405.00~1397.80~1391.00~1428.70~1440.00~1421.30~1412.00~1425.75~1419.90','1414.00~1408.00~1411.95~1425.00~1426.10~1392.00~1382.45~1373.00~1373.00~1416.60~1412.00~1406.20~1400.15~1389.35~1397.00','5.67~5.77~5.96~6.97~9.05~11.37~12.13~13.71~21.1~24.38~24.82','2010-11-01','5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0','0.55~2.7~3.65~4.55~1299.38~1316.03~1332.68~1349.33~1365.93~1382.58~1399.23','1233.55~1250.2~1266.85~1283.45~1300.1~1316.75~1333.4~1350.05~1366.65~1383.3','-1113.25~-566.25~-396.6~-196.45~703.4~1890.1~7160.53~6924.589~7182.735~8531.484~9879.983'),(110,164,'TATASTEEL.NS','TATASTEEL.NS','0.10364928531502','92.5~77.6~62.85~48.05~33.45~18.05~6.25~3.85~3.1~1.2~0.85','0.85~1.25~2.55~3.65~6.65~18.95~34.2~50.15~65.6~81.1~97.45','513.05~528.3~543.6~558.85~574.15~589.4~604.65~619.95~635.2~650.5~665.75',447.75,'644.75~642.00~639.30~653.25~658.60~652.90~652.55~629.50~626.80~633.00~625.90~624.95~608.85~605.45~602.95','631.25~630.00~633.60~640.65~645.60~630.00~645.10~622.65~617.85~624.10~621.00~620.70~601.90~593.70~595.00','5.8~8.84~9.08~10.58~12.35~14.96~15.37~16.13~18.71~23.33~24.65','2010-11-01','9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0','1.5~1.8~2.7~3.6~6.65~17.3~167.4~194.5875~209.8375~225.1375~240.3875','74.3~89.55~104.85~120.1~135.4~150.65~165.9~181.2~196.45~211.75','-56.3~-69.35~-77.8~-88.55~-86.85~-33.1~10.5~51.75~72.755~110.855~63.205'),(111,166,'TCS.NS','TCS.NS','0.13296606986353','202.75~169.5~135.35~100.85~67.5~30.15~12.2~7.25~4.05~2~0.85','1~1.3~3.65~5.15~14.8~22.75~69.85~105.85~142.95~178~214.5','877.9~912.9~947.9~982.9~1017.9~1052.9~1087.9~1122.9~1157.9~1192.9~1227.9',2694.9,'953.95~955.80~958.00~1004.00~1010.00~970.00~987.40~980.50~979.95~1041.40~1057.00~1064.75~1062.95~1060.00~1066.40','929.05~939.10~949.10~995.00~968.10~932.00~968.50~963.20~970.00~1022.00~1037.20~1046.10~1051.10~1047.50~1052.05','6.87~7.77~9.45~11.98~16.07~16.13~16.92~17.64~18.33~19.11~22.22','2010-11-01','1952.969~1915.869~1878.769~1841.669~1804.569~1767.469~1730.369~1693.269~1656.169~1619.069','14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0','-774.27~-818.07~-840.17~-881.12~-952.635~-1300.917~-1391.116~-1434.84~-1471.15~-1463.1','1831.0~1796.0~1761.0~1726.0~1691.0~1656.0~1621.0~1586.0~1551.0~1516.0~1481.0'),(112,167,'TECHM.NS','TECHM.NS','0.1334770018183','141.35~118.35~94.75~71~46.6~17.8~12.6~4.95~2.6~1.2~0.25','0.9~1.6~3.35~4.75~7.65~16.4~49.25~73.45~98.55~124.25~149.75','607.6~631.9~656.25~680.6~704.9~729.25~753.6~777.9~802.25~826.6~850.9',2368.85,'774.00~776.40~779.00~797.70~782.35~765.00~762.40~766.00~759.80~764.00~767.70~789.45~792.00~758.10~742.00','767.05~760.30~773.15~788.10~772.00~730.00~751.30~692.90~753.00~755.55~763.50~769.95~764.10~745.10~731.00','5.32~5.89~11.76~11.77~12.26~13.53~15.39~16.53~17.93~20.92~23.09','2010-11-01','1890.6135~1864.8555~1839.0445~1813.2335~1787.4755~1761.6645~1735.8535~1710.0955~1684.2845~1658.4735','14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0','-1089.757~-1126.407~-846.613~-1033.062~-1211.058~-1433.336~-1445.937~-1499.875~-1497.305~-1484.09','1775.25~1750.95~1726.6~1702.25~1677.95~1653.6~1629.25~1604.95~1580.6~1556.25~1531.95'),(113,169,'TRIVENI.NS','TRIVENI.NS','0.071693448702101','14~12~10~7.75~5.85~3.85~2.7~2.1~1.75~1.35~0.65','0.9~1.2~1.8~1.9~2.85~3.65~6~8.2~10.4~12.45~14.85','109.05~111.2~113.35~115.5~117.65~119.8~121.95~124.1~126.25~128.4~130.55',22,'125.40~125.35~128.00~126.45~123.90~122.85~125.70~120.00~119.45~119.00~118.85~118.50~124.10~120.50~122.50','122.45~124.00~126.00~124.15~121.45~120.50~122.20~117.00~117.60~117.65~117.55~117.30~120.80~118.00~120.40','5.58~7.72~14.61~14.76~15.98~16.27~18.43~23.06~24.1~24.39~24.92','2010-11-01','5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0','88.15~90.3~92.45~94.6~96.75~98.9~101.05~103.2~105.35~107.5~109.65','92.05~94.2~96.35~98.5~100.65~102.8~104.95~107.1~109.25~111.4','15.45~44.65~106.9~106.7~144.45~173.5~338.2~452.9~594.1~340.724~279.683'),(114,170,'TTML.NS','TTML.NS','0.098223615464995','4~4~3.15~2.15~1.65~1.45~1.3~1.2~1.1~1.05~0.55','0.25~1.1~1.1~1.25~1.4~1.55~1.8~2.9~3.35~4.05~6.7','19.75~20.3~20.85~21.4~21.95~22.5~23.05~23.6~24.15~24.7~25.25',9.7,'23.75~23.90~24.30~24.20~24.70~25.10~24.80~24.50~24.15~24.50~24.55~24.55~24.15~23.05~22.85','23.30~23.40~23.65~23.95~24.20~24.60~24.40~24.20~24.00~24.20~24.30~23.50~23.85~22.75~22.50','5.29~5.79~8.02~12.28~14.12~14.61~15.66~17.8~23.13~24.59~24.88','2010-11-01','5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0','0.15~11.085~11.635~12.185~12.735~13.285~13.835~14.385~14.935~15.485~16.035','15.05~15.6~16.15~16.7~17.25~17.8~18.35~18.9~19.45~20.0','-1.55~25.95~23.25~18.4~17.4~17.8~21.225~34.975~23.225~18.575~34.775'),(115,171,'TULIP.NS','TULIP.NS','0.13555227486243','35.05~29.15~23.2~17.4~11.6~5.7~3.9~3.15~1.75~1.35~0.4','0.4~1.35~1.95~3.4~3.5~6.35~12~18.35~24.4~30.55~36.95','148.2~154.25~160.3~166.35~172.4~178.45~184.5~190.55~196.6~202.65~208.7',3.95,'184.90~179.35~182.00~184.00~181.00~179.00~178.60~176.40~182.00~191.80~198.90~193.05~190.65~181.00~180.35','181.25~174.25~176.65~178.30~178.25~176.10~176.55~173.65~174.50~187.50~194.50~190.05~184.75~175.10~178.40','9.39~9.85~10.78~13.79~14.46~20.5~20.97~21.39~22.35~23.12~24.73','2010-11-01','5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0','144.4475~150.4975~156.5475~162.5975~168.6475~174.6975~180.7475~186.7975~192.8475~198.8975~204.9475','149.25~155.3~161.35~167.4~173.45~179.5~185.55~191.6~197.65~203.7','297.35~1241.55~1787.5~3081.5~3105.3~5631.65~7562.25~11108.69~11553.44~13241.74~15293.24'),(116,172,'TV-18.NS','TV-18.NS','0.068371157713301','9.8~8.35~6.95~5.45~4~2.85~2.4~1.75~1.3~1.1~0.3','0.85~1.25~1.5~2.05~2.25~2.9~4.15~5.75~7.25~8.8~10.2','79.45~80.9~82.4~83.9~85.35~86.85~88.35~89.8~91.3~92.8~94.25',86.75,'92.90~92.35~90.60~91.35~90.00~86.50~85.90~85.40~85.20~86.35~85.90~88.00~89.10~87.75~88.25','91.50~91.20~90.10~89.30~88.55~85.05~85.00~84.05~84.60~85.05~84.20~86.10~88.00~87.00~87.05','6.94~8.38~9.69~10.83~13.88~14.3~14.7~18.02~23.45~24.31~24.75','2010-11-01','10.1625~8.7125~7.2125~5.875~3.4~2.05~2.55~1.7~2.25~1.65~1.1','0.65~0.85~0.9~0.9~1~1.35~3~4.3~6.1~8.1~9.05','2.8625~2.8625~2.8625~3.025~2~2.15~4.15~4.75~6.8~7.7~8.6','7.95~6.7~5.25~3.75~2.4~1.25~1.4~1.25~1.55~2.05~1.55'),(117,173,'UCOBANK.NS','UCOBANK.NS','0.16650342801175','29.3~24.35~19.4~14.1~9.1~5.35~4.35~2.3~2.05~1.15~0.75','0.35~1.4~1.9~2.8~3.4~5.3~9.45~14.95~20.25~25.85~31','99.4~104.6~109.85~115.05~120.3~125.5~130.7~135.95~141.15~146.4~151.6',80.2,'124.10~124.00~122.85~127.25~124.20~121.90~121.40~118.60~120.95~126.50~130.45~132.25~138.25~130.45~128.90','122.50~122.25~121.35~124.80~122.50~118.35~119.30~117.00~119.05~124.65~126.90~130.05~133.85~126.65~125.00','5.93~6.51~7.08~7.18~11.43~11.75~15.21~17.62~17.71~20.89~23.94','2010-11-01','5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0','0.5~1.15~1.4~35.75~2.3~2.75~54.51~59.76~64.96~70.21~75.41','24.2~29.4~34.65~39.85~45.1~50.3~55.5~60.75~65.95~71.2','-13.0~-3.3~-5.3~0.9~-2.5~-0.05~27.8~24.142~48.95~66.5~77.215'),(118,175,'UNIONBANK.NS','UNIONBANK.NS','0.13510464970548','73.7~61.8~49.2~36.75~24.65~9.35~6~3.1~2.5~1.45~0.55','0.95~1~2.9~3.8~6.3~12.05~25.4~38.7~52~64.45~78.25','314.1~326.9~339.65~352.4~365.2~377.95~390.7~403.5~416.25~429~441.8',211.5,'393.00~390.35~389.90~402.85~398.95~395.80~404.40~404.00~400.00~407.00~419.00~415.40~425.90~380.00~384.90','385.25~383.95~385.65~397.50~390.50~388.30~398.55~399.25~394.00~403.95~415.10~412.10~417.50~372.00~374.00','6.09~6.39~7.35~10.14~10.52~11.53~13.27~14.85~16.01~20.79~23.37','2010-11-01','9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0','0.75~0.85~1.65~2.25~4.1~8.85~189.775~202.575~215.325~228.075~240.875','111.6~124.4~137.15~149.9~162.7~175.45~188.2~201.0~213.75~226.5','-83.95~-97.8~-89.1~-93.15~-82.25~-45.65~55.05~153.0~145.9~168.9~222.15'),(119,176,'UNIPHOS.NS','UNIPHOS.NS','0.15445349124106','43.9~36.15~29~21.5~13.75~7.45~4.55~2.75~2.35~1.3~0.95','0.35~1.1~2.25~3.05~5.45~7.45~14.4~22.6~30.5~38.35~46.1','160.8~168.5~176.2~183.9~191.6~199.3~207~214.7~222.4~230.1~237.8',204.25,'184.55~187.45~194.80~193.00~199.25~191.80~193.65~193.80~195.00~209.40~215.25~218.30~204.90~208.00~205.00','182.45~184.80~192.10~190.50~193.60~187.70~189.10~187.00~189.80~206.50~210.00~213.80~198.10~198.55~202.00','6.46~7.06~9.62~10.63~10.93~12.21~12.82~14.71~18.68~18.8~19.56','2010-11-01','44.0725~36.3225~28.9725~21.1725~15.5~9.95~16.66~10.36~12.18~9.145~10.7725','0.05~0.8~2.1~2.45~3~4~12.9625~20.4675~27.9175~35.7175~43.3175','0.62250000000001~0.5725~0.92249999999999~0.82250000000001~2.85~5~19.41~20.81~30.33~34.995~44.3225','43.5~36.55~30.15~22.8~15.65~8.95~10.2125~10.0175~9.7675~9.8675~9.7675'),(120,177,'UNITECH.NS','UNITECH.NS','0.12172955108785','15.5~13.05~10.35~7.85~5.4~3.2~2.9~1.9~1.6~1.2~0.65','0.4~1~1.45~2.25~2.7~3.2~5.5~8.2~10.9~13.55~16.45','73.6~76.25~78.9~81.5~84.15~86.8~89.45~92.1~94.7~97.35~100',18.35,'101.00~95.50~93.60~95.95~96.30~95.50~96.00~91.75~89.50~91.65~90.10~90.55~90.30~86.80~89.00','94.55~93.60~92.55~94.90~94.15~92.90~94.55~88.20~88.50~90.05~89.10~89.30~89.05~85.25~88.10','7.68~13.65~14.22~15.68~15.71~16.72~17.32~18.16~19.2~21.53~21.74','2010-11-01','5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0~5.0','0.05~58.8175~61.4675~64.0675~66.7175~69.3675~72.0175~74.6675~77.2675~79.9175~82.5675','60.25~62.9~65.55~68.15~70.8~73.45~76.1~78.75~81.35~84.0','-21.6~25.35~47.05~95.0~115.15~138.55~264.3~426.3~528.9~644.5~417.9'),(121,179,'VOLTAS.NS','VOLTAS.NS','0.074730842305256','29.8~25.55~20.75~16.4~11.8~5.35~5.05~3.35~1.9~1.1~0.35','1~1.25~2.25~2.85~3.65~6~12.4~17~21.85~26.6~31.05','222.3~226.9~231.45~236.05~240.6~245.2~249.8~254.35~258.95~263.5~268.1',235.3,'242.55~245.90~241.80~248.80~241.15~237.50~240.85~235.40~233.70~240.05~241.00~245.70~242.50~244.80~248.05','240.75~239.50~239.60~246.75~235.15~228.00~236.00~233.10~231.50~236.10~235.20~242.25~234.35~240.00~245.55','5.31~7.73~7.94~8.44~11.42~12.85~16.43~18.19~18.55~18.65~20.37','2010-11-01','24.525~17.793~15.175~11.725~9.0~9.0~9.0~9.0~9.0~9.0','9.0~9.0~9.0~9.0~9.0~10.5~20.85~25.55~31.8~38.6~40.4','11.525~9.393~11.325~12.475~14.3~18.9~23.5~28.05~32.65~37.2','22.0~17.4~12.85~8.25~3.7~0.6~6.35~6.5~8.15~10.4~7.6'),(122,181,'WIPRO.NS','WIPRO.NS','0.16283473086286','96.75~79.6~63.75~47~30.05~11.05~7.8~4.7~2.15~1.15~0.5','0.4~1.15~2.85~4.6~9.45~12.65~31.55~49.2~67.1~84.55~102.15','334.3~351.4~368.5~385.6~402.65~419.75~436.85~453.9~471~488.1~505.2',587,'468.65~472.00~468.00~492.45~499.75~475.00~474.50~469.00~470.70~463.70~450.00~434.70~435.75~430.00~428.00','460.60~461.05~462.00~483.35~478.20~464.35~463.35~460.00~464.00~450.15~434.40~425.70~432.25~424.50~423.10','10.6~10.93~11.22~11.94~13.59~15.32~15.6~16.21~17.38~18.5~23.28','2010-11-01','394.1~331.995~272.945~210.795~201.281~183.155~165.029~146.956~128.83~110.704','14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0','141.4~96.395~54.445~9.395~-31.203~-99.965~-96.35~-94.27~-99.093~-84.9','266.7~249.6~232.5~215.4~198.35~181.25~164.15~147.1~130.0~112.9~95.8'),(123,182,'YESBANK.NS','YESBANK.NS','0.10409745293466','56.5~47.7~38.6~29.3~20.1~10.3~6.75~3.15~2.55~1.45~0.65','0.75~1.2~1.8~4.55~7.25~11.75~21.2~30.55~40.4~50.35~60.05','312.7~322.1~331.45~340.8~350.15~359.5~368.85~378.2~387.55~396.9~406.3',592.85,'354.20~354.95~351.60~355.90~351.10~349.00~358.40~353.30~356.50~362.95~372.00~374.85~380.00~361.00~365.40','351.10~332.70~347.60~352.50~346.90~342.40~352.70~347.35~352.35~356.50~368.55~369.65~376.05~354.70~359.50','10.01~10.51~10.69~11.66~12.32~13.16~15.28~16.95~20.97~21.08~21.45','2010-11-01','302.8875~292.9235~283.0125~273.1015~263.1905~253.2795~243.3685~233.4575~223.5465~213.6355','14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0~14.0','-1.648~-24.865~-57.3~-82.75~-116.6~-157.3~-166.905~-170.15~-160.81~-162.475','294.15~284.75~275.4~266.05~256.7~247.35~238.0~228.65~219.3~209.95~200.55'),(124,183,'ZEEL.NS','ZEEL.NS','0.068151328145962','31.45~26.7~22.05~17.6~13.15~6.05~3.85~3.1~1.95~1.25~0.8','0.45~1.35~1.75~2.55~4.45~7.6~13.1~18.35~22.95~28.05~32.8','252.65~257.4~262.1~266.8~271.5~276.2~280.9~285.6~290.3~295~299.75',318,'297.40~292.65~283.05~283.40~279.30~275.60~277.70~283.20~282.90~289.05~286.85~285.50~281.70~281.85~281.00','294.50~288.20~272.80~277.05~273.70~270.00~275.50~278.20~279.60~284.50~283.50~283.50~278.00~277.25~277.30','9.22~9.42~9.51~9.96~10.64~11.56~14.43~16.1~17.05~18.46~18.98','2010-11-01','76.62~67.82~58.67~57.452~52.47~47.488~42.506~37.524~32.542~27.56','9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0~9.0','11.27~7.22~2.77~-0.982~-4.683~-13.7~-15.6~-11.508~-11.25~-12.25','74.35~69.6~64.9~60.2~55.5~50.8~46.1~41.4~36.7~32.0~27.25');

UNLOCK TABLES;

/*Table structure for table `options_opened_or_closed` */

DROP TABLE IF EXISTS `options_opened_or_closed`;

CREATE TABLE `options_opened_or_closed` (
  `transactionId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` varchar(65) NOT NULL,
  `name` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `expiry` varchar(25) NOT NULL,
  `longORshort` varchar(25) NOT NULL,
  `callORput` varchar(25) NOT NULL,
  `margin` double NOT NULL,
  `strikePrice` double NOT NULL,
  `underlyingPrice` double NOT NULL,
  `premium` double NOT NULL,
  `profit` double NOT NULL,
  `volatility` double NOT NULL,
  `status` double NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`transactionId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `options_opened_or_closed` */

LOCK TABLES `options_opened_or_closed` WRITE;

insert  into `options_opened_or_closed`(`transactionId`,`userId`,`name`,`quantity`,`expiry`,`longORshort`,`callORput`,`margin`,`strikePrice`,`underlyingPrice`,`premium`,`profit`,`volatility`,`status`,`date`) values (1,'nilmadhab','nil',12,'12','22','22',22,22,22,323,222,22,1,'0000-00-00 00:00:00'),(2,'nilmadhab','COLPAL.NS',1,'Jan 21 2011','Long','Call',0,822.85,883.1,104.405,0,0.052059346489055,1,'0000-00-00 00:00:00'),(3,'nilmadhab','ASHOKLEY.NS',10,'Jan 21 2011','Long','Call',0,67.25,76.1,126.55,0,0.090006618133686,0,'0000-00-00 00:00:00'),(4,'nilmadhab','BIOCON.NS',1,'Jan 21 2011','Long','Put',0,326.1,428.6,0.1,0,0.16629419517506,1,'0000-00-00 00:00:00'),(5,'nilmadhab','CANBK.NS',3,'Jan 21 2011','Short','Put',856.35,570.9,742.05,0.75,0,0.16697750959956,1,'0000-00-00 00:00:00'),(6,'nilmadhab','CAIRN.NS',2,'Jan 21 2011','Long','Call',0,320.9,323.85,31.415,0,0.059638554216867,1,'0000-00-00 00:00:00'),(7,'nilmadhab','ASHOKLEY.NS',2,'Jan 21 2011','Short','Put',75.8,75.8,76.1,0.3,0,0.090006618133686,1,'0000-00-00 00:00:00'),(8,'nilmadhab','ABAN.NS',1,'Jan 21 2011','Long','Call',0,669.15,796.05,151.375,0,0.11976047904192,0,'0000-00-00 00:00:00'),(9,'nilmadhab','ABAN.NS',12,'Oct 11 2014','Long','Call',0,669.15,796.05,1816.5,0,0.11976047904192,1,'0000-00-00 00:00:00'),(10,'nilmadhab','ABAN.NS',12,'Oct 11 2014','Long','Call',0,669.15,796.05,1816.5,0,0.11976047904192,1,'0000-00-00 00:00:00'),(11,'nilmadhab','ABAN.NS',1,'November 15 2013','Long','Call',0,669.15,611,912.175,0,0.11976047904192,1,'2014-10-18 05:03:18'),(12,'nilmadhab','BHUSANSTL.NS',1,'November 15 2013','Short','Put',255.35,510.7,107.8,408.29,0,0.12001360478111,1,'2014-10-18 05:05:17');

UNLOCK TABLES;

/*Table structure for table `piston_consumer` */

DROP TABLE IF EXISTS `piston_consumer`;

CREATE TABLE `piston_consumer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `key` varchar(18) NOT NULL,
  `secret` varchar(32) NOT NULL,
  `status` varchar(16) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `piston_consumer_6340c63c` (`user_id`),
  CONSTRAINT `user_id_refs_id_4a00bde1` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `piston_consumer` */

LOCK TABLES `piston_consumer` WRITE;

UNLOCK TABLES;

/*Table structure for table `piston_nonce` */

DROP TABLE IF EXISTS `piston_nonce`;

CREATE TABLE `piston_nonce` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_key` varchar(18) NOT NULL,
  `consumer_key` varchar(18) NOT NULL,
  `key` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `piston_nonce` */

LOCK TABLES `piston_nonce` WRITE;

UNLOCK TABLES;

/*Table structure for table `piston_resource` */

DROP TABLE IF EXISTS `piston_resource`;

CREATE TABLE `piston_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `url` longtext NOT NULL,
  `is_readonly` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `piston_resource` */

LOCK TABLES `piston_resource` WRITE;

UNLOCK TABLES;

/*Table structure for table `piston_token` */

DROP TABLE IF EXISTS `piston_token`;

CREATE TABLE `piston_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(18) NOT NULL,
  `secret` varchar(32) NOT NULL,
  `token_type` int(11) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `is_approved` tinyint(1) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `consumer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `piston_token_6340c63c` (`user_id`),
  KEY `piston_token_aa0187c8` (`consumer_id`),
  CONSTRAINT `consumer_id_refs_id_7b010675` FOREIGN KEY (`consumer_id`) REFERENCES `piston_consumer` (`id`),
  CONSTRAINT `user_id_refs_id_31a3844a` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `piston_token` */

LOCK TABLES `piston_token` WRITE;

UNLOCK TABLES;

/*Table structure for table `rediff_link` */

DROP TABLE IF EXISTS `rediff_link`;

CREATE TABLE `rediff_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `EMail` varchar(50) NOT NULL,
  `sent` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rediff_link` */

LOCK TABLES `rediff_link` WRITE;

UNLOCK TABLES;

/*Table structure for table `rediff_links` */

DROP TABLE IF EXISTS `rediff_links`;

CREATE TABLE `rediff_links` (
  `link` varchar(500) NOT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rediff_links` */

LOCK TABLES `rediff_links` WRITE;

UNLOCK TABLES;

/*Table structure for table `status` */

DROP TABLE IF EXISTS `status`;

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `status` */

LOCK TABLES `status` WRITE;

UNLOCK TABLES;

/*Table structure for table `stockprice` */

DROP TABLE IF EXISTS `stockprice`;

CREATE TABLE `stockprice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` double NOT NULL,
  `vary` varchar(100) NOT NULL,
  `series` varchar(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `stockprice` */

LOCK TABLES `stockprice` WRITE;

UNLOCK TABLES;

/*Table structure for table `stocks_rate` */

DROP TABLE IF EXISTS `stocks_rate`;

CREATE TABLE `stocks_rate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock` varchar(100) NOT NULL,
  `compStatName` varchar(25) NOT NULL,
  `price` double NOT NULL,
  `open` double NOT NULL,
  `lastMonday` double NOT NULL,
  `lastFriday` double NOT NULL,
  `diff` double NOT NULL,
  `series` varchar(2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `lastmin` double NOT NULL,
  `diffmin` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=477 DEFAULT CHARSET=latin1;

/*Data for the table `stocks_rate` */

LOCK TABLES `stocks_rate` WRITE;

insert  into `stocks_rate`(`id`,`stock`,`compStatName`,`price`,`open`,`lastMonday`,`lastFriday`,`diff`,`series`,`quantity`,`lastmin`,`diffmin`) values (1,'20MICRONS','20MICRONS',30.4,35.065842097232,54.35,54.35,0,'EQ',63663,35.065842097232,-4.665842097232),(2,'ABAN','ABAN',606.95,261.95,786.95,786.95,0,'EQ',841768,611,-4.04999999999995),(3,'ABB','ABB',1070,644.7,822.2,822.2,0,'EQ',165534,1074.7,-4.70000000000005),(4,'ABGSHIP','ABGSHIP',225.6,285.1,453.95,453.95,0,'EQ',265187,285.1,-59.5),(6,'ACC','ACC',1393,1111.65,985.35,985.35,0,'EQ',1024187,1394.9,-1.90000000000009),(7,'ACE','ACE',31.8,13,60.45,60.45,0,'EQ',91189,32.1,-0.300000000000001),(9,'ADANIENT','ADANIENT',456,209.4,704.45,704.45,0,'EQ',538913,454.85,1.14999999999998),(10,'ADSL','ADSL',22.25,19.2,221.55,221.55,0,'EQ',118856,22.25,0),(11,'AEGISCHEM','AEGISCHEM',346,160.4,329.25,329.25,0,'EQ',962764,346.95,-0.949999999999989),(12,'AFTEK','AFTEK',6.5,3.4,16.4,16.4,0,'EQ',2361799,6.5,0),(13,'AHLUCONT','AHLUCONT',154.5,27.9,175.4,175.4,0,'EQ',222119,155,-0.5),(14,'ALBK','ALBK',103.1,92.7,247.75,247.75,0,'EQ',657733,103.75,-0.650000000000006),(15,'ALCHEM','ALCHEM',43.95,47.2,246.65,246.65,0,'EQ',72426,43.15,0.800000000000004),(16,'ALEMBICLTD','ALEMBICLT',53.15,15.2,74.75,74.75,0,'EQ',138362,53.35,-0.200000000000003),(17,'ALOKTEXT','ALOKTEXT',12.25,8,27.15,27.15,0,'EQ',4891784,12.25,0),(19,'AMBUJACEM','AMBUJACEM',210.35,180.75,140.1,140.1,0,'EQ',3220061,180.75,29.6),(20,'AMTEKINDIA','AMTEKINDI',99.95,68.45,63.4,63.4,0,'EQ',3279774,100.35,-0.399999999999991),(21,'ANANTRAJ','ANANTRAJ',53.65,55.15,129.55,129.55,0,'EQ',412537,53.2,0.449999999999996),(22,'ANDHRABANK','ANDHRABAN',68.6,66.6,178.2,178.2,0,'EQ',554557,69.15,-0.550000000000011),(27,'APTECHT','APTECHT',81.95,74.95,148.55,148.55,0,'EQ',459257,82.15,-0.200000000000003),(28,'ARCHIES','ARCHIES',30.55,19.25,183.05,183.05,0,'EQ',2331525,30.7,-0.149999999999999),(29,'ARSSINFRA','ARSSINFRA',34.5,21.15,1116.7,1116.7,0,'EQ',27825,35.2,-0.700000000000003),(30,'ARVIND','ARVIND',283,107.2,57.8,57.8,0,'EQ',1419149,282,1),(32,'ASHOKLEY','ASHOKLEY',44.8,17.7,75.8,75.8,0,'EQ',4280762,44.8,0),(34,'ASSAMCO','ASSAMCO',4.5,4.45,21.3,21.3,0,'EQ',6749653,4.5,0),(35,'ASTRAMICRO','ASTRAMICR',111.25,46.2,68.25,68.25,0,'EQ',113810,111.35,-0.0999999999999943),(36,'ATLANTA','ATLANTA',65.35,31.7,447.75,447.75,0,'EQ',1290060,65.55,-0.200000000000003),(37,'ATLASCYCLE','ATLASCYCL',263,262.15,260.3,260.3,0,'EQ',126097,264,-1),(38,'ATUL','ATUL',1404,390.6,172.15,172.15,0,'EQ',287538,1393.95,10.05),(39,'AUROPHARMA','AUROPHARM',936.55,242.45,1184.95,1184.95,0,'EQ',591794,937.05,-0.5),(40,'AUTOIND','AUTOIND',95.5,71.55,237.75,237.75,0,'EQ',171848,95.8,-0.299999999999997),(41,'AXISBANK','AXISBANK',391.05,1213,1471.05,1471.05,0,'EQ',1429001,392,-0.949999999999989),(42,'BAGFILMS','BAGFILMS',3.9,3.15,11.5,11.5,0,'EQ',986002,3.75,0.15),(43,'BAJAJELEC','BAJAJELEC',287.35,172.05,269.65,269.65,0,'EQ',122162,289.75,-2.39999999999998),(44,'BAJAJFINSV','BAJAJFINS',1117.75,668.35,470.2,470.2,0,'EQ',143666,1113,4.75),(45,'BAJAJHIND','BAJAJHIND',18,14.2,123.3,123.3,0,'EQ',4998913,18,0),(46,'BALAJITELE','BALAJITEL',79.35,37.4,48.95,48.95,0,'EQ',129205,79.5,-0.150000000000006),(47,'BALLARPUR','BALLARPUR',16.75,14.3,40.1,40.1,0,'EQ',612201,16.8,-0.0500000000000007),(49,'BANCOINDIA','BANCOINDI',140.6,41.9,98.35,98.35,0,'EQ',30154,141.25,-0.650000000000006),(50,'BANKBARODA','BANKBAROD',871,643.35,1014.15,1014.15,0,'EQ',214984,871.8,-0.799999999999955),(51,'BANKINDIA','BANKINDIA',251.5,230.7,486.55,486.55,0,'EQ',880085,252.4,-0.900000000000006),(54,'BASF','BASF',1310,608.8,669.95,669.95,0,'EQ',63731,1295,15),(55,'BATAINDIA','BATAINDIA',1288.5,939.05,336.15,336.15,0,'EQ',114821,1307,-18.5),(56,'BEPL','BEPL',16.9,17.85,40.95,40.95,0,'BE',12514,16.9,0),(57,'BERGEPAINT','BERGEPAIN',392.8,230.75,101.2,101.2,0,'EQ',1491550,230.75,162.05),(58,'BFUTILITIE','BFUTILITI',675.4,337.45,918.95,918.95,0,'EQ',211139,674.1,1.29999999999995),(59,'BGRENERGY','BGRENERGY',151.2,117.85,753.5,753.5,0,'EQ',67876,152.75,-1.55000000000001),(61,'BHARATFORG','BHARATFOR',774.5,295.55,377.15,377.15,0,'EQ',330320,772,2.5),(62,'BHARTIARTL','BHARTIART',397.55,355.6,325.65,325.65,0,'EQ',9573742,398,-0.449999999999989),(63,'BHARTISHIP','BHARTISHI',35.15,30.25,264.6,264.6,0,'EQ',196959,34.8,0.350000000000001),(64,'BHUSANSTL','BHUSANSTL',107.1,490.35,495.8,495.8,0,'EQ',370035,107.8,-0.700000000000003),(66,'BIOCON','BIOCON',475.55,371.25,411.7,411.7,0,'EQ',582554,477.65,-2.09999999999997),(68,'BOMDYEING','BOMDYEING',66.75,68,613.4,613.4,0,'EQ',251200,67.1,-0.349999999999994),(69,'BPCL','BPCL',661.65,369.85,730.05,730.05,0,'EQ',2143677,666,-4.35000000000002),(71,'BRFL','BRFL',144.4,196.1,219.8,219.8,0,'EQ',405788,196.1,-51.7),(72,'BROADCAST','BROADCAST',4.1,2.9,20.55,20.55,0,'EQ',14112,4.05,0.0499999999999998),(73,'CAIRN','CAIRN',286.35,318.2,320.9,320.9,0,'EQ',2093223,285.45,0.900000000000034),(75,'CANBK','CANBK',375.8,269.3,721.5,721.5,0,'EQ',234882,379.1,-3.30000000000001),(76,'CCCL','CCCL',6.4,4.5,76.95,76.95,0,'EQ',1848798,6.2,0.2),(77,'CEATLTD','CEATLTD',778,217.85,156.85,156.85,0,'EQ',150053,777.15,0.850000000000023),(78,'CELESTIAL','CELESTIAL',29.5,15.7,30.15,30.15,0,'EQ',59190,29.45,0.0500000000000007),(79,'CENTRALBK','CENTRALBK',60.85,63.15,226.25,226.25,0,'EQ',667871,63.15,-2.3),(80,'CENTURYPLY','CENTURYPL',114.2,28.5,68.55,68.55,0,'EQ',1042730,114.6,-0.399999999999991),(81,'CENTURYTEX','CENTURYTE',534,286.2,500.45,500.45,0,'EQ',412423,536.25,-2.25),(82,'CESC','CESC',715.25,389.9,371.3,371.3,0,'EQ',178711,721.05,-5.79999999999995),(83,'CHAMBLFERT','CHAMBLFER',61.5,38.85,80.05,80.05,0,'EQ',1854933,61.75,-0.25),(84,'CHENNPETRO','CHENNPETR',103.5,64.95,240.5,240.5,0,'EQ',179320,64.95,38.55),(85,'CHOLAFIN','CHOLAFIN',456.45,240.2,182.05,182.05,0,'EQ',95884,460,-3.55000000000001),(87,'CIPLA','CIPLA',583.95,424.65,352.95,352.95,0,'EQ',2154362,582.1,1.85000000000002),(88,'COLPAL','COLPAL',1717.4,1241.15,880.15,880.15,0,'EQ',424956,1718.4,-1),(90,'CORPBANK','CORPBANK',327.75,313,746.15,746.15,0,'EQ',94138,328.65,-0.899999999999977),(91,'COSMOFILMS','COSMOFILM',108.5,55.5,169.75,169.75,0,'EQ',2090173,109.25,-0.75),(94,'CROMPGREAV','CROMPGREA',207.75,109.85,317.95,317.95,0,'EQ',813907,208.55,-0.800000000000011),(95,'CUB','CUB',83.75,49.35,50.6,50.6,0,'EQ',2010662,83.9,-0.150000000000006),(97,'DABUR','DABUR',214,170,99.75,99.75,0,'EQ',1325966,214.4,-0.400000000000006),(98,'DBREALTY','DBREALTY',62.45,60.3,419.55,419.55,0,'EQ',370434,62.2,0.25),(101,'DCM','DCM',81.75,71.9,135.25,135.25,0,'EQ',1085786,82,-0.25),(102,'DCW','DCW',24.6,11.1,16.95,16.95,0,'EQ',179768,24.8,-0.199999999999999),(103,'DELTACORP','DELTACORP',94.5,96.1,90.2,90.2,0,'EQ',16864823,96.1,-1.59999999999999),(104,'DENABANK','DENABANK',59.05,62.4,136.7,136.7,0,'EQ',1392822,59.5,-0.450000000000003),(106,'DHAMPURSUG','DHAMPURSU',48.15,31.2,72.75,72.75,0,'EQ',2338951,47.8,0.350000000000001),(107,'DHANBANK','DHANBANK',44.35,45.3,186.2,186.2,0,'EQ',2095207,44.6,-0.25),(108,'DISHTV','DISHTV',54.6,55.6,56.1,56.1,0,'EQ',2051430,54.75,-0.149999999999999),(109,'DIVISLAB','DIVISLAB',1769.45,1029.65,692.3,692.3,0,'EQ',205755,1761,8.45000000000005),(110,'DLF','DLF',112,155.9,350.7,350.7,0,'EQ',8271863,114.75,-2.75),(111,'DOLPHINOFF','DOLPHINOF',141.7,74.25,249.25,249.25,0,'EQ',88432,143.3,-1.60000000000002),(112,'DRREDDY','DRREDDY',2952.65,2397.7,1659.6,1659.6,0,'EQ',297315,2962,-9.34999999999991),(116,'EDUCOMP','EDUCOMP',29.65,25.5,550.45,550.45,0,'EQ',1088844,29.55,0.0999999999999979),(117,'EKC','EKC',14.55,12.25,125.95,125.95,0,'EQ',1654219,12.25,2.3),(118,'ELECTCAST','ELECTCAST',20.45,14,41.25,41.25,0,'EQ',415913,14,6.45),(119,'EMCO','EMCO',38.3,16.05,57.9,57.9,0,'EQ',109570,16.05,22.25),(120,'EMMBI','EMMBI',13.9,10.2,19.2,19.2,0,'EQ',29684,10.2,3.7),(121,'ENGINERSIN','ENGINERSI',236.8,181.6,345.25,345.25,0,'EQ',408279,181.6,55.2),(122,'ERAINFRA','ERAINFRA',13.6,30.85,222.45,222.45,0,'EQ',194466,30.85,-17.25),(123,'ESCORTS','ESCORTS',161.6,114.55,220.7,220.7,0,'EQ',720700,114.55,47.05),(124,'ESSAROIL','ESSAROIL',119.4,55.7,147.2,147.2,0,'EQ',1530326,55.7,63.7),(126,'ESSDEE','ESSDEE',394.5,498.45,474.65,474.65,0,'EQ',11692,498.45,-103.95),(127,'ESSELPACK','ESSELPACK',111,47.7,51.75,51.75,0,'EQ',223722,47.7,63.3),(129,'EVEREADY','EVEREADY',102.6,25.35,67.9,67.9,0,'EQ',178641,25.35,77.25),(130,'EVERONN','EVERONN',38.1,38.1,660.9,660.9,0,'EQ',1932426,38.1,0),(131,'EXCELINFO','EXCELINFO',18.5,16.5,48.7,48.7,0,'EQ',43312,16.5,2),(132,'EXIDEIND','EXIDEIND',170,125.4,154.9,154.9,0,'EQ',1042382,125.4,44.6),(134,'FEDERALBNK','FEDERALBN',137.6,81.3,472,472,0,'EQ',1102260,81.3,56.3),(135,'FINANTECH','FINANTECH',225.5,171.05,982.35,982.35,0,'EQ',134574,171.05,54.45),(136,'FINPIPE','FINPIPE',339.15,145.55,116.25,116.25,0,'EQ',127515,145.55,193.6),(137,'FORTIS','FORTIS',117.8,102.65,160.2,160.2,0,'EQ',1795040,102.65,15.15),(138,'FSL','FSL',22.85,22.85,27.2,27.2,0,'EQ',6516685,22.85,0),(139,'GABRIEL','GABRIEL',82.95,22.05,67.55,67.55,0,'EQ',57630,22.05,60.9),(140,'GAEL','GAEL',59.2,26.7,42.7,42.7,0,'EQ',503444,26.7,32.5),(141,'GAIL','GAIL',443.35,345.15,491.55,491.55,0,'EQ',1027490,345.15,98.2),(143,'GATI','GATI',181.65,31.7,71,71,0,'EQ',451989,31.7,149.95),(144,'GDL','GDL',264.8,125.2,111.25,111.25,0,'EQ',257474,125.2,139.6),(146,'GEOMETRIC','GEOMETRIC',138,86.3,66.55,66.55,0,'EQ',181515,86.3,51.7),(147,'GESHIP','GESHIP',408.85,288.7,317.55,317.55,0,'EQ',890513,288.7,120.15),(148,'GHCL','GHCL',93.6,31.5,47.85,47.85,0,'EQ',118121,31.5,62.1),(149,'GICHSGFIN','GICHSGFIN',160.25,103.8,149.9,149.9,0,'EQ',235531,103.8,56.45),(150,'GITANJALI','GITANJALI',62.05,63.5,296.45,296.45,0,'EQ',1353879,63.5,-1.45),(151,'GLENMARK','GLENMARK',721.35,519.15,338.6,338.6,0,'EQ',597996,519.15,202.2),(152,'GLOBUSSPR','GLOBUSSPR',75.4,105.35,159.5,159.5,0,'EQ',102953,105.35,-29.95),(153,'GMDCLTD','GMDCLTD',157.7,104.8,135.15,135.15,0,'EQ',1768512,104.8,52.9),(154,'GMRINFRA','GMRINFRA',23.2,23.2,53.5,53.5,0,'EQ',1821126,23.2,0),(155,'GODREJCP','GODREJCP',965.15,837.2,419.95,419.95,0,'EQ',456412,837.2,127.95),(156,'GODREJIND','GODREJIND',292.6,295.45,213.2,213.2,0,'EQ',884435,295.45,-2.84999999999997),(157,'GODREJPROP','GODREJPRO',366.35,366.35,728.6,728.6,0,'EQ',54750,366.35,0),(158,'GOKUL','GOKUL',18.5,17.3,98,98,0,'EQ',121117,17.3,1.2),(159,'GOLDBEES','GOLDBEES',2493,2876.7,1895.05,1895.05,0,'EQ',62615,2876.7,-383.7),(160,'GRAPHITE','GRAPHITE',93.75,74.1,93.7,93.7,0,'EQ',506321,74.1,19.65),(161,'GRASIM','GRASIM',3435.45,2745.75,2240.5,2240.5,0,'EQ',89838,2745.75,689.7),(162,'GRINDWELL','GRINDWELL',499,262,230.5,230.5,0,'EQ',19031,262,237),(163,'GSPL','GSPL',89.15,62.1,112.55,112.55,0,'EQ',643077,62.1,27.05),(164,'GTLINFRA','GTLINFRA',2.75,1.8,44.75,44.75,0,'EQ',801763,1.8,0.95),(165,'GTOFFSHORE','GTOFFSHOR',96.9,57,376.85,376.85,0,'EQ',107991,57,39.9),(166,'GUJNRECOKE','GUJNRECOK',9.05,13.4,61.8,61.8,0,'EQ',911432,13.4,-4.35),(167,'GUJNREDVR','GUJNREDVR',4.35,6,41.3,41.3,0,'EQ',114350,6,-1.65),(169,'GULFOILCOR','GULFOILCO',164.6,84.95,127.85,127.85,0,'EQ',352628,84.95,79.65),(170,'GVKPIL','GVKPIL',10.15,8.4,42.55,42.55,0,'EQ',4599545,8.4,1.75),(172,'HANUNG','HANUNG',40.85,31.7,362.5,362.5,0,'EQ',559223,31.7,9.15),(173,'HAVELLS','HAVELLS',267.7,750.55,414.4,414.4,0,'EQ',73556,750.55,-482.85),(174,'HCC','HCC',31.4,14.75,61.1,61.1,0,'EQ',1445339,14.75,16.65),(176,'HCLTECH','HCLTECH',1740.65,1083.05,403.8,403.8,0,'EQ',774866,1083.05,657.6),(177,'HDIL','HDIL',78.6,44.65,247.5,247.5,0,'EQ',5845572,44.65,33.95),(178,'HERCULES','HERCULES',185.45,100.15,333.4,333.4,0,'EQ',150762,100.15,85.3),(180,'HEXAWARE','HEXAWARE',185.4,133.75,87.75,87.75,0,'EQ',6389618,133.75,51.65),(181,'HIMATSEIDE','HIMATSEID',43.4,43.4,44.25,44.25,0,'EQ',255672,43.4,0),(182,'HINDALCO','HINDALCO',149.6,115.25,210.5,210.5,0,'EQ',8555553,115.25,34.35),(183,'HINDCOPPER','HINDCOPPE',78.75,75.4,429.6,429.6,0,'EQ',177340,75.4,3.34999999999999),(185,'HINDOILEXP','HINDOILEX',59,30.5,254.05,254.05,0,'EQ',1281589,30.5,28.5),(186,'HINDPETRO','HINDPETRO',508.9,228.05,487.3,487.3,0,'EQ',1999295,228.05,280.85),(187,'HITACHIHOM','HITACHIHO',619.05,109.1,300.05,300.05,0,'EQ',37130,109.1,509.95),(188,'HMT','HMT',31.15,30.25,70.6,70.6,0,'EQ',173556,30.25,0.899999999999999),(189,'HOTELEELA','HOTELEELA',21.05,15.85,51.6,51.6,0,'EQ',1108167,15.85,5.2),(190,'HSIL','HSIL',404.05,95.95,140.5,140.5,0,'EQ',98698,95.95,308.1),(192,'IBPOW','IBPOW',13.05,9.25,27.75,27.75,0,'EQ',720507,9.25,3.8),(193,'IBREALEST','IBREALEST',66.95,64.85,191.4,191.4,0,'EQ',4252611,64.85,2.10000000000001),(194,'IBSEC','IBSEC',24.95,16.25,26.95,26.95,0,'EQ',1700431,16.25,8.7),(195,'ICICIBANK','ICICIBANK',1479.1,1080.4,1163,1163,0,'EQ',3136582,1080.4,398.7),(196,'ICSA','ICSA',4.2,4.35,129.65,129.65,0,'EQ',239555,4.35,-0.149999999999999),(197,'IDBI','IDBI',62.25,68.55,180.6,180.6,0,'EQ',5565480,68.55,-6.3),(198,'IDEA','IDEA',155.1,166.5,67.45,67.45,0,'EQ',4738501,166.5,-11.4),(199,'IDFC','IDFC',140.55,110.4,200.05,200.05,0,'EQ',3943098,110.4,30.15),(200,'IFBIND','IFBIND',343.95,65.45,162.05,162.05,0,'EQ',16765,65.45,278.5),(201,'IFCI','IFCI',34.15,25.4,69.15,69.15,0,'EQ',18523810,25.4,8.75),(202,'IFGLREFRAC','IFGLREFRA',180,37,50.9,50.9,0,'EQ',49028,37,143),(203,'IGL','IGL',426.05,286.75,328.85,328.85,0,'EQ',264036,286.75,139.3),(204,'INDBANK','INDBANK',5.3,3.5,21.7,21.7,0,'EQ',28473,3.5,1.8),(205,'INDHOTEL','INDHOTEL',96.35,53.5,100.1,100.1,0,'EQ',1451073,53.5,42.85),(207,'INDIACEM','INDIACEM',108.3,55.85,110.1,110.1,0,'EQ',7032603,55.85,52.45),(208,'INDIAINFO','INDIAINFO',72,60.4,116.7,116.7,0,'EQ',10787737,60.4,11.6),(209,'INDIANB','INDIANB',153.5,103.2,291.8,291.8,0,'EQ',1208171,103.2,50.3),(212,'INGVYSYABK','INGVYSYAB',620.4,611.15,403.95,403.95,0,'EQ',193797,611.15,9.25),(213,'INOXLEISUR','INOXLEISU',172,91.85,72.35,72.35,0,'EQ',111142,91.85,80.15),(214,'IOB','IOB',58.55,53.55,159.8,159.8,0,'EQ',965286,53.55,5),(215,'IOC','IOC',377.9,216.15,418.05,418.05,0,'EQ',1923797,216.15,161.75),(216,'IPCALAB','IPCALAB',722.8,684.65,325.75,325.75,0,'EQ',199391,684.65,38.15),(217,'IRB','IRB',90.45,90.45,259.85,259.85,0,'EQ',2428348,90.45,0),(218,'ISFT','ISFT',49.4,41.8,98,98,0,'EQ',868531,41.8,7.6),(219,'ISMTLTD','ISMTLTD',16.4,9.1,51.65,51.65,0,'EQ',45446,9.1,7.3),(221,'ITC','ITC',346.75,318.7,171.2,171.2,0,'EQ',13855370,318.7,28.05),(222,'JAGRAN','JAGRAN',126.15,86.55,132.75,132.75,0,'EQ',49308,86.55,39.6),(224,'JBCHEPHARM','JBCHEPHAR',232,101.25,121.85,121.85,0,'EQ',265941,101.25,130.75),(225,'JBFIND','JBFIND',150,84.05,214.1,214.1,0,'EQ',774873,84.05,65.95),(226,'JETAIRWAYS','JETAIRWAY',235.25,350.45,809.9,809.9,0,'EQ',490749,350.45,-115.2),(229,'JKLAKSHMI','JKLAKSHMI',341.5,69.95,61.4,61.4,0,'EQ',241128,69.95,271.55),(230,'JKTYRE','JKTYRE',517.65,134.8,163.5,163.5,0,'EQ',213402,134.8,382.85),(232,'JPASSOCIAT','JPASSOCIA',30.3,47.75,120.25,120.25,0,'EQ',7313905,47.75,-17.45),(233,'JPINFRATEC','JPINFRATE',21.7,18.7,89.75,89.75,0,'EQ',1065650,18.7,3),(234,'JPPOWER','JPPOWER',12.6,19.45,62.45,62.45,0,'EQ',721711,19.45,-6.85),(235,'JSWENERGY','JSWENERGY',74.15,48.05,119.3,119.3,0,'EQ',2211599,48.05,26.1),(236,'JSWSTEEL','JSWSTEEL',1120.55,863.8,1341.95,1341.95,0,'EQ',1778711,863.8,256.75),(237,'JUBILANT','JUBILANT',147.15,104.65,313.25,313.25,0,'EQ',100075,104.65,42.5),(238,'JUBLFOOD','JUBLFOOD',1220.25,1303.25,512.3,512.3,0,'EQ',667736,1303.25,-83),(240,'JYOTHYLAB','JYOTHYLAB',190.7,190.7,276,276,0,'EQ',31648,190.7,0),(241,'KAJARIACER','KAJARIACE',652.05,242.45,74.8,74.8,0,'EQ',503100,242.45,409.6),(242,'KALINDEE','KALINDEE',89,69.7,169.8,169.8,0,'EQ',330856,69.7,19.3),(243,'KCPSUGIND','KCPSUGIND',20.95,16.65,19.8,19.8,0,'EQ',192817,16.65,4.3),(244,'KERNEX','KERNEX',75.05,47.05,150.9,150.9,0,'EQ',222708,47.05,28),(245,'KGL','KGL',1.3,1.05,34.9,34.9,0,'EQ',27961353,1.05,0.25),(246,'KOHINOOR','KOHINOOR',48.1,39.15,64.05,64.05,0,'EQ',198724,39.15,8.95),(247,'KOLTEPATIL','KOLTEPATI',185.8,88.8,66.8,66.8,0,'EQ',401638,88.8,97),(248,'KOPRAN','KOPRAN',63.5,16.7,36,36,0,'EQ',236785,16.7,46.8),(249,'KOTAKBANK','KOTAKBANK',1016.95,731.45,464.25,464.25,0,'EQ',1420480,731.45,285.5),(250,'KPIT','KPIT',160.15,149.75,157.1,157.1,0,'EQ',182199,149.75,10.4),(251,'KPRMILL','KPRMILL',314,150,227.45,227.45,0,'EQ',15559,150,164),(252,'KRBL','KRBL',31.4,31.4,49.5,49.5,0,'EQ',2276451,31.4,0),(253,'KSL','KSL',51.6,51.6,117.75,117.75,0,'EQ',377105,51.6,0),(255,'KTKBANK','KTKBANK',114.65,101.55,184.35,184.35,0,'EQ',579440,101.55,13.1),(256,'KWALITY','KWALITY',47.05,27.15,123.45,123.45,0,'EQ',541478,27.15,19.9),(257,'LAKSHVILAS','LAKSHVILA',74.8,74.8,129.75,129.75,0,'EQ',1564655,74.8,0),(258,'LIBERTSHOE','LIBERTSHO',301.5,87.05,104.3,104.3,0,'EQ',65354,87.05,214.45),(259,'LICHSGFIN','LICHSGFIN',333.6,223.8,1340.55,1340.55,0,'EQ',865078,223.8,109.8),(260,'LIQUIDBEES','LIQUIDBEE',1000,1000.01,999.99,999.99,0,'EQ',232620,1000.01,-0.00999999999999091),(261,'LITL','LITL',7.35,6.7,63.75,63.75,0,'EQ',2587824,6.7,0.649999999999999),(263,'LOTUSEYE','LOTUSEYE',5.95,5.95,17.05,17.05,0,'EQ',89363,5.95,0),(264,'LT','LT',1451.5,963.8,2027.8,2027.8,0,'EQ',1515463,963.8,487.7),(265,'LUPIN','LUPIN',1325,874.1,438.3,438.3,0,'EQ',922390,874.1,450.9),(266,'MAGMA','MAGMA',120.35,70.15,75.55,75.55,0,'EQ',1973763,70.15,50.2),(267,'MAHABANK','MAHABANK',40.55,40.6,77.65,77.65,0,'EQ',435799,40.6,-0.0500000000000043),(268,'MANAPPURAM','MANAPPURA',28.1,16.95,147.7,147.7,0,'EQ',445578,16.95,11.15),(269,'MANGCHEFER','MANGCHEFE',95,56.75,42.05,42.05,0,'EQ',424785,56.75,38.25),(270,'MARICO','MARICO',211,211,137.15,137.15,0,'EQ',1206197,211,0),(271,'MARUTI','MARUTI',2980.5,1614.55,1551.6,1551.6,0,'EQ',542217,1614.55,1365.95),(272,'MASTEK','MASTEK',261,153.3,203.85,203.85,0,'EQ',28620,153.3,107.7),(273,'MAX','MAX',316.95,194.1,158.25,158.25,0,'EQ',1264282,194.1,122.85),(276,'MCDHOLDING','MCDHOLDIN',31.35,30.05,144.35,144.35,0,'EQ',1032348,30.05,1.3),(278,'MCLEODRUSS','MCLEODRUS',267.75,271.15,230.05,230.05,0,'EQ',440845,271.15,-3.39999999999998),(279,'MEGH','MEGH',18.1,7,21.15,21.15,0,'EQ',628250,7,11.1),(280,'MIC','MIC',4.2,3.95,39.7,39.7,0,'EQ',554831,3.95,0.25),(283,'MIRZAINT','MIRZAINT',43.1,24.8,15.75,15.75,0,'EQ',264597,24.8,18.3),(285,'MMFL','MMFL',508,96.35,143.8,143.8,0,'EQ',14162,96.35,411.65),(286,'MOSERBAER','MOSERBAER',7,3.35,64.8,64.8,0,'EQ',1463969,3.35,3.65),(288,'MPHASIS','MPHASIS',418.05,419.55,610.4,610.4,0,'EQ',330648,419.55,-1.5),(290,'MRPL','MRPL',60.55,40.4,81.2,81.2,0,'EQ',1718254,40.4,20.15),(292,'MTNL','MTNL',27.55,15.85,67.5,67.5,0,'EQ',2734125,15.85,11.7),(293,'MUDRA','MUDRA',12.8,10.5,53.55,53.55,0,'EQ',2585856,10.5,2.3),(294,'MUKANDLTD','MUKANDLTD',41.45,21.75,67.85,67.85,0,'EQ',78230,21.75,19.7),(298,'NDTV','NDTV',125.85,91.8,102.75,102.75,0,'EQ',114283,91.8,34.05),(299,'NETWORK18','NETWORK18',49.9,32.3,161.35,161.35,0,'EQ',84860,32.3,17.6),(300,'NEYVELILIG','NEYVELILI',84.45,68.45,156.65,156.65,0,'EQ',348621,68.45,16),(301,'NFL','NFL',33.2,24.2,117.45,117.45,0,'EQ',187808,24.2,9),(302,'NHPC','NHPC',18.55,18.55,31.2,31.2,0,'EQ',17366419,18.55,0),(303,'NIFTYBEES','NIFTYBEES',799.4,628.88,607.98,607.98,0,'EQ',95372,628.88,170.52),(304,'NIITLTD','NIITLTD',21.75,21.75,65.75,65.75,0,'EQ',889789,21.75,0),(305,'NIITTECH','NIITTECH',402.45,284.45,219.4,219.4,0,'EQ',89202,284.45,118),(306,'NILKAMAL','NILKAMAL',357.95,142.4,375.5,375.5,0,'EQ',249288,142.4,215.55),(309,'NITCO','NITCO',23.35,62.8,63,63,0,'EQ',1549652,62.8,-39.45),(310,'NMDC','NMDC',153.1,276.95,277.3,277.3,0,'EQ',1196953,276.95,-123.85),(311,'NOCIL','NOCIL',41.8,22.6,22.4,22.4,0,'EQ',398314,22.6,19.2),(312,'NOIDATOLL','NOIDATOLL',32.2,32.2,31.7,31.7,0,'EQ',1293066,32.2,0),(313,'NRBBEARING','NRBBEARIN',131.85,57.5,57.75,57.75,0,'EQ',36822,57.5,74.35),(314,'NTPC','NTPC',193.2,193.2,195.3,195.3,0,'EQ',3000318,193.2,0),(315,'OIL','OIL',594.6,1432.5,1450.05,1450.05,0,'EQ',88742,1432.5,-837.9),(316,'OISL','OISL',24.25,78.1,75.15,75.15,0,'EQ',288341,78.1,-53.85),(317,'OMAXE','OMAXE',127.75,152.15,149.3,149.3,0,'EQ',298335,152.15,-24.4),(318,'ONGC','ONGC',403.9,1319.35,1304.05,1304.05,0,'EQ',1141980,1319.35,-915.45),(319,'ONMOBILE','ONMOBILE',31.3,334.75,334.4,334.4,0,'EQ',83485,334.75,-303.45),(320,'OPTOCIRCUI','OPTOCIRCU',20.05,293.65,287.55,287.55,0,'EQ',429805,293.65,-273.6),(321,'ORBITCORP','ORBITCORP',14.55,118.25,116.05,116.05,0,'EQ',638790,118.25,-103.7),(324,'PANACEABIO','PANACEABI',170.95,203.15,199,199,0,'EQ',105706,203.15,-32.2),(327,'PARSVNATH','PARSVNATH',20.4,69.9,66.8,66.8,0,'EQ',123120,69.9,-49.5),(329,'PETRONET','PETRONET',180.1,118.55,111.2,111.2,0,'EQ',1517760,118.55,61.55),(330,'PFC','PFC',245.5,363.25,360,360,0,'EQ',352481,363.25,-117.75),(334,'PIRGLASS','PIRGLASS',138.65,134.35,137.65,137.65,0,'EQ',12989,134.35,4.30000000000001),(336,'PNB','PNB',1030,1450,1290.8,1290.8,0,'EQ',203191,1450,-420),(337,'POCHIRAJU','POCHIRAJU',29.55,23.4,23.7,23.7,0,'EQ',65988,23.4,6.15),(338,'POLARIS','POLARIS',163.75,163.75,161.75,161.75,0,'EQ',505434,163.75,0),(339,'POWERGRID','POWERGRID',135.1,100.65,100.4,100.4,0,'EQ',932875,100.65,34.45),(340,'PRAENG','PRAENG',9.9,27.2,27.6,27.6,0,'EQ',211283,27.2,-17.3),(341,'PRAJIND','PRAJIND',62.15,70,68.95,68.95,0,'EQ',1193696,70,-7.85),(342,'PRAKASH','PRAKASH',55.6,146.6,144.15,144.15,0,'EQ',505612,146.6,-91),(343,'PRATIBHA','PRATIBHA',51.6,77.8,77.85,77.85,0,'EQ',39982,77.8,-26.2),(344,'PRICOL','PRICOL',25.45,25.45,26,26,0,'EQ',82250,25.45,0),(345,'PRISMCEM','PRISMCEM',77.25,59.5,59.1,59.1,0,'EQ',737874,59.5,17.75),(346,'PRITHVI','PRITHVI',4.45,45.55,45.65,45.65,0,'EQ',30396,45.55,-41.1),(348,'PSL','PSL',110.8,110.8,109.85,109.85,0,'EQ',262815,110.8,0),(349,'PTC','PTC',83.75,135.2,134.55,134.55,0,'EQ',711485,135.2,-51.45),(350,'PUNJLLOYD','PUNJLLOYD',37,122.55,117.4,117.4,0,'EQ',3502725,122.55,-85.55),(353,'RANBAXY','RANBAXY',601.55,602.3,579.75,579.75,0,'EQ',1315614,602.3,-0.75),(354,'RAYMOND','RAYMOND',460.4,430.7,431.25,431.25,0,'EQ',408948,430.7,29.7),(355,'RCF','RCF',53.65,95.9,94.8,94.8,0,'EQ',553684,95.9,-42.25),(356,'RCOM','RCOM',183.55,183.55,180.1,180.1,0,'EQ',11137717,183.55,0),(357,'RECLTD','RECLTD',254.5,366.55,370.65,370.65,0,'EQ',565720,366.55,-112.05),(359,'RELCAPITAL','RELCAPITA',446.6,823.3,813.9,813.9,0,'EQ',1805700,823.3,-376.7),(360,'RELIANCE','RELIANCE',969.5,1093.2,1096.25,1096.25,0,'EQ',6953447,1093.2,-123.7),(361,'RELINFRA','RELINFRA',571.95,1048.9,1036,1036,0,'EQ',1125364,1048.9,-476.95),(363,'RENUKA','RENUKA',16.55,94.3,90.55,90.55,0,'EQ',12103617,94.3,-77.75),(365,'RICOAUTO','RICOAUTO',32.65,26.7,26.85,26.85,0,'EQ',582346,26.7,5.95),(366,'RIIL','RIIL',495.9,868.45,860.05,860.05,0,'EQ',650651,868.45,-372.55),(368,'ROLTA','ROLTA',116.15,170.85,167.3,167.3,0,'EQ',639016,170.85,-54.7),(369,'RPOWER','RPOWER',71.05,158.3,157.05,157.05,0,'EQ',1510791,158.3,-87.25),(370,'RUCHINFRA','RUCHINFRA',16.7,31.7,32.4,32.4,0,'EQ',51663,31.7,-15),(371,'RUCHISOYA','RUCHISOYA',36.3,132.95,132.1,132.1,0,'EQ',294998,132.95,-96.65),(372,'SABERORGAN','SABERORGA',176.85,69.15,67.3,67.3,0,'EQ',51492,69.15,107.7),(373,'SAIL','SAIL',76.2,193.3,194.15,194.15,0,'EQ',3053944,193.3,-117.1),(374,'SAKHTISUG','SAKHTISUG',15.85,59.3,58.25,58.25,0,'EQ',1451323,59.3,-43.45),(376,'SANWARIA','SANWARIA',8.8,133.95,130.7,130.7,0,'EQ',75425,133.95,-125.15),(378,'SCI','SCI',57.4,176.35,173.1,173.1,0,'EQ',228088,176.35,-118.95),(380,'SELMCL','SELMCL',4.1,41.55,44.55,44.55,0,'EQ',1201632,41.55,-37.45),(382,'SGJHL','SGJHL',27.1,207.9,203.6,203.6,0,'EQ',194729,207.9,-180.8),(385,'SIEMENS','SIEMENS',820,828.75,817.55,817.55,0,'EQ',312522,828.75,-8.75),(386,'SINTEX','SINTEX',89.85,211.2,204.15,204.15,0,'EQ',359789,211.2,-121.35),(387,'SJVN','SJVN',22.5,24.3,24.05,24.05,0,'EQ',1418540,24.3,-1.8),(390,'SMPL','SMPL',3.95,31.9,29.65,29.65,0,'EQ',424739,31.9,-27.95),(391,'SOBHA','SOBHA',403.5,366.85,361.15,361.15,0,'EQ',128448,366.85,36.65),(392,'SONASTEER','SONASTEER',53.75,20.5,20.1,20.1,0,'EQ',266335,20.5,33.25),(394,'SOUTHBANK','SOUTHBANK',26.05,28.45,27.55,27.55,0,'EQ',1807846,28.45,-2.4),(395,'SPARC','SPARC',196.6,92.75,92.5,92.5,0,'EQ',1001236,92.75,103.85),(398,'SRF','SRF',709,417.5,402.45,402.45,0,'EQ',204236,417.5,291.5),(400,'STAR','STAR',793.95,426.9,420.8,420.8,0,'EQ',160514,426.9,367.05),(401,'STCINDIA','STCINDIA',197.5,381.4,374.9,374.9,0,'EQ',165139,381.4,-183.9),(404,'STRTECH','STRTECH',74.4,83.45,82.65,82.65,0,'EQ',1093528,83.45,-9.05),(405,'SUBEX','SUBEX',9.05,74.45,73.8,73.8,0,'EQ',310687,74.45,-65.4),(407,'SUNFLAG','SUNFLAG',27.15,29.6,29.65,29.65,0,'EQ',290420,29.6,-2.45),(408,'SUNPHARMA','SUNPHARMA',808.6,2190.7,2110.05,2110.05,0,'EQ',432733,2190.7,-1382.1),(409,'SUNTV','SUNTV',314.35,502.25,499.3,499.3,0,'EQ',355958,502.25,-187.9),(411,'SURYAROSNI','SURYAROSN',93.55,114.8,113.8,113.8,0,'EQ',62152,114.8,-21.25),(412,'SUVEN','SUVEN',191.1,28.2,28.45,28.45,0,'EQ',185456,28.2,162.9),(413,'SUZLON','SUZLON',12.65,58.45,55.45,55.45,0,'EQ',25857306,58.45,-45.8),(414,'SYNCOM','SYNCOM',6.75,50.55,50.7,50.7,0,'EQ',210133,50.55,-43.8),(415,'SYNDIBANK','SYNDIBANK',116,142,138.8,138.8,0,'EQ',493493,142,-26),(416,'TANLA','TANLA',18.8,25.9,25.8,25.8,0,'EQ',610078,25.9,-7.1),(417,'TARAPUR','TARAPUR',8.25,34.5,34.1,34.1,0,'EQ',146370,34.5,-26.25),(418,'TATACHEM','TATACHEM',388.25,386.15,390.45,390.45,0,'EQ',379929,386.15,2.10000000000002),(419,'TATACOFFEE','TATACOFFE',614.4,614.4,605.35,605.35,0,'EQ',264380,614.4,0),(420,'TATACOMM','TATACOMM',383.2,304.4,306.25,306.25,0,'EQ',191634,304.4,78.8),(421,'TATAELXSI','TATAELXSI',625.6,285.7,277.25,277.25,0,'EQ',238703,285.7,339.9),(422,'TATAMETALI','TATAMETAL',141.75,135.65,135.15,135.15,0,'EQ',177469,135.65,6.09999999999999),(423,'TATAMOTORS','TATAMOTOR',488.65,1170.6,1159,1159,0,'EQ',2658263,1170.6,-681.95),(424,'TATAMTRDVR','TATAMTRDV',321,850.7,840.4,840.4,0,'EQ',177366,850.7,-529.7),(425,'TATAPOWER','TATAPOWER',85.6,1404.65,1397.35,1397.35,0,'EQ',428850,1404.65,-1319.05),(426,'TATASTEEL','TATASTEEL',447.75,591.9,589.4,589.4,0,'EQ',8336195,591.9,-144.15),(427,'TCS','TCS',2694.9,1053.4,1052.9,1052.9,0,'EQ',1209554,1053.4,1641.5),(428,'TECHM','TECHM',2368.85,735.6,729.25,729.25,0,'EQ',182636,735.6,1633.25),(429,'TFCILTD','TFCILTD',42.5,40.3,36.3,36.3,0,'EQ',2121845,40.3,2.2),(430,'THINKSOFT','THINKSOFT',593.6,109.85,110.15,110.15,0,'EQ',54579,109.85,483.75),(431,'THOMASCOOK','THOMASCOO',148.1,67.7,66.85,66.85,0,'EQ',759064,67.7,80.4),(432,'TIMETECHNO','TIMETECHN',48.35,61.45,60.45,60.45,0,'EQ',199041,61.45,-13.1),(433,'TNPL','TNPL',140.65,154.2,151.7,151.7,0,'EQ',43213,154.2,-13.55),(434,'TORNTPOWER','TORNTPOWE',143.45,284.75,291.3,291.3,0,'EQ',161103,284.75,-141.3),(435,'TRIVENI','TRIVENI',22,122.1,119.8,119.8,0,'EQ',841521,122.1,-100.1),(436,'TTML','TTML',9.7,22.75,22.5,22.5,0,'EQ',2381341,22.75,-13.05),(437,'TUBEINVEST','TUBEINVES',317.95,153.9,150.95,150.95,0,'EQ',116471,153.9,164.05),(438,'TULIP','TULIP',3.95,180,178.45,178.45,0,'EQ',189857,180,-176.05),(439,'TULSI','TULSI',4.8,58.45,59.9,59.9,0,'EQ',878617,58.45,-53.65),(441,'TVSMOTOR','TVSMOTOR',225.1,77.75,73.05,73.05,0,'EQ',1269836,77.75,147.35),(442,'TWL','TWL',209.7,500.55,494.9,494.9,0,'EQ',450273,500.55,-290.85),(443,'UBENGG','UBENGG',11.55,199.3,187.95,187.95,0,'EQ',88368,199.3,-187.75),(444,'UBHOLDINGS','UBHOLDING',27.65,291.25,286.6,286.6,0,'EQ',434434,291.25,-263.6),(445,'UBL','UBL',709.25,445.25,447.3,447.3,0,'EQ',250932,445.25,264),(446,'UCOBANK','UCOBANK',80.2,134.95,125.5,125.5,0,'EQ',2002500,134.95,-54.75),(447,'UFLEX','UFLEX',161.35,305.2,303.65,303.65,0,'EQ',2114276,305.2,-143.85),(448,'ULTRACEMCO','ULTRACEMC',2484.05,1102.5,1099.9,1099.9,0,'EQ',255933,1102.5,1381.55),(449,'UNIONBANK','UNIONBANK',211.5,381.95,377.95,377.95,0,'EQ',559687,381.95,-170.45),(451,'UNITECH','UNITECH',18.35,89.25,86.8,86.8,0,'EQ',22271345,89.25,-70.9),(452,'UNITEDBNK','UNITEDBNK',42.35,128.35,126.55,126.55,0,'EQ',421721,128.35,-86),(453,'UNITY','UNITY',23.45,105.45,104.75,104.75,0,'EQ',53770,105.45,-82),(454,'USHAMART','USHAMART',22,86.05,86.9,86.9,0,'EQ',334602,86.05,-64.05),(455,'UTTAMSTL','UTTAMSTL',72.05,152.9,152.95,152.95,0,'EQ',178992,152.9,-80.85),(456,'VALUEIND','VALUEIND',11.75,27.5,27.45,27.45,0,'EQ',38070,27.5,-15.75),(458,'VARUN','VARUN',7.45,225,221.15,221.15,0,'EQ',468704,225,-217.55),(459,'VARUNSHIP','VARUNSHIP',7.25,41.45,41.25,41.25,0,'EQ',252180,41.45,-34.2),(460,'VGUARD','VGUARD',911.05,188.35,187.05,187.05,0,'EQ',681141,188.35,722.7),(461,'VICEROY','VICEROY',19.3,42.65,43.2,43.2,0,'EQ',129533,42.65,-23.35),(462,'VIDEOIND','VIDEOIND',166,260.6,253.65,253.65,0,'EQ',674768,260.6,-94.6),(464,'VIPIND','VIPIND',595.7,595.7,590.1,590.1,0,'EQ',309323,595.7,0),(465,'VOLTAS','VOLTAS',235.3,242.45,245.2,245.2,0,'EQ',2469094,242.45,-7.14999999999998),(466,'WELCORP','WELCORP',82.25,258.8,248.75,248.75,0,'EQ',433906,258.8,-176.55),(467,'WHIRLPOOL','WHIRLPOOL',434.05,301.8,301.45,301.45,0,'BE',86813,301.8,132.25),(468,'WIPRO','WIPRO',587,423.7,419.75,419.75,0,'EQ',972372,423.7,163.3),(472,'YESBANK','YESBANK',592.85,369.35,359.5,359.5,0,'EQ',1295695,369.35,223.5),(473,'ZEEL','ZEEL',318,284.4,276.2,276.2,0,'EQ',1503855,284.4,33.6),(475,'ZENITHBIR','ZENITHBIR',1.75,11.8,11.8,11.8,0,'EQ',891922,11.8,-10.05),(476,'ZYLOG','ZYLOG',7.6,573.55,571.05,571.05,0,'EQ',69447,573.55,-565.95);

UNLOCK TABLES;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `Username` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  PRIMARY KEY (`Username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `users` */

LOCK TABLES `users` WRITE;

insert  into `users`(`Username`,`Password`) values ('nilmadhab','a44a4fcc2a9c631cedaa38dfbb5c2ce18d0b47ee'),('nilmadhab1','058b9101175607822bb9e676c90c49c480565597'),('root','b24bbf5f381e65420a33b33a01855d889e5e5000'),('root_nil','15def2e7b5dc84a03205150d9a3955bb8a5960db');

UNLOCK TABLES;

/*Table structure for table `wud_closed_options` */

DROP TABLE IF EXISTS `wud_closed_options`;

CREATE TABLE `wud_closed_options` (
  `userid` varchar(200) NOT NULL,
  `orderno` int(11) NOT NULL AUTO_INCREMENT,
  `trandateandtime` datetime NOT NULL,
  `stockname` varchar(200) NOT NULL,
  `bors` varchar(20) NOT NULL,
  `quantity` bigint(20) NOT NULL,
  `tranprice` double NOT NULL,
  `brockerage` double NOT NULL,
  `closeCP` double NOT NULL,
  `profit` double NOT NULL,
  PRIMARY KEY (`orderno`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `wud_closed_options` */

LOCK TABLES `wud_closed_options` WRITE;

insert  into `wud_closed_options`(`userid`,`orderno`,`trandateandtime`,`stockname`,`bors`,`quantity`,`tranprice`,`brockerage`,`closeCP`,`profit`) values ('nilmadhab',1,'2014-10-12 15:48:21','20MICRONS','Sell',25,10,0.5,10,0),('nilmadhab',2,'2014-10-14 01:52:16','20MICRONS','Sell',2,10,0.04,10,0),('nilmadhab',3,'2014-10-17 03:45:05','20MICRONS','Short Cover',12,10,0.24,30.4,-244.8),('nilmadhab',4,'2014-10-18 05:03:38','20MICRONS','Sell',122,10,2.44,30.4,2488.8);

UNLOCK TABLES;

/*Table structure for table `wud_opened_options` */

DROP TABLE IF EXISTS `wud_opened_options`;

CREATE TABLE `wud_opened_options` (
  `userid` varchar(200) NOT NULL,
  `orderno` int(11) NOT NULL AUTO_INCREMENT,
  `trandateandtime` datetime NOT NULL,
  `stockname` varchar(200) NOT NULL,
  `bors` varchar(10) NOT NULL,
  `quantity` bigint(20) NOT NULL,
  `tranprice` double NOT NULL,
  `triggerprice` double NOT NULL,
  `margin` double NOT NULL,
  PRIMARY KEY (`orderno`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `wud_opened_options` */

LOCK TABLES `wud_opened_options` WRITE;

insert  into `wud_opened_options`(`userid`,`orderno`,`trandateandtime`,`stockname`,`bors`,`quantity`,`tranprice`,`triggerprice`,`margin`) values ('nilmadhab',2,'2014-10-18 05:03:14','20MICRONS','Buy',1,30.4,0,0),('nilmadhab',3,'2014-10-18 05:04:33','AFTEK','Short Sell',1,6.5,0,3.25),('nilmadhab',4,'2014-10-18 05:04:45','AFTEK','Short Sell',20,6.5,0,65),('nilmadhab',5,'2014-10-18 12:40:58','AMTEKINDIA','Buy',1,99.95,0,0),('nilmadhab',6,'2014-10-29 19:03:31','AFTEK','Short Sell',10,6.5,0,32.5);

UNLOCK TABLES;

/*Table structure for table `wud_pending_close_options` */

DROP TABLE IF EXISTS `wud_pending_close_options`;

CREATE TABLE `wud_pending_close_options` (
  `userid` varchar(200) NOT NULL,
  `orderno` int(11) NOT NULL AUTO_INCREMENT,
  `trandateandtime` datetime NOT NULL,
  `stockname` varchar(200) NOT NULL,
  `bors` varchar(10) NOT NULL,
  `quantity` bigint(20) NOT NULL,
  `pendingprice` double NOT NULL,
  `time` datetime NOT NULL,
  `tranPrice` double NOT NULL,
  `margin` double NOT NULL,
  PRIMARY KEY (`orderno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `wud_pending_close_options` */

LOCK TABLES `wud_pending_close_options` WRITE;

UNLOCK TABLES;

/*Table structure for table `wud_pending_options` */

DROP TABLE IF EXISTS `wud_pending_options`;

CREATE TABLE `wud_pending_options` (
  `pendingid` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(200) NOT NULL,
  `stockname` varchar(200) NOT NULL,
  `quantity` bigint(20) NOT NULL,
  `bors` varchar(10) NOT NULL,
  `pendingprice` double NOT NULL,
  `comment` varchar(200) NOT NULL,
  `time` datetime NOT NULL,
  `triggerprice` double NOT NULL,
  PRIMARY KEY (`pendingid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `wud_pending_options` */

LOCK TABLES `wud_pending_options` WRITE;

UNLOCK TABLES;

/*Table structure for table `wud_rankings` */

DROP TABLE IF EXISTS `wud_rankings`;

CREATE TABLE `wud_rankings` (
  `userid` varchar(200) NOT NULL,
  `cashbalance` double NOT NULL,
  `reserve` double NOT NULL,
  `equity` double NOT NULL,
  `rank` int(11) NOT NULL,
  `margin` double NOT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `wud_rankings` */

LOCK TABLES `wud_rankings` WRITE;

insert  into `wud_rankings`(`userid`,`cashbalance`,`reserve`,`equity`,`rank`,`margin`) values ('',2500000,2500000,2500000,0,0),('nilmadhab',10000,1203271.730025,2209226.780025,1,7192.55),('nilmadhab1',2500000,2499981.8,2500000,0,18.2),('root_nil',2500000,2500000,2500000,0,0);

UNLOCK TABLES;

/*Table structure for table `wud_user_deductions` */

DROP TABLE IF EXISTS `wud_user_deductions`;

CREATE TABLE `wud_user_deductions` (
  `userid` varchar(50) NOT NULL,
  `orderno` int(11) NOT NULL,
  `bal_ded` double NOT NULL,
  `worth_ded` double NOT NULL,
  `Seq_no` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`Seq_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `wud_user_deductions` */

LOCK TABLES `wud_user_deductions` WRITE;

UNLOCK TABLES;

/*Table structure for table `yiisession` */

DROP TABLE IF EXISTS `yiisession`;

CREATE TABLE `yiisession` (
  `id` char(32) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` longblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `yiisession` */

LOCK TABLES `yiisession` WRITE;

insert  into `yiisession`(`id`,`expire`,`data`) values ('kgsum939g052330ha3nq3umee7',1412835057,'gii__returnUrl|s:25:\"/bazzer_yii/index.php/gii\";gii__id|s:5:\"yiier\";gii__name|s:5:\"yiier\";gii__states|a:0:{}95a8b5195fb098fa18144ab7771d746b__returnUrl|s:38:\"/bazzer_yii/index.php/forexBase/create\";');

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
